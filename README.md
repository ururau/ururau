# URURAU 1.2 - a Discret Event Simulator

- Installation

Install the Java Runtime Environment ( JRE ) version 6.0 or higher :
http://www.java.com/pt_BR/download

Install a PDF viewer such as Adobe Reader or Foxit Reader . Ensure
the tagged PDF files are properly associated with one of
viewers:
http://www.adobe.com/br/products/reader.html
www.foxitsoftware.com

Copy the folder ' ururau ' to any folder on your computer . It is important that
the target folder is writable . Do not run from Ururau
CD-ROM.

- Instructions for Use

Double-click ' ururau.jar '
Construe the simulation model using the templates palette on the left side
the window. Just click and drag them to the desktop. Edit each
template by clicking on them already on the desktop with the left mouse button
and then " Edit " . Connect all templates . To run the simulation
click Run -> Go . A report is generated inside the folder ' ururau '
and then displays this report .

- Limitations
No debug tool for models yet.

- Commands/Modules

1. Entities Generator
EntityProcessGenerator (entities generator - Create) is not necessarily a ProcessCommand. This class comes before all other commands in the process of model building. It is responsible for generating entities. The entity is marked as visible (by an attribute), and the nesting level (nesting attribute) is used for the Batch and Separate commands. No command processes an invisible entity, that is, it is ignored.

2. Batch
This module makes a specific number of entities enter into a queue and is transformed into one entity. The batch may be temporary, i.e., it can be subsequently undone by a command that separates batches (Separate) or may be permanent. The operation of this module is as follows: the entities that pass through this module are marked as invisible but the ninth entity; for example, if the batch is of 10, then, nine are invisible and one, visible. It also goes up one level of nesting, because there can be a batch subsequent to other one.

3. Hold
 This module holds the entities in a queue until a condition is met.

4. Separate
This module undoes a temporary batch done by the Batch command. The command decreases the nesting level and makes it visible.

5. Seize
This module and command holds one or more entities for processing. For each entity that passes through the Seize command, one or more resources are occupied, depending on the amount of resources required. One entity may get stuck while two or more resources remain "working" in this entity if the requested amount exceeds one. The entities that cannot be met enter the queue until the resource is released.

6. Release (Local)
This module and command releases the requested resource for the entity. Just point which resource and which queue, so that the resource used by the current entity is released.

7. Delay (Function)
Time that an entity takes to processing. It receives an expression that is a probability distribution. Delay is a module and command.

8. Process (Function)
This command aggregates, in a single command, commands Seize, Delay, Release, in that order.

9. Terminate
This command ends the list of commands. The visible entities are accounted at this point.

10. JumpTo
It diverts the processing of commands to another command. An index is passed, which symbolizes the position where the command is.

11. Decide (X – Decisor)
Along with JumpTo, it diverts the execution of commands depending on a condition. There are three types of decision: by chance; by condition; and by ANN. If it is by chance, the deviation happens when it is defined that the JumpTo has x% chance of occurring to a position previously specified, if not, the process goes to the command with the false branch. If the type of the Decide is by condition, then the JumpTo diverts the flow if the specified logical expression is true. If this expression is false, the flow of entities follows the false branch. Being the kind of the Decide by ANN, the JumpTo diverts the flow by means of the examples given to the net in the training. 

12. AddAtribute
This command adds an attribute within the current entity. The attribute value is a real number, which is the result of the evaluation of an expression specified in the command.

13. AddVariable
This command adds a variable in the model. The value of the variable is a real heat, which is the result of the evaluation of an expression specified in the command.

14. Record
This module serves as a counter or measures the time interval depending on an attribute (previously defined) with the value of the current simulation time.


15. Write
This module is used to write data, such as time of arrival or another variable, to txt file.

16. Emissions 
This module allows calculating the emissions of CO in systems that involve the vehicle movement.


- DCOM Configuration

http://j-interop.org/quickstart.html

project website https://www.bitbucket.org/tulioap/ururau
