/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau;

import java.text.ParseException;
import java.util.Map;
import jsl.utilities.random.distributions.Beta;
import jsl.utilities.random.distributions.Constant;
import jsl.utilities.random.distributions.DEmpirical;
import jsl.utilities.random.distributions.DistributionIfc;
import jsl.utilities.random.distributions.Exponential;
import jsl.utilities.random.distributions.Gamma;
import jsl.utilities.random.distributions.Lognormal;
import jsl.utilities.random.distributions.Normal;
import jsl.utilities.random.distributions.Poisson;
import jsl.utilities.random.distributions.Triangular;
import jsl.utilities.random.distributions.Uniform;
import jsl.utilities.random.distributions.Weibull;
import net.sourceforge.jdistlib.math.VectorMath;

/**
 *
 * @author tulio
 */
public class DExpression {

    String expression;
    double[] parameter;
    DistributionIfc distribution;
    boolean valid = false;
    public enum DistributionName { EXPO, NORM, TRIA, POIS, WEIB, BETA, GAMM, UNIF, LOGN, DISC };
    public DExpression(String expr)
    {
        // Evaluate a constant
        expression = expr;
        if (expression.matches("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?"))
        {
            distribution = new Constant(Double.parseDouble(expression));
            valid=true;
            return;
        } else if (expression.matches("[-+]?[0-9]*\\,?[0-9]+([eE][-+]?[0-9]+)?"))
        {
            valid=false;
            return;
        }

        // Evaluate func(parm1, parm2, ...,parmN)
        String[] funcAndParams = expression.split("\\(");
        String function = funcAndParams[0];
        String[] params = funcAndParams[1].split("\\)");
        String[] param = params[0].split(",");


        parameter = new double[param.length];
        for (int i = 0; i < param.length; i++)
        {
            param[i] = param[i].trim();
            if (!param[i].matches("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?"))
            {
                valid=false;
                return;
            }
            double value = Double.parseDouble(param[i]);
            parameter[i] = value;
        }

        if (function.equalsIgnoreCase(DistributionName.EXPO.name()))
        {
            if (parameter.length != 1)
            {
                valid=false;
                return;
            }
            distribution = new Exponential();
            distribution.setParameters(parameter);
            valid=true;
        }
        else if (function.equalsIgnoreCase(DistributionName.POIS.name()))
        {
            if (parameter.length != 1)
            {
                valid=false;
                return;
            }
            distribution = new Poisson();
            distribution.setParameters(parameter);
            valid=true;
        }
        else if(function.equalsIgnoreCase(DistributionName.NORM.name()))
        {
            if (parameter.length != 2)
            {
                valid=false;
                return;
            }
            distribution = new Normal();
            parameter[1]=parameter[1]*parameter[1];
            distribution.setParameters(parameter);
            valid=true;
        } 
        else if(function.equalsIgnoreCase(DistributionName.WEIB.name()))
        {
            if (parameter.length != 2)
            {
                valid=false;
                return;
            }
            distribution = new Weibull();
            
            double t = parameter[0];
            parameter[0]=parameter[1];
            parameter[1]=t;
            distribution.setParameters(parameter);
            valid=true;
        } 
        else if(function.equalsIgnoreCase(DistributionName.BETA.name()))
        {
            if (parameter.length != 2)
            {
                valid=false;
                return;
            }
            double t = parameter[0];
            parameter[0]=parameter[1];
            parameter[1]=t;
            distribution = new Beta();
            distribution.setParameters(parameter);
            valid=true;
        } 
        else if(function.equalsIgnoreCase(DistributionName.GAMM.name()))
        {
            if (parameter.length != 2)
            {
                valid=false;
                return;
            }
            double t = parameter[0];
            parameter[0]=parameter[1];
            parameter[1]=t;
            distribution = new Gamma();
            distribution.setParameters(parameter);
            valid=true;
        } 
        else if(function.equalsIgnoreCase(DistributionName.UNIF.name()))
        {
            if (parameter.length != 2)
            {
                valid=false;
                return;
            }
            distribution = new Uniform();
            distribution.setParameters(parameter);
            valid=true;
        } 
        else if(function.equalsIgnoreCase(DistributionName.LOGN.name()))
        {
            if (parameter.length != 2)
            {
                valid=false;
                return;
            }
            distribution = new Lognormal();
            parameter[1]=parameter[1]*parameter[1];
            distribution.setParameters(parameter);
            valid=true;
        } 
        else if(function.equalsIgnoreCase(DistributionName.TRIA.name()))
        {
            if (parameter.length != 3)
            {
                valid=false;
                return;
            }
            distribution = new Triangular();
            distribution.setParameters(parameter);
            valid=true;
        }
        else if(function.equalsIgnoreCase(DistributionName.DISC.name()))
        {
            if (parameter.length % 2 != 0)
            {
                valid=false;
                return;
            }
            //distribution = new DEmpirical;
            DEmpirical demp = (DEmpirical) distribution;
            double prev = 0;
            for (int i=0; i < parameter.length; i+=2)
            {
                if (i<parameter.length-2)
                {
                    demp.addProbabilityPoint(parameter[i+1], parameter[i]-prev);
                } else {
                    demp.addLastProbabilityPoint(parameter[i+1]);
                }
                prev = parameter[i];
            }
            
            //distribution.setParameters(parameter);
            valid=true;
        }
        else {
            valid = false;
        }
    }

    public DistributionIfc eval() throws ParseException
    {
        if (isValid())
        {
            return distribution;
        }
        else
        {
            throw new ParseException("Invalid Expression: "+expression,0);
        }
    }
    
    public boolean isValid()
    {
        return valid;
    }

    public String getExpression()
    {
        return expression;
    }

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        
        double data[] = {1,2,2,3,3,3,4,4,4,4,5,5,5,5,5};
        Map<String, Integer> table = VectorMath.table(data);
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DExpression e1 = new DExpression("NORM(2,1)");
                System.out.println(e1.isValid());
                DExpression e8 = new DExpression("NORM(2, 1)");
                System.out.println(e8.isValid());
                DExpression e6 = new DExpression("2.0");
                System.out.println(e6.isValid());
                DExpression e2 = new DExpression("EXPO(1.0)");
                System.out.println(e2.isValid());
                DExpression e3 = new DExpression("EXPO(1.");
                System.out.println(e3.isValid());
                DExpression e4 = new DExpression("EXPO(1.0,2)");
                System.out.println(e4.isValid());
                DExpression e5 = new DExpression("BETA(1.0,2)");
                System.out.println(e5.isValid());
                DExpression e7 = new DExpression("3,0");
                System.out.println(e7.isValid());
            }
        });
    }
}
