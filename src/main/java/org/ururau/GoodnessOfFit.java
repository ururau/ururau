/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau;

import com.mxgraph.util.mxResources;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.swing.JPanel;
import net.sourceforge.jdistlib.Beta;
import net.sourceforge.jdistlib.Exponential;
import net.sourceforge.jdistlib.Gamma;
import net.sourceforge.jdistlib.Normal;
import net.sourceforge.jdistlib.LogNormal;
import net.sourceforge.jdistlib.Uniform;
import net.sourceforge.jdistlib.Weibull;
import net.sourceforge.jdistlib.Poisson;
import net.sourceforge.jdistlib.disttest.DistributionTest;
import net.sourceforge.jdistlib.generic.GenericDistribution;
import net.sourceforge.jdistlib.math.VectorMath;
import org.renjin.script.RenjinScriptEngineFactory;
import org.renjin.sexp.Vector;
/*
import jsc.descriptive.DoubleFrequencyTable;
import jsc.descriptive.DoubleTally;
import jsc.descriptive.FrequencyTable;
import jsc.descriptive.VectorMath.mean;
import jsc.descriptive.OrderStatistics;
import jsc.distributions.Beta;
import jsc.distributions.Distribution;
import jsc.distributions.Exponential;
import jsc.distributions.Gamma;
import jsc.distributions.LogNormal;
import jsc.distributions.Normal;
import jsc.distributions.Poisson;
import jsc.distributions.Uniform;
import jsc.distributions.Weibull;
import jsc.goodnessfit.ChiSquaredFitTest;
*/
//public static final double[] kolmogorov_smirnov_test(double[] X,
//                                                     GenericGenericDistribution dist)
/**
 *
 * @author tulio
 */
public class GoodnessOfFit {
    //jsl.utilities.random.distributions.GenericDistribution distribution;
    GenericDistribution distribution;
    GoodnessFitTable rank;
    final static int SP = 0;
    final static int P_VALUE = 1;
    int distributionSelectedIndex = -1;
    RenjinScriptEngineFactory Rfactory;

    public RenjinScriptEngineFactory getRfactory() {
        return Rfactory;
    }
    
    public GoodnessOfFit(double[] obs) throws ScriptException
    {
        /* normalizating data */
        int numParam = 1;
        rank = new GoodnessFitTable();
        DecimalFormat number = new DecimalFormat("0.000");
        number.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ENGLISH));
        
        Rfactory = new RenjinScriptEngineFactory();
        
        ScriptEngine engine = Rfactory.getScriptEngine();
        engine.eval("library(fitdistrplus)");
        engine.eval("set.seed(5)");
        engine.eval("rmse <- function(error){ sqrt(mean(error^2)) }");
        engine.eval("scal <- function(data) { if (( min(data)-(max(data)-min(data))/ceiling(log2(length(data))+1) ) > 0) { max(data)-(max(data)-min(data))/ceiling(log2(length(data))+1) } else { if (all(data - round(data)) != 0) { max(data)+min(data)} else { ceiling(max(data)-min(data)) }  } }");
        engine.eval("offst <- function(data) { if (( min(data)-(max(data)-min(data))/ceiling(log2(length(data))+1) ) > 0) { min(data)-(max(data)-min(data))/ceiling(log2(length(data))+1) } else { trunc(min(data))  } }");
            
        
        engine.put("obs", obs);
        engine.eval("obs=obs+0.001");
        engine.eval("scale <- scal(obs)");
        engine.eval("offset <- offst(obs)");
        engine.eval("mystart <- list(rate=1/mean(obs))");
        Vector scaleVector = (Vector)engine.eval("scale");
        double scaleValue = (double)scaleVector.getElementAsDouble(0);
        Vector offsetVector = (Vector)engine.eval("offset");
        double offsetValue = (double)offsetVector.getElementAsDouble(0);
        engine.eval("normalized = (obs-min(obs))/(max(obs)-min(obs))");
        
        engine.eval("fbeta <- fitdist(normalized,'beta', method='mme')");
        
        engine.eval("fexp <- fitdist(obs,'exp', method='mme', start=list(rate=1/mean(obs)))");
        
        engine.eval("fgamma <- fitdist(obs,'gamma')");
        engine.eval("flnorm <- fitdist(obs,'lnorm')");
        engine.eval("fnorm <- fitdist(obs,'norm')");
        engine.eval("funif <- fitdist(obs,'unif')");
        engine.eval("fweibull <- fitdist(obs,'weibull')");
        
        engine.eval("gof <- gofstat(list(fexp,fgamma,flnorm,fnorm,funif,fweibull), "
        +"fitnames = c('expo','gamma','logn','norm','unif','weib'))");
        
        engine.eval("par(mfrow=c(1,1))");
//        engine.eval("denscomp(list(fbeta,fbeta), legendtext = plot.legend)");

        Vector gofAttVector = (Vector)engine.eval("attributes(sort(gof$ks))$names");
        Map<String,String> distMap = new HashMap<String,String>();
        distMap.put("expo","fexp");
        distMap.put("gamma","fgamma");
        distMap.put("logn","flnorm");
        distMap.put("norm","fnorm");
        distMap.put("unif","funif");
        distMap.put("weib","fweibull");
        Map<String,String> distUMap = new HashMap<String,String>();
        distUMap.put("expo","Exponential");
        distUMap.put("gamma","Gamma");
        distUMap.put("logn","Lognormal");
        distUMap.put("norm","Normal");
        distUMap.put("unif","Uniform");
        distUMap.put("weib","Weibull");
        String gofArray[] = new String[gofAttVector.length()];
        gofArray[0] = gofAttVector.getElementAsString(0);
        gofArray[1] = gofAttVector.getElementAsString(1);
        gofArray[2] = gofAttVector.getElementAsString(2);
        gofArray[3] = gofAttVector.getElementAsString(3);
        gofArray[4] = gofAttVector.getElementAsString(4);
        gofArray[5] = gofAttVector.getElementAsString(5);
        engine.eval("plot.legend <- c('"
            +mxResources.get(distUMap.get(gofArray[0]))+"','"
            +mxResources.get(distUMap.get(gofArray[1]))+"','"
            +mxResources.get(distUMap.get(gofArray[2]))+"','"
            +mxResources.get(distUMap.get(gofArray[3]))+"','"
            +mxResources.get(distUMap.get(gofArray[4]))+"','"
            +mxResources.get(distUMap.get(gofArray[5]))+"')");
        engine.eval("denscomp(list("+distMap.get(gofArray[0])
                +","+distMap.get(gofArray[1])
                +","+distMap.get(gofArray[2])
                +","+distMap.get(gofArray[3])
                +","+distMap.get(gofArray[4])
                +","+distMap.get(gofArray[5])+"), legendtext = plot.legend)");
        
        if (offsetValue > 1)
        {
            engine.eval("obs=obs-offset+0.001");
            engine.eval("fexp <- fitdist(obs,'exp', method='mme', start=list(rate=1/mean(obs)))");
            engine.eval("fgamma <- fitdist(obs,'gamma', method='mme', start=list(rate=0.01,shape=0.01))");
            engine.eval("flnorm <- fitdist(obs,'lnorm')");
            engine.eval("fnorm <- fitdist(obs,'norm')");
            engine.eval("funif <- fitdist(obs,'unif')");
            engine.eval("fweibull <- fitdist(obs,'weibull')");
        }
        
        
        engine.eval("y <- rbeta(length(normalized), "
                +"shape1=fexp$estimate['shape1'], shape2=fexp$estimate['shape2'] )");
        engine.eval("result = ks.test(obs,y)");
        
        engine.eval("hobs <- hist(obs,plot=FALSE)");
        engine.eval("fobs <- 100*hobs$density");
        engine.eval("hy <- hist(y,plot=FALSE)");
        engine.eval("fy <- 100*hy$density");
        Vector pValueVector = (Vector)engine.eval("result$p.value");
        double pValue = (double)pValueVector.getElementAsDouble(0);
        Vector cdfDistanceVector = (Vector) engine.eval("gof$ks");
        double cdfDistance = (double)cdfDistanceVector.getElementAsDouble(0);
        Vector shape1Vector = (Vector)engine.eval("fbeta$estimate['shape1']");
        double shape1 = (double)shape1Vector.getElementAsDouble(0);
        Vector shape2Vector = (Vector)engine.eval("fbeta$estimate['shape2']");
        double shape2 = (double)shape2Vector.getElementAsDouble(0);
        Vector freqObsVector = (Vector)engine.eval("fobs*scale");
        Vector freqExpecVector = (Vector)engine.eval("fy*scale");
        double[] freqObs = new double[freqObsVector.length()];
        double[] freqExpecBeta = new double[freqExpecVector.length()];
        freqObsVector.copyTo(freqObs, 0, freqObsVector.length()-1);
        freqExpecVector.copyTo(freqExpecBeta, 0, freqExpecVector.length()-1);
        Beta beta = new Beta(shape1,shape2);
        if (offsetValue > 1) {
            rank.add(mxResources.get("Beta"),pValue, cdfDistance,
                beta,
                number.format(offsetValue)+"+"+number.format(scaleValue)+"*"+"BETA("+number.format(shape1)
                        +","+number.format(shape2)+")", 
                        freqObs, freqExpecBeta);
        } else {
            rank.add(mxResources.get("Beta"),pValue, cdfDistance,
                beta,
                number.format(scaleValue)+"*"+"BETA("+number.format(shape1)
                        +","+number.format(shape2)+")", 
                        freqObs, freqExpecBeta);
        }
        engine.eval("y <- rexp(length(obs), rate=fexp$estimate['rate'])");
        Vector rateVector = (Vector)engine.eval("fexp$estimate['rate']");
        double rate = (double)rateVector.getElementAsDouble(0);
        engine.eval("result = ks.test(obs,y)");
        //engine.eval("hobs <- hist(obs)");
        engine.eval("fobs <- 100*hobs$density");
        engine.eval("hy <- hist(y,plot=FALSE)");
        engine.eval("fy <- 100*hy$density");
        pValueVector = (Vector)engine.eval("result$p.value");
        pValue = (double)pValueVector.getElementAsDouble(0);
        cdfDistanceVector = (Vector) engine.eval("gof$ks");
        cdfDistance = (double)cdfDistanceVector.getElementAsDouble(0);
        freqExpecVector = (Vector)engine.eval("fy*scale");
        double[] freqExpecExpo = new double[freqExpecVector.length()];
        freqExpecVector.copyTo(freqExpecExpo, 0, freqExpecVector.length()-1);
        Exponential expo = new Exponential(1/rate);
        if (offsetValue > 1)
        {
            rank.add(mxResources.get("Exponential"),pValue, cdfDistance,
                expo,
                number.format(offsetValue)+"+"+"EXPO("+number.format(1/rate)+")", 
                        freqObs, freqExpecExpo);
        } else {
            rank.add(mxResources.get("Exponential"),pValue, cdfDistance,
                expo,
                "EXPO("+number.format(1/rate)+")", 
                        freqObs, freqExpecExpo);
        }
        //param order
        //new jsl.utilities.random.distributions.Exponential(1/rate);
        
        
        engine.eval("y <- rgamma(length(obs), shape = fgamma$estimate['shape'], "
                +"rate = fgamma$estimate['rate'])");
        Vector shapeVector = (Vector)engine.eval("fgamma$estimate['shape']");
        double shape = (double)shapeVector.getElementAsDouble(0);
        rateVector = (Vector)engine.eval("fgamma$estimate['rate']");
        rate = (double)rateVector.getElementAsDouble(0);
        
        engine.eval("result = ks.test(obs,y)");
        //engine.eval("hobs <- hist(obs)");
        engine.eval("fobs <- 100*hobs$density");
        engine.eval("hy <- hist(y,plot=FALSE)");
        engine.eval("fy <- 100*hy$density");
        pValueVector = (Vector)engine.eval("result$p.value");
        pValue = (double)pValueVector.getElementAsDouble(0);
        cdfDistance = (double)cdfDistanceVector.getElementAsDouble(1);
        freqExpecVector = (Vector)engine.eval("fy*scale");
        double[] freqExpecGamma = new double[freqExpecVector.length()];
        freqExpecVector.copyTo(freqExpecGamma, 0, freqExpecVector.length()-1);
        Gamma gamma = new Gamma(shape,1/rate);
        if (offsetValue > 1)
        {
            rank.add(mxResources.get("Gamma"),pValue, cdfDistance,
                gamma,
                number.format(offsetValue)+"+"+"GAMM("+number.format(1/rate)+","+number.format(shape)+")", 
                        freqObs, freqExpecGamma);
        } else {
            rank.add(mxResources.get("Gamma"),pValue, cdfDistance,
                gamma,
                "GAMM("+number.format(1/rate)+","+number.format(shape)+")", 
                        freqObs, freqExpecGamma);
        }
        //param order
        //new jsl.utilities.random.distributions.Gamma(1/rate, shape);
        Vector meanlogVector;
        double meanlog;
        Vector sdlogVector;
        double sdlog;
        
        if (offsetValue > 1)
        {
            engine.eval("y <- rlnorm(length(obs), meanlog = flnorm$estimate['meanlog'], "
                +"sdlog = flnorm$estimate['sdlog'])");
            meanlogVector = (Vector)engine.eval("flnorm$estimate['meanlog']");
            meanlog = (double)meanlogVector.getElementAsDouble(0);
            sdlogVector = (Vector)engine.eval("flnorm$estimate['sdlog']");
            sdlog = (double)sdlogVector.getElementAsDouble(0);
        } 
        
        else {
            engine.eval("y <- rlnorm(length(obs), meanlog = flnorm$estimate['meanlog'], "
                +"sdlog = flnorm$estimate['sdlog'])");
            meanlogVector = (Vector)engine.eval("flnorm$estimate['meanlog']");
            meanlog = (double)meanlogVector.getElementAsDouble(0);
            sdlogVector = (Vector)engine.eval("flnorm$estimate['sdlog']");
            sdlog = (double)sdlogVector.getElementAsDouble(0);
        }
        //Vector logOffsetVector = (Vector)engine.eval("flnorm$estimate['meanlog']");
        //double logOffset = (double)logOffsetVector.getElementAsDouble(0);
        engine.eval("result = ks.test(obs,y)");
        //engine.eval("hobs <- hist(obs)");
        engine.eval("fobs <- 100*hobs$density");
        engine.eval("hy <- hist(y,plot=FALSE)");
        engine.eval("fy <- 100*hy$density");
        pValueVector = (Vector)engine.eval("result$p.value");
        pValue = (double)pValueVector.getElementAsDouble(0);

        cdfDistance = (double)cdfDistanceVector.getElementAsDouble(2);
        freqExpecVector = (Vector)engine.eval("fy*scale");
        double[] freqExpecLogNormal = new double[freqExpecVector.length()];
        freqExpecVector.copyTo(freqExpecLogNormal, 0, freqExpecVector.length()-1);
        LogNormal logn = new LogNormal(meanlog,sdlog);
        
        double m = Math.exp(meanlog+sdlog*sdlog/2);
        double s = m*Math.sqrt(Math.exp(sdlog*sdlog)-1);
        if (offsetValue > 1)
        {
            rank.add(mxResources.get("Lognormal"),pValue, cdfDistance,
                logn,
                number.format(offsetValue)+"+"
                        +"LOGN("+number.format( m )
                         +","+number.format( s )+")", 
                        freqObs, freqExpecLogNormal);
        } else {
            rank.add(mxResources.get("Lognormal"),pValue, cdfDistance,
                logn,
                "LOGN("+number.format(m)
                        +","+number.format(s)+")", 
                        freqObs, freqExpecLogNormal);
        }
        engine.eval("y <- rnorm(length(obs), mean = fnorm$estimate['mean'], "
                +"sd = fnorm$estimate['sd'])");
        Vector meanVector = (Vector)engine.eval("fnorm$estimate['mean']");
        double mean = (double)meanVector.getElementAsDouble(0);
        Vector sdVector;
        sdVector = (Vector)engine.eval("fnorm$estimate['sd']");
        double sd = (double)sdVector.getElementAsDouble(0);
        engine.eval("result = ks.test(obs,y)");
       // engine.eval("hobs <- hist(obs)");
        engine.eval("fobs <- 100*hobs$density");
        engine.eval("hy <- hist(y,plot=FALSE)");
        engine.eval("fy <- 100*hy$density");
        pValueVector = (Vector)engine.eval("result$p.value");
        pValue = (double)pValueVector.getElementAsDouble(0);
        
        cdfDistance = (double)cdfDistanceVector.getElementAsDouble(3);
        freqExpecVector = (Vector)engine.eval("fy*scale");
        double[] freqExpecNormal = new double[freqExpecVector.length()];
        freqExpecVector.copyTo(freqExpecNormal, 0, freqExpecVector.length()-1);
        Normal norm = new Normal(mean,sd);
        
        if (offsetValue > 1)
        {
        rank.add(mxResources.get("Normal"),pValue, cdfDistance,
                norm,
                "NORM("+number.format(mean+offsetValue)+","+number.format(sd)+")", 
                        freqObs, freqExpecNormal);
        } else {
            rank.add(mxResources.get("Normal"),pValue, cdfDistance,
                norm,
                "NORM("+number.format(mean)+","+number.format(sd)+")", 
                        freqObs, freqExpecNormal);
        }
        //param order
        //new jsl.utilities.random.distributions.Normal(mean, sd*sd);
        engine.eval("y <- runif(length(obs), min = funif$estimate['min'], "
                +"max = funif$estimate['max'])");
        Vector minVector = (Vector)engine.eval("funif$estimate['min']");
        double min = (double)minVector.getElementAsDouble(0);
        Vector maxVector = (Vector)engine.eval("funif$estimate['max']");
        double max = (double)maxVector.getElementAsDouble(0);
        engine.eval("result = ks.test(obs,y)");
        //engine.eval("hobs <- hist(obs)");
        engine.eval("fobs <- 100*hobs$density");
        engine.eval("hy <- hist(y,plot=FALSE)");
        engine.eval("fy <- 100*hy$density");
        pValueVector = (Vector)engine.eval("result$p.value");
        pValue = (double)pValueVector.getElementAsDouble(0);
        
        cdfDistance = (double)cdfDistanceVector.getElementAsDouble(4);
        freqExpecVector = (Vector)engine.eval("fy*scale");
        double[] freqExpecUniform = new double[freqExpecVector.length()];
        freqExpecVector.copyTo(freqExpecUniform, 0, freqExpecVector.length()-1);
        Uniform unif = new Uniform(min,max);
        if (offsetValue > 1)
        {
            rank.add(mxResources.get("Uniform"),pValue, cdfDistance,
                unif,
                "UNIF("+number.format(min+offsetValue-0.001)+","+number.format(max+offsetValue-0.001)+")", 
                        freqObs, freqExpecUniform);
        } else {
            rank.add(mxResources.get("Uniform"),pValue, cdfDistance,
                unif,
                "UNIF("+number.format(min)+","+number.format(max)+")", 
                        freqObs, freqExpecUniform);            
        }
         //param order
        //new jsl.utilities.random.distributions.Uniform(min, max);
        engine.eval("y <- rweibull(length(obs), shape = fweibull$estimate['shape'], "
                +"scale = fweibull$estimate['scale'])");
        shapeVector = (Vector)engine.eval("fweibull$estimate['shape']");
        shape = (double)shapeVector.getElementAsDouble(0);
        scaleVector = (Vector)engine.eval("fweibull$estimate['scale']");
        double scale = (double)scaleVector.getElementAsDouble(0);
        engine.eval("result = ks.test(obs,y)");
        //engine.eval("hobs <- hist(obs)");
        engine.eval("fobs <- 100*hobs$density");
        engine.eval("hy <- hist(y,plot=FALSE)");
        engine.eval("fy <- 100*hy$density");
        pValueVector = (Vector)engine.eval("result$p.value");
        pValue = (double)pValueVector.getElementAsDouble(0);
        
        cdfDistance = (double)cdfDistanceVector.getElementAsDouble(5);
        freqExpecVector = (Vector)engine.eval("fy*scale");
        double[] freqExpecWeibull = new double[freqExpecVector.length()];
        freqExpecVector.copyTo(freqExpecWeibull, 0, freqExpecVector.length()-1);
        Weibull weib = new Weibull(shape,scale);
        if (offsetValue > 1)
        {
            rank.add(mxResources.get("Weibull"),pValue, cdfDistance,
                weib,
                number.format(offsetValue)+"+"+"WEIB("+number.format(scale)+","+number.format(shape)+")", 
                        freqObs, freqExpecWeibull);
        } else {
            rank.add(mxResources.get("Weibull"),pValue, cdfDistance,
                weib,
                "WEIB("+number.format(scale)+","+number.format(shape)+")", 
                        freqObs, freqExpecWeibull);
        }
        //param order
        //new jsl.utilities.random.distributions.Weibull(shape, scale);
        Rfactory.getScriptEngine().getSession().close();
        Rfactory.getScriptEngine().getRuntimeContext().exit();
        rank.evalBestDistribution();
        distribution = rank.bestDistribution;
        
    }

    public String[] getEnabledDistributions()
    {
        String[] dist = new String[rank.distributionName.size()];
        for (int i =0; i< rank.distributionName.size(); i++)
        {
            dist[i] = rank.distributionName.get(i);
        }
        return dist;
    }
    
    public int getDistributionSelectedIndex() {
        return distributionSelectedIndex;
    }

    public void setDistributionSelectedIndex(int distributionSelectedIndex) {
        this.distributionSelectedIndex = distributionSelectedIndex;
    }
    
    double cdf(double x)
    {
        return distribution.cumulative(x);
    }
    
    /*
    private FrequencyTable doFrequencyTable(double [] data, boolean midpoint)
    {
        int numBins = (int)Math.floor(Math.sqrt(data.length)) ;
        if (checkInteger(data) && data.length < 15)
        {
            numBins = data.length;
        }
        FrequencyTable ft = new FrequencyTable("ft", numBins, data, midpoint);
        return ft;
    }
    */
    private boolean checkPositive(double [] data)
    {
        for(int i=0; i< data.length; i++)
        {
            if (data[i]<=0)
                return false;
        }
        return true;
    }
    
    private boolean checkInteger(double [] data)
    {
        for(int i=0; i< data.length; i++)
        {
            if (Math.ceil(data[i]) != data[i])
                return false;
        }
        return true;
    }

    private double gammaFunction(double x)
    {
        return jsl.utilities.random.distributions.Gamma.gammaFunction(x);
    }
    
    private double [] normalizeForBeta(double [] data) 
    {
        double[] dataMod= new double[data.length];
        for (int i =0; i< data.length; i++)
        {
            dataMod[i] = data[i] - VectorMath.min(data);
        }
        
        double max = VectorMath.max(data)-VectorMath.min(data);
        for (int i =0; i< data.length; i++)
        {
            dataMod[i] = data[i] / max;
            if (dataMod[i]>1)
            {
                System.out.println("Error in normalization");
                return null;
            }
        }
        return dataMod;
    }
    
    private double [] normalizeForLocalization(double [] data) 
    {
        double[] dataMod= new double[data.length];
        for (int i =0; i< data.length; i++)
        {
            if ((data[i] - (VectorMath.min(data))) > 0)
                dataMod[i] = data[i] - (VectorMath.min(data));
            else
                dataMod[i]=data[i];
        }
        return dataMod;
    }

    private double[] trim(double [] data)
    {
        ArrayList<Double> r = new ArrayList<Double>();
        for(int i =0; i< data.length; i++)
        {
            r.add(data[i]);
        }
        double[] ret = new double[r.size()];
        for(int i =0; i< r.size(); i++)
        {
            ret[i] = r.get(i);
        }
        return ret;
    }
    /*
     * Coefficient of Variation - CV
     */
    private double cv(double x, double s)
    {
        return s/x;
    }

    @Override
    public String toString()
    {
        String out="Expression: "+rank.bestExpression+"\n"
                +"p-value: "+ rank.bestSp+"\n"
                +"CDF distance: "+rank.bestCdfDistance+"\n";
               
        return out;
    }
   
    public String getOtherDistributions()
    {
        String out = "";
        for (int i=0; i < rank.expression.size(); i++)
        {
            if (rank.distributionName.contains("BETA"))
            {
                out+="Expression: "+rank.expression.get(i) +"\n"
                +"p-value: "+ rank.spTable.get(i) +"\n"
                +"CDF distance: --\n\n";
            } else {
            if (rank.cdfDistanceTable.get(i) > rank.bestCdfDistance) {
            out+="Expression: "+rank.expression.get(i) +"\n"
                +"p-value: "+ rank.spTable.get(i) +"\n"
                +"CDF distance: "+rank.cdfDistanceTable.get(i) +"\n\n";
            }
            }
        }
        return out;
    }
    
    public double [] getObservedFrequency(String distributionName)
    {
        for(int i=0; i< rank.distributionName.size(); i++)
        {
            if (distributionName.compareTo(rank.distributionName.get(i)) == 0
                )
            {
                return rank.observedFreq.get(i);
            }
        }
        return null;
    }
  
    public double [] getObservedFrequency(int i)
    {
        if (i>=0 && i < rank.observedFreq.size())
            return rank.observedFreq.get(i);
        else
            return null;
    }
    
    public double [] getExpectedFrequency(int i)
    {
        if (i>=0 && i < rank.expectedFreq.size())
            return rank.expectedFreq.get(i);
        else
            return null;
    }
    
    public String getExpression(int i)
    {
        if (i>=0 && i < rank.expression.size())
            return rank.expression.get(i);
        else
            return "";
    }
    
    public int getBestDistributionIndex()
    {
        for(int i=0; i< rank.distribution.size(); i++)
        {
            if (distribution == rank.distribution.get(i))
            {
                return i;
            }
        }
        return -1;
    }
        
    private class GoodnessFitTable {
        java.util.Vector<Double> cdfDistanceTable;
        java.util.Vector<Double> spTable;
        java.util.Vector<double[]> observedFreq;
        java.util.Vector<double[]> expectedFreq;
        java.util.Vector<String> distributionName;
        java.util.Vector<String> expression;
        java.util.Vector<GenericDistribution> distribution;
        String bestExpression ;
        GenericDistribution bestDistribution;
        Double bestSp;
        Double bestCdfDistance;
        
        GoodnessFitTable()
        {
            cdfDistanceTable = new java.util.Vector<Double>();
            spTable = new java.util.Vector<Double>();
            distributionName = new java.util.Vector<String>();
            distribution = new java.util.Vector<GenericDistribution>();
            expression = new java.util.Vector<String>();
            expectedFreq = new java.util.Vector<double[]>();
            observedFreq = new java.util.Vector<double[]>();
        }
        
        void add(String distName, double sp, double cdfDistance, GenericDistribution dist, 
                String expr, double[] observedFrequency, double[] expectedFrequency)
        {
            int i=0;int pos = 0;
            if (cdfDistanceTable.size() == 0)
            {
                pos = 0;
            } else {
                pos = cdfDistanceTable.size() -1;
            }
            for (double cdf : cdfDistanceTable)
            {
                if (cdf > cdfDistance)
                {
                    pos = i;
                    break;
                }
                i++;
            }
            
            cdfDistanceTable.add(pos,cdfDistance);
            spTable.add(pos,sp);
            distributionName.add(pos,distName);
            distribution.add(pos,dist);
            expression.add(pos,expr);
            expectedFreq.add(pos,expectedFrequency);
            observedFreq.add(pos,observedFrequency);
            System.out.println(distName+" sp:"+sp+" cdfDist:"+cdfDistance+", expr:"+expr);                
        }
        
        void evalBestDistribution()
        {
            if (distribution.isEmpty())
                return;
            
            double min = 1000;
            int best = 0;
            for (int i =0; i < cdfDistanceTable.size(); i++)
            {
                if (cdfDistanceTable.get(i) < min && spTable.get(i) != 0)
                {
                    min = cdfDistanceTable.get(i);
                    best = i;
                }
            }
            bestExpression = expression.elementAt(best);
            bestDistribution = distribution.elementAt(best);
            bestSp = spTable.elementAt(best);
            bestCdfDistance = cdfDistanceTable.elementAt(best);
            return ;
        }
    }
}




 