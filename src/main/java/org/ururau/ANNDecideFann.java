package org.ururau;

import com.googlecode.fannj.Fann;
import com.googlecode.fannj.FannSparse;
import com.googlecode.fannj.Layer;
import com.googlecode.fannj.Neuron;
import com.googlecode.fannj.TrainingAlgorithm;
import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
//import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
//import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.neural.networks.training.propagation.back.Backpropagation;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.util.csv.CSVFormat;
import org.encog.util.simple.EncogUtility;
import org.encog.util.simple.TrainingSetUtil;
import com.googlecode.fannj.ActivationFunction;
import com.googlecode.fannj.Trainer;
import com.mxgraph.util.mxResources;
import com.sun.jna.Pointer;
import java.net.URL;

/**
 *
 * @author Marília
 */
public class ANNDecideFann {
	
    
        Fann fann;
        

        public double consulta(Fann ann, double no1,double no2, double no3, double no4, double no5, int cmd1) {
           
            
            float input1[];
            float [] output = new float[1];
            output[0] = 0;
            switch(cmd1)
            {
                case 2:
                    input1 = new float[2];
                    input1[0] = (float)no1;
                    input1[1] = (float)no2;
                    output = fann.run(input1);
                    break;

                case 3:
                    input1 = new float[3];
                    input1[0] = (float)no1;
                    input1[1] = (float)no2;
                    input1[2] = (float)no3;
                    output = fann.run(input1);
                    break;

                case 4:
                    input1 = new float[4];
                    input1[0] = (float)no1;
                    input1[1] = (float)no2;
                    input1[2] = (float)no3;
                    input1[3] = (float)no4;
                    output = fann.run(input1);
                    break;
                case 5:
                    input1 = new float[5];
                    input1[0] = (float)no1;
                    input1[1] = (float)no2;
                    input1[2] = (float)no3;
                    input1[3] = (float)no4;
                    input1[4] = (float)no5;
                    output = fann.run(input1);
                    break;

                default:
                    javax.swing.JDialog f=new javax.swing.JDialog();  
                    f.setSize(250,150); 
                    javax.swing.JOptionPane.showMessageDialog(f,mxResources.get("warning"),mxResources.get("annWarningLimit"),javax.swing.JOptionPane.WARNING_MESSAGE);
            }
            
            
            
            System.out.println("--------------Valores dinâmicos dos neurônios------------");
            System.out.println("1º Neurônio:"+no1);
            System.out.println("2º Neurônio:"+no2);
            switch(cmd1)
            {
                case 3:
                    System.out.println("3º Neurônio:"+no3);
                    break;
                case 4:
                    System.out.println("3º Neurônio:"+no3);
                    System.out.println("4º Neurônio:"+no4);
                    break;
                case 5:
                    System.out.println("3º Neurônio:"+no3);
                    System.out.println("4º Neurônio:"+no4);
                    System.out.println("5º Neurônio:"+no5);
                    break;
            }           
            System.out.println("Saída: "+output[0]);
             if (output[0]>0.5) {
                //System.out.println("TRUE");
                return 1.0;                
            } else {
                //System.out.println("FALSE");
                return 0.0;
            }  
           // return output1.getData(0);
        }
        
        public Fann Treinamento( String path, int cmd1, int cmd2, int cmd3) throws IOException, Exception {
            System.out.println(path);
            File trainingFile = new File(path);
            if (!FileUtils.readFileToString(trainingFile).contains(","))
            {
                return null;
            } else {

                
                /* convert training file to Fann format */
                List<String> readLines = FileUtils.readLines(new File(path));
                path = trainingFile.getAbsolutePath().replaceAll(".csv", ".train");
                String pathConfigFile = trainingFile.getAbsolutePath().replaceAll(".csv", "_float.net");
                File fannTrainingFile = new File(path);
                String header = readLines.size()+" "+cmd1+" "+1+"\n";
                int i = 0;
                for (i = 0; i < readLines.size(); i++)
                {
                    String line[] = readLines.get(i).split(",");
                    String inp ="";
                    if (cmd1 > line.length-1)
                    {
                        throw new Exception("Arquivo de treinamento tem menos neuronios de entrada do que especificado."+cmd1+" != "+line.length);
                    }
                    for (int j =0 ; j < cmd1; j++)
                    {
                       inp = inp + line[j]+" ";
                    }
                    String outp = line[line.length - 1];
                    String pair = inp+"\n"+outp+"\n";
                    if (i ==0)
                    {
                        FileUtils.write(fannTrainingFile, header, false);
                    }
                    FileUtils.write(fannTrainingFile, pair, true);
                }
                if (i>0)
                {
                    
                List<Layer> layers = new ArrayList<Layer>();
                layers.add(Layer.create(cmd1));
                layers.add(Layer.create(cmd2, ActivationFunction.FANN_SIGMOID));
                layers.add(Layer.create(1, ActivationFunction.FANN_SIGMOID));

                fann = new Fann(layers);
                Trainer trainer = new Trainer(fann);

                float desiredError = .013f;
                float mse = trainer.train(path, 500000, 1000,
                    desiredError);
                    
                    return fann;
                } else {
                    return null;
                }
            }
        }
        
        
        public static void main(final String args[]) {
		/*
		// create a neural network, without using a factory
		BasicNetwork network = new BasicNetwork();
		network.addLayer(new BasicLayer(null,true,2));
		network.addLayer(new BasicLayer(new ActivationSigmoid(),true,3));
		network.addLayer(new BasicLayer(new ActivationSigmoid(),false,1));
		network.getStructure().finalizeStructure();
		network.reset();

		// create training data
		MLDataSet trainingSet = new BasicMLDataSet(XOR_INPUT, XOR_IDEAL);
		
		// train the neural network
		final ResilientPropagation train = new ResilientPropagation(network, trainingSet);
                
		int epoch = 1;
		do {
			train.iteration();
			System.out.println("Epoch #" + epoch + " Error:" + train.getError());
			epoch++;
		} while(train.getError() > 0.01);
		train.finishTraining();
                double result = consulta(network,1.0,0.0);                                
		System.out.println(result);			
		Encog.getInstance().shutdown();*/
	}        
}