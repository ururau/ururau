/**
 * $Id: GraphEditor.java,v 1.8 2011-02-14 15:45:58 gaudenz Exp $
 * Copyright (c) 2006-2010, Gaudenz Alder, David Benson
 */
package org.ururau;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.ururau.modules.UTerminate;
import org.ururau.modules.UAnd;
import org.ururau.modules.UResource;
import org.ururau.modules.ULocal;
import org.ururau.modules.UJump;
import org.ururau.modules.UCreate;
import org.ururau.modules.UXor;
import org.ururau.modules.UXorBranch;
import org.ururau.modules.UFunction;
import java.awt.Color;
import java.awt.Point;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.UIManager;

import org.w3c.dom.Document;

import org.ururau.editor.BasicGraphEditor;
import org.ururau.editor.EditorMenuBar;
import org.ururau.editor.EditorPalette;
import com.mxgraph.io.mxCodec;
import com.mxgraph.io.mxCodecRegistry;
import com.mxgraph.io.mxObjectCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxICell;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxGraphTransferable;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxResources;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxGraph;
import com.sun.jna.FunctionMapper;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.prefs.Preferences;
import javax.swing.JDialog;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.lang.SystemUtils;
import org.ururau.Template.TemplateType;
import org.ururau.commands.Decide.DecideType;
import org.ururau.editor.EditorActions.TogglePropertyItem;
import org.ururau.modules.UInventorFunction;
import org.ururau.modules.UVariable;
import org.ururau.utils.Configuration;
import org.ururau.ErrorMsg;

public class GraphEditor extends BasicGraphEditor
{

	/**
	 * 
	 */
        
	private static final long serialVersionUID = -4601740824088314699L;

    
	/**
	 * Holds the shared number formatter.
	 * 
	 * @see NumberFormat#getInstance()
	 */
	public static final NumberFormat numberFormat = NumberFormat.getInstance();

	/**
	 * Holds the URL for the icon to be used as a handle for creating new
	 * connections. This is currently unused.
	 */
	//public static URL url = null;

	//GraphEditor.class.getClassLoader().getResource("images/connector.gif");

	public GraphEditor()
	{

		this("Ururau 1.2", new CustomGraphComponent(new CustomGraph()));
	}

	/**
	 * 
	 */
	public GraphEditor(String appTitle, mxGraphComponent component)
	{
		super(appTitle, component);
		final mxGraph graph = graphComponent.getGraph();

        // sets graph connection properties
        TogglePropertyItem tpi = new TogglePropertyItem(graph, mxResources
				.get("disconnectOnMove"), "DisconnectOnMove");
        tpi.execute(graph, "DisconnectOnMove", false);
		// Creates the shapes palette
		EditorPalette shapesPalette = insertPalette(mxResources.get("shapes"));

		// Sets the edge template to be used for creating new edges if an edge
		// is clicked in the shape palette
		shapesPalette.addListener(mxEvent.SELECT, new mxIEventListener()
		{
			public void invoke(Object sender, mxEventObject evt)
			{
				Object tmp = evt.getProperty("transferable");
				if (tmp instanceof mxGraphTransferable)
				{
					mxGraphTransferable t = (mxGraphTransferable) tmp;
					Object cell = t.getCells()[0];

					if (graph.getModel().isEdge(cell))
					{
						((CustomGraph) graph).setEdgeTemplate(cell);
					}
				}
			}

		});

        
        
        shapesPalette
				.addTemplate(
						mxResources.get("create"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/t_create.png")),
						"create;image=/images/create.png;",
						80, 80, Template.TemplateType.Create);
        shapesPalette
				.addTemplate(
						mxResources.get("function"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/rectangle.png")),
						"retangle", 60, 60, Template.TemplateType.Function);
        shapesPalette
				.addTemplate(
						mxResources.get("variable"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/rectangle.png")),
						"retangle", 180, 60, Template.TemplateType.Variable);
        
        shapesPalette
				.addTemplate(
						mxResources.get("inventorfunction"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/rectangle.png")),
						"retangle", 60, 60, Template.TemplateType.InventorFunction);
        shapesPalette
				.addTemplate(
						mxResources.get("moving"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/moving.png")),
						"moving;image=/images/moving.png;", 100, 100, Template.TemplateType.Function);
        shapesPalette
				.addTemplate(
						mxResources.get("terminate"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/terminate.png")),
						"image;image=/images/terminate.png",
						60, 60, Template.TemplateType.Terminate);
       shapesPalette
				.addTemplate(
						mxResources.get("jump"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/triangle.png")),
						"jump;image=/images/triangle.png",
						60, 60, Template.TemplateType.Jump);
       shapesPalette
				.addTemplate(
						mxResources.get("local"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/rectangle.png")),
						"retangle", 60, 60, Template.TemplateType.Local);
       shapesPalette
				.addTemplate(
						mxResources.get("hold"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/rectangle.png")),
						"retangle", 60, 60, Template.TemplateType.Hold);
       shapesPalette
				.addTemplate(
						mxResources.get("assign"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/rectangle.png")),
	
                                                "retangle", 60, 60, Template.TemplateType.Assign);
       // /* BEGIN modification Fabio & Eder */
       shapesPalette
				.addTemplate(
						mxResources.get("myEmissions"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/moving.png")),
						"moving;image=/images/moving.png;", 100, 100, Template.TemplateType.MyEmissions);
///* END modification Fabio & Eder */
       shapesPalette
				.addTemplate(
						mxResources.get("record"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/rectangle.png")),
						"retangle", 60, 60, Template.TemplateType.Record);

       shapesPalette
				.addTemplate(
						mxResources.get("batch"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/rectangle.png")),
						"retangle", 60, 60, Template.TemplateType.Batch);
       shapesPalette
				.addTemplate(
						mxResources.get("separate"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/rectangle.png")),
						"retangle", 60, 60, Template.TemplateType.Separate);
       shapesPalette
				.addTemplate(
						mxResources.get("write"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/rectangle.png")),
						"retangle", 60, 60, Template.TemplateType.Write);
       shapesPalette
				.addTemplate(
						mxResources.get("readwrite"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/rectangle.png")),
						"retangle", 60, 60, Template.TemplateType.SheetReadWrite);
        shapesPalette
				.addTemplate(
						mxResources.get("runmacro"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/rectangle.png")),
						"retangle", 60, 60, Template.TemplateType.RunMacro);

        shapesPalette
				.addTemplate(
						"&",
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/AND.png")),
						"image;image=/images/AND.png",
						60, 60,  Template.TemplateType.And );
         shapesPalette
				.addTemplate(
						"X",
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/X.png")),
						"image;image=/images/X.png",
						60, 60, Template.TemplateType.Xor);

        
        shapesPalette
				.addTemplate(
						mxResources.get("resource"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/resource.png")),
						"rectangle", 60, 60, Template.TemplateType.Resource);

       shapesPalette
				.addEdgeTemplate(
						mxResources.get("connector"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/entity.png")),
						"entity", 100, 100, "");

       shapesPalette
				.addEdgeTemplate(
						mxResources.get("verticalConnector"),
						new ImageIcon(
								GraphEditor.class.getClassLoader()
										.getResource("images/vertical.png")),
						null, 100, 100, "");
 	}

	public static class CustomGraphComponent extends mxGraphComponent
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = -6833603133512882012L;

		/**
		 * 
		 * @param graph
		 */
		public CustomGraphComponent(mxGraph graph)
		{
			super(graph);

			// Sets switches typically used in an editor
			setPageVisible(true);
                        ///*LANDSCAPE \/
                            PageFormat format = new PageFormat();//
                            Paper size = new Paper();
                            size.setSize(810, 670); //610, 470
                            format.setPaper(size);

                            setPageFormat(format);
                        //LANDSCAPE /\*/
			setGridVisible(true);
			setToolTips(true);
			getConnectionHandler().setCreateTarget(true);

                    // Loads the defalt stylesheet from an external file
                        System.setProperty("javax.xml.parsers.DocumentBuilderFactory", "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
			mxCodec codec = new mxCodec();
			Document doc = mxUtils.loadDocument(GraphEditor.class.getClassLoader().getResource(
					"basic-style.xml")
					.toString());
			codec.decode(doc.getDocumentElement(), graph.getStylesheet());

			// Sets the background to white
			getViewport().setOpaque(true);
			getViewport().setBackground(Color.WHITE);
		}

		/**
		 * Overrides drop behaviour to set the cell style if the target
		 * is not a valid drop target and the cells are of the same
		 * type (eg. both vertices or both edges). 
		 */
		public Object[] importCells(Object[] cells, double dx, double dy,
				Object target, Point location)
		{
			if (target == null && cells.length == 1 && location != null)
			{
				target = getCellAt(location.x, location.y);

				if (target instanceof mxICell && cells[0] instanceof mxICell)
				{
					mxICell targetCell = (mxICell) target;
					mxICell dropCell = (mxICell) cells[0];

					if (targetCell.isVertex() == dropCell.isVertex()
							|| targetCell.isEdge() == dropCell.isEdge())
					{
						mxIGraphModel model = graph.getModel();
						model.setStyle(target, model.getStyle(cells[0]));
						graph.setSelectionCell(target);

						return null;
					}
				}
			}

			return super.importCells(cells, dx, dy, target, location);
		}

	}

	/**
	 * A graph that creates new edges from a given template edge.
	 */
	public static class CustomGraph extends mxGraph
	{


		/**
		 * Holds the edge to be used as a template for inserting new edges.
		 */
		protected Object edgeTemplate;

		/**
		 * Custom graph that defines the alternate edge style to be used when
		 * the middle control point of edges is double clicked (flipped).
		 */
		public CustomGraph()
		{
			setAlternateEdgeStyle("edgeStyle=mxEdgeStyle.ElbowConnector;elbow=vertical");
		}

		/**
		 * Sets the edge template to be used to inserting edges.
		 */
		public void setEdgeTemplate(Object template)
		{
			edgeTemplate = template;
		}

		/**
		 * Prints out some useful information about the cell in the tooltip.
		 */
		public String getToolTipForCell(Object cell)
		{
			String tip = "<html>";
			mxGeometry geo = getModel().getGeometry(cell);
			mxCellState state = getView().getState(cell);

			if (getModel().isEdge(cell))
			{
				tip += "points={";

				if (geo != null)
				{
					List<mxPoint> points = geo.getPoints();

					if (points != null)
					{
						Iterator<mxPoint> it = points.iterator();

						while (it.hasNext())
						{
							mxPoint point = it.next();
							tip += "[x=" + numberFormat.format(point.getX())
									+ ",y=" + numberFormat.format(point.getY())
									+ "],";
						}

						tip = tip.substring(0, tip.length() - 1);
					}
				}

				tip += "}<br>";
				tip += "absPoints={";

				if (state != null)
				{

					for (int i = 0; i < state.getAbsolutePointCount(); i++)
					{
						mxPoint point = state.getAbsolutePoint(i);
						tip += "[x=" + numberFormat.format(point.getX())
								+ ",y=" + numberFormat.format(point.getY())
								+ "],";
					}

					tip = tip.substring(0, tip.length() - 1);
				}

				tip += "}";
			}
			else
			{
				tip += "geo=[";

				if (geo != null)
				{
					tip += "x=" + numberFormat.format(geo.getX()) + ",y="
							+ numberFormat.format(geo.getY()) + ",width="
							+ numberFormat.format(geo.getWidth()) + ",height="
							+ numberFormat.format(geo.getHeight());
				}

				tip += "]<br>";
				tip += "state=[";

				if (state != null)
				{
					tip += "x=" + numberFormat.format(state.getX()) + ",y="
							+ numberFormat.format(state.getY()) + ",width="
							+ numberFormat.format(state.getWidth())
							+ ",height="
							+ numberFormat.format(state.getHeight());
				}

				tip += "]";
			}

			mxPoint trans = getView().getTranslate();

			tip += "<br>scale=" + numberFormat.format(getView().getScale())
					+ ", translate=[x=" + numberFormat.format(trans.getX())
					+ ",y=" + numberFormat.format(trans.getY()) + "]";
			tip += "</html>";

			return tip;
		}

		/**
		 * Overrides the method to use the currently selected edge template for
		 * new edges.
		 *
		 * @param graph
		 * @param parent
		 * @param id
		 * @param value
		 * @param source
		 * @param target
		 * @param style
		 * @return
		 */
		public Object createEdge(Object parent, String id, Object value,
				Object source, Object target, String style)
		{
			if (edgeTemplate != null)
			{
                            mxCell edge = (mxCell) cloneCells(new Object[] { edgeTemplate })[0];
                            edge.setId(id);
                            if (( (mxCell)source).getValue() instanceof UXor)
                            {
                                UXor xor = (UXor) ( (mxCell)source).getValue();
                                UXorBranch xorBranch = new UXorBranch(xor.getXorType());
                                edge.setValue(xorBranch);
                            }

				return edge;
			}
			return super.createEdge(parent, id, value, source, target, style);
		}

        
           // Overrides method to disallow edge label editing
        public boolean isCellEditable(Object cell)
        {
            //return !getModel().isEdge(cell);
            return false;
        }
        

        // Overrides method to provide a cell label in the display
        @Override
        public String convertValueToString(Object cell)
        {
            if (cell instanceof mxCell)
            {
                if ( ((mxCell) cell).isVertex() )
                {
                    Object value = ((mxCell) cell).getValue();

                    if (value instanceof Template)
                    {
                        Template elt = (Template) value;
                        if (value instanceof UTerminate)
                        {
                            return "";
                        }
                        if (value instanceof UXor)
                        {
                            return "";
                        }
                        if (value instanceof UAnd)
                        {
                            return "";
                        }
                        if (value instanceof UJump)
                        {
                            return elt.getName();
                        }
                        if (value instanceof UVariable)
                        {
                            UVariable uvar;
                            uvar = (UVariable)elt;
                            if (uvar.getVariableName() !=null)
                            {
                                return uvar.getVariableName();
                            } else {
                               return elt.getIdsim();
                            }
                        }
                        else
                        {
                            return elt.getIdsim();
                        }
                    }
                } else {
                    Object value = ((mxCell) cell).getValue();
                    if (value instanceof UXorBranch)
                    {
                        UXorBranch xor = (UXorBranch) value;
                        return xor.toString();
                    }
                }

            }
            
            return super.convertValueToString(cell);
        }

        
        public void updateModelOnAdd()
        {
            final mxGraph graph = this;

            if (graph.getChildEdges(graph.getDefaultParent()).length < 1 )
            {
                return;
            }

            Object obj [] = (Object[]) graph.getChildEdges(graph.getDefaultParent());
            for( int i = 0; i < obj.length; i++)
            {
                mxCell edge = (mxCell) obj[i];
                mxCell s = (mxCell) edge.getSource();
                mxCell t = (mxCell) edge.getTarget();
                Template tt = (Template) t.getValue();
                Template ts = (Template) s.getValue();
                if (ts instanceof Template && tt instanceof UAnd)
                {
                    if (!(ts instanceof UXor))
                    {
                        UJump jump = (UJump) TemplateFactory.getInstance(Template.TemplateType.Jump,false,true);
                        jump.setOrigin(true);
                        jump.setDestination(tt);
                        edge.setValue(jump);
                    }
                }
                if (ts instanceof UResource && tt instanceof UFunction)
                {
                    UResource r = (UResource) ts;
                    UFunction f = (UFunction) tt;
                    f.setFunctionType(FunctionType.Process.ordinal());
                    f.setTResource(r);
                    t.setValue(tt);
                }
                if (ts instanceof UResource && tt instanceof UInventorFunction)
                {
                    UResource r = (UResource) ts;
                    UInventorFunction f = (UInventorFunction) tt;
                    f.setFunctionType(InventorFunctionType.Process.ordinal());
                    f.setTResource(r);
                    t.setValue(tt);
                }
                if (ts instanceof UResource && tt instanceof ULocal)
                {
                    UResource r = (UResource) ts;
                    ULocal f = (ULocal) tt;
                    if (f.getLocalType() == LocalType.Seize.ordinal()
                            || f.getLocalType() == LocalType.Local.ordinal())
                    {
                        f.setLocalType(LocalType.Seize.ordinal());
                        f.setTResource(r);
                        t.setValue(tt);
                    } else if (f.getLocalType() == LocalType.Release.ordinal()){
                        f.setTResource(r);
                        t.setValue(tt);
                    }
                }

            }
	
	}

  	public void updateModelOnDelete()
	{
            final mxGraph graph = this;
            if (graph.getChildVertices(graph.getDefaultParent()).length < 1 )
            {
                return;
            }

            Object obj [] = (Object[]) graph.getChildVertices(graph.getDefaultParent());
            for(int i = 0; i < obj.length; i++)
            {
                mxCell vertex = (mxCell) obj[i];
                Template tt = (Template) vertex.getValue();
                // remove Resource from Process
                if (tt instanceof UFunction)
                {
                    boolean hasResource = false;
                    Object obje [] = (Object[]) graph.getConnections(obj[i]);
                    for (int j =0; j< obje.length; j++)
                    {
                        mxCell edge = (mxCell) obje[j];
                        mxCell s = (mxCell) edge.getSource();
                        Template ts = (Template) s.getValue();
                        if (ts instanceof UResource)
                        {
                            hasResource = true;
                        }
                    }
                    if (!hasResource)
                    {
                        UFunction f = (UFunction) tt;
                        f.setFunctionType(FunctionType.Delay.ordinal());
                        vertex.setValue(f);
                    }
                }
                if (tt instanceof UInventorFunction)
                {
                    boolean hasResource = false;
                    Object obje [] = (Object[]) graph.getConnections(obj[i]);
                    for (int j =0; j< obje.length; j++)
                    {
                        mxCell edge = (mxCell) obje[j];
                        mxCell s = (mxCell) edge.getSource();
                        Template ts = (Template) s.getValue();
                        if (ts instanceof UResource)
                        {
                            hasResource = true;
                        }
                    }
                    if (!hasResource)
                    {
                        UInventorFunction f = (UInventorFunction) tt;
                        f.setFunctionType(FunctionType.Delay.ordinal());
                        vertex.setValue(f);
                    }
                }
                // remove Resource from Seize or Release
                if (tt instanceof ULocal)
                {
                    boolean hasResource = false;
                    Object obje [] = (Object[]) graph.getConnections(obj[i]);
                    for (int j =0; j< obje.length; j++)
                    {
                        mxCell edge = (mxCell) obje[j];
                        mxCell s = (mxCell) edge.getSource();
                        Template ts = (Template) s.getValue();
                        if (ts instanceof UResource)
                        {
                            hasResource = true;
                        }
                    }
                    if (!hasResource)
                    {
                        ULocal l = (ULocal) tt;
                        l.setLocalType(LocalType.Local.ordinal());
                        vertex.setValue(l);
                    }
                }

            }
        }

        public void updateModelOnStart()
        {
            final mxGraph graph = this;
            if (graph.getChildEdges(graph.getDefaultParent()).length < 1 )
            {
                return;
            }

            Object obj [] = (Object[]) graph.getChildEdges(graph.getDefaultParent());
            for( int i = 0; i < obj.length; i++)
            {
                mxCell edge = (mxCell) obj[i];
                mxCell s = (mxCell) edge.getSource();
                mxCell t = (mxCell) edge.getTarget();
                Template tt = (Template) t.getValue();
                Template ts = (Template) s.getValue();

                // link Resource with Process
                if (ts instanceof UResource && tt instanceof UFunction)
                {
                    UResource r = (UResource) ts;
                    UFunction f = (UFunction) tt;
                    if (f.getFunctionType() == FunctionType.Process.ordinal())
                    {
                        f.setTResource(r);
                        t.setValue(f);
                    }
                }
                if (ts instanceof UResource && tt instanceof UInventorFunction)
                {
                    UResource r = (UResource) ts;
                    UInventorFunction f = (UInventorFunction) tt;
                    if (f.getFunctionType() == InventorFunctionType.Process.ordinal())
                    {
                        f.setTResource(r);
                        t.setValue(f);
                    }
                }
            }
        }

        public void updateModelQueues()
        {
            final mxGraph graph = this;
            if (graph.getChildVertices(graph.getDefaultParent()).length < 1 )
            {
                return;
            }
            Object obj [] = (Object[]) graph.getChildVertices(graph.getDefaultParent());
            UResource resourceSeize = null;
            String releasebleQueue = null;
            for(int i = 0; i < obj.length; i++)
            {
                mxCell vertex_i = (mxCell) obj[i];
                Template temp_i = (Template) vertex_i.getValue();

                if (temp_i instanceof ULocal)
                {
                    ULocal ls = (ULocal) temp_i;
                    if (ls.getLocalType() == LocalType.Seize.ordinal())
                    {
                        resourceSeize = ls.getTResource();
                        releasebleQueue = ls.getQueueName();
                    }
                }
                for(int j = 0; j < obj.length; j++)
                {
                    mxCell vertex_j = (mxCell) obj[j];
                    Template temp_j = (Template) vertex_j.getValue();

                    if (temp_j instanceof ULocal)
                    {
                        ULocal lr = (ULocal) temp_j;
                        if (lr.getLocalType() == LocalType.Release.ordinal())
                        {
                            if (resourceSeize != null && resourceSeize.getIdsim() != null && lr.getTResource().getIdsim() != null)
                            {
                                if (resourceSeize.getIdsim().compareTo(lr.getTResource().getIdsim()) == 0)
                                {
                                    lr.setReleasebleQueue(releasebleQueue);
                                }
                            }
                        }
                    }
                }
            }
        }
   

        public void updateLines()
        {
            final mxGraph graph = this;
            if (graph.getChildVertices(graph.getDefaultParent()).length < 1 )
            {
                return;
            }
            Object[] v = graph.getChildVertices(graph.getDefaultParent());
            for (int i=0; i< v.length; i++)
            {
                Object from = null;
                if (((mxCell)v[i]).getValue() instanceof UCreate)
                {
                    from = (mxCell) v[i];
                    Template.resetLineCounter();
                    resetTemplateLineCode(graph, from);
                    enumerateLines(graph, from);
                    enumerateTerminals(graph, from);
                }
            }
        }

        private void resetTemplateLineCode(final mxGraph graph, Object from) {
            graph.traverse(from, true, new mxICellVisitor()
            {
              public boolean visit(Object vertex, Object edge)
              {
                      
                  Template temp = (Template) ((mxCell)vertex).getValue();
                  temp.setLineCodeNumber(-1);
                      
                      for (int i = 0; i < graph.getEdges(vertex).length; i++)
                      {
                        mxCell e = ((mxCell)graph.getEdges(vertex)[i]);
                        if ( ((mxCell)e).getValue() instanceof UJump 
                                && (!(((mxCell)vertex).getValue() instanceof UAnd)) )
                        {
                            Template tempe = (Template) ((mxCell)e).getValue();
                            tempe.setLineCodeNumber(-1);
                        }
                      }
                  return true;
              }
            });
        }

        private void enumerateLines(final mxGraph graph, Object from) {
            graph.traverse(from, true, new mxICellVisitor()
            {
              public boolean visit(Object vertex, Object edge)
              {
                  if ( (((mxCell)vertex).getValue() instanceof UCreate) )
                  {
                      Template temp = (Template) ((mxCell)vertex).getValue();
                      //temp.setLineCodeNumber(Template.incrementLineCounter());
                  }
                  if ( (((mxCell)vertex).getValue() instanceof UAnd) )
                  {
                      for (int i = 0; i < graph.getEdges(vertex).length; i++)
                      {
                        mxCell e = ((mxCell)graph.getEdges(vertex)[i]);
                        if (((mxCell)(e)) != null)
                        {
                            if (((mxCell)(e)).getValue() instanceof Template)
                            {
                                Template edgeTemplate = ((Template)((mxCell)(e)).getValue());
                                if (edgeTemplate.getLineCodeNumber() == -1)
                                {    
                                } else {
                                    Template.decrementLineCounter();
                                    break;
                                }
                            }
                        }
                      }
                      //Template.decrementLineCounter();
                  }
                  if (! (((mxCell)vertex).getValue() instanceof UCreate) )
                  {
                      
                      Template temp = (Template) ((mxCell)vertex).getValue();
                      if (temp.getLineCodeNumber() == -1)
                      {
                          temp.setLineCodeNumber(Template.incrementLineCounter());
                      }
                      
                      for (int i = 0; i < graph.getEdges(vertex).length; i++)
                      {
                        mxCell e = ((mxCell)graph.getEdges(vertex)[i]);
                        if ( ((mxCell)e).getValue() instanceof UJump 
                                && (!(((mxCell)vertex).getValue() instanceof UAnd)) )
                        {
                            Template tempe = (Template) ((mxCell)e).getValue();
                            if (tempe.getLineCodeNumber() == -1)
                            {
                                tempe.setLineCodeNumber(Template.incrementLineCounter());
                            }
                        }
                      }
                  }
                  if (((mxCell)vertex).getValue() instanceof UCreate)
                  {
                      Template.incrementLineCounter();
                  }
                  if (((mxCell)vertex).getValue() instanceof UTerminate)
                  {
                      UTerminate temp = (UTerminate) ((mxCell)vertex).getValue();
                      temp.setLastLine(Template.getLineCounter());
                  }
                  return true;
              }
            });
        }
        
        private void enumerateTerminals(final mxGraph graph, Object from) {
            graph.traverse(from, true, new mxICellVisitor()
            {
              public boolean visit(Object vertex, Object edge)
              {
                  if (((mxCell)vertex).getValue() instanceof UTerminate)
                  {
                      UTerminate temp = (UTerminate) ((mxCell)vertex).getValue();
                      temp.setLastLine(Template.getLineCounter());
                  }
                  return true;
              }
            });
        }

        public void updateModelJumps ()
        {
            final mxGraph graph = this;
            if (graph.getChildVertices(graph.getDefaultParent()).length < 1 )
            {
                return;
            }
            
            UJump from = null;
            UJump to = null;
            UJump jump = null;
            Object[] v = graph.getChildVertices(graph.getDefaultParent());
            HashMap<String, UJump> jumpMap = new HashMap<String, UJump>();
            for (int i=0; i< v.length; i++)
            {
                Object node = null;
                if (((mxCell)v[i]).getValue() instanceof UJump)
                {
                    node = (mxCell) v[i];
                    jump = (UJump)((mxCell)node).getValue();
                    if ( !jump.isOrigin())
                    {
                        if (!jumpMap.containsKey( (jump).getName()))
                        {
                            to = jump; 
                            jumpMap.put(jump.getName(), jump);
                        } 
                    }
                }
            }
            
            for (int i=0; i < v.length; i++)
            {
                Object node = null;
                if (((mxCell)v[i]).getValue() instanceof UJump)
                {
                    node = (mxCell) v[i];
                    jump = (UJump)((mxCell)node).getValue();
                    if ( jump.isOrigin() )
                    {
                        from = jump;
                        to = jumpMap.get(from.getName());
                        from.setDestination(to);
                        System.out.println(from.getName()+" "+from.getLineCodeNumber());
                        System.out.println(to.getName()+" "+to.getLineCodeNumber());
                        to = null;
                    }
                } if (((mxCell)v[i]).getValue() instanceof UAnd && to != null)
                {
                    node = (mxCell) v[i];
                    node = ((mxCell)node).getParent();
                    if( ((mxCell)node).getValue() instanceof UJump )
                    {
                        jump = (UJump)((mxCell)node).getValue();
                        from = to;
                        to = jump;
                        from.setDestination(to);
                    }  
                }
            }
        }

        public void updateXor()
        {
            final mxGraph graph = this;
            if (graph.getChildVertices(graph.getDefaultParent()).length < 1 )
            {
                return;
            }
            Object[] v = graph.getChildVertices(graph.getDefaultParent());
            for (int i=0; i< v.length; i++)
            {
                if (((mxCell)v[i]).getValue() instanceof UXor)
                {
                    Object[] e = graph.getOutgoingEdges(v[i]);
                    UXor xor = (UXor) ((mxCell)v[i]).getValue();
                    xor.clearExpressions();
                    if (xor.getXorType() == DecideType.TWO_WAY_BY_CHANCE.ordinal()
                        || xor.getXorType() == DecideType.N_WAY_BY_CHANCE.ordinal())
                    {
                        for (int j=0; j< e.length; j++)
                        {
                            if (((mxCell)e[j]).getValue() instanceof UXorBranch)
                            {
                                UXorBranch branch = (UXorBranch) ((mxCell)e[j]).getValue();
                                Template target = (Template) ((mxCell)e[j]).getTarget().getValue();
                                if (branch.getXorBranchType() == XorBranchType.True.ordinal())
                                {
                                    xor.putExpression(target,branch.getProbability()+"");
                                } else {
                                    xor.setTemplateFalse(target);
                                }
                                branch.setXorType(xor.getXorType());
                            }
                        }
                    }
                    else if (xor.getXorType() == DecideType.TWO_WAY_BY_CONDITION.ordinal()
                            || xor.getXorType() == DecideType.N_WAY_BY_CONDITION.ordinal())
                    {
                        for (int j=0; j< e.length; j++)
                        {
                            if (((mxCell)e[j]).getValue() instanceof UXorBranch)
                            {
                                UXorBranch branch = (UXorBranch) ((mxCell)e[j]).getValue();
                                Template target = (Template) ((mxCell)e[j]).getTarget().getValue();
                                if (branch.getXorBranchType() == XorBranchType.True.ordinal())
                                {
                                    xor.putExpression(target,branch.getExpression()+"");
                                } else {
                                    xor.setTemplateFalse(target);
                                }
                                branch.setXorType(xor.getXorType());
                            }
                        }
                    }else{
                    
                        //Criado por Jhonathan Camacho
                        if (xor.getXorType() == DecideType.TWO_WAY_BY_RNA.ordinal()){
                            
                            for (int j=0; j< e.length; j++){
                                
                                if (((mxCell)e[j]).getValue() instanceof UXorBranch){
                                    
                                    UXorBranch branch = (UXorBranch) ((mxCell)e[j]).getValue();
                                    Template target = (Template) ((mxCell)e[j]).getTarget().getValue();

                                    //System.out.println("AAAAAAAAAAAAAAAAAAAAA");
                                    xor.putExpression(target,branch.getNumberOfPath()+"");

                                    branch.setXorType(xor.getXorType());
                                }
                            }
                        }
                    
                    }

                }
            }
        }
        public void updateModel()
        {
            this.updateModelOnStart();
            this.updateModelOnAdd();
            this.updateModelOnDelete();
            this.updateXor();
            this.updateModelJumps();
            this.updateModelQueues();
            this.updateLines();
            
            
        }
    } // fim classe CustomGraph

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
        try {
            try
            {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                //UIManager.setLookAndFeel(new SynthLookAndFeel());
            }
            catch (Exception e1)
            {
                    e1.printStackTrace();
            }

            //mxConstants.SHADOW_COLOR = Color.LIGHT_GRAY;
            mxConstants.W3C_SHADOWCOLOR = "#D3D3D3";

    //mxCodecRegistry.addPackage("examples.jgraphx.swing");
    Preferences prefs = Preferences.userRoot().node("ururau");
    String localeSetting = prefs.get("locale", "en_US");
    StringTokenizer tk = new StringTokenizer(localeSetting,"_");
    String lang = "";
    String country = "";
    if (tk.hasMoreTokens())
        lang = tk.nextToken();
    if (tk.hasMoreTokens())
        country = tk.nextToken();
    Locale locale = new Locale(lang,country);
    //String jnapath = System.getProperty("jna.library.path");
    //System.out.println("JNA PATH: "+jnapath);
    
    final Field sysPathsField = ClassLoader.class.getDeclaredField("sys_paths");
    sysPathsField.setAccessible(true);
            try {
                sysPathsField.set(null, null);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(GraphEditor.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(GraphEditor.class.getName()).log(Level.SEVERE, null, ex);
            }
    
    mxResources.add("i18n.editor", locale);
    mxResources.add("i18n.exceptions", locale);
    System.setProperty("jna.library.path", prefs.get("jnaLibraryPath","C:\\Windows\\System32"));
    try {
        if (SystemUtils.IS_OS_WINDOWS) {
            
            File file = new File(System.getProperty("jna.library.path") + "\\" + "fannfloat.dll");
            System.out.println(file.getAbsolutePath());
            System.load(file.getAbsolutePath());
        } else {
            File file = new File(System.getProperty("jna.library.path") + "/"+"fannfloat.so");
            if (!file.exists())
            {
                file = new File(System.getProperty("jna.library.path") + "/"+"libfloatfann.so");
            }
            System.out.println(file.getAbsolutePath());
            System.load(file.getAbsolutePath());
        }
    } catch ( Exception e ){
        javax.swing.JDialog f=new javax.swing.JDialog();  
          f.setSize(250,150); 
          javax.swing.JOptionPane.showMessageDialog(f,mxResources.get("fannLibraryNotFound"), mxResources.get("warning"),javax.swing.JOptionPane.WARNING_MESSAGE);  
    } catch (UnsatisfiedLinkError e) {
        javax.swing.JDialog f=new javax.swing.JDialog();  
          f.setSize(250,150); 
          javax.swing.JOptionPane.showMessageDialog(f,mxResources.get("fannLibraryNotFound")+": "+e.getLocalizedMessage(), mxResources.get("warning"),
                  javax.swing.JOptionPane.WARNING_MESSAGE);          
    }
    java.util.Locale.setDefault(locale);
    mxCodecRegistry.addPackage("ururau");

    for (TemplateType tt : TemplateType.values())
    {
        mxCodecRegistry.register(new mxObjectCodec(
             TemplateFactory.getInstance(tt,false, true)));
    }
    
            GraphEditor editor = new GraphEditor();
            editor.createFrame(new EditorMenuBar(editor)).setVisible(true);
        } catch (NoSuchFieldException ex) {
            Logger.getLogger(GraphEditor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(GraphEditor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
