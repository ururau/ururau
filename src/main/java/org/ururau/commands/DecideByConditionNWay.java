/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;
import org.ururau.ErrorMsg;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

/**
 *
 * @author jhonathan
 */
public class DecideByConditionNWay extends DecideByCondition{
    
    private String myExpressionArray[];
    private int myCommandIndex[];

    public DecideByConditionNWay(ModelElement parent, String expression[], int commandIndex[],
            int jumpToCommandIndexIfFalse, String name) {
        super(parent, name, jumpToCommandIndexIfFalse);
        myExpressionArray = expression;
        myCommandIndex = commandIndex;
    }

    @Override
    public void execute() {
        
        Entity entity = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(entity))
            return;
       
        try {
                
                for(int i = 0; i < this.myExpressionArray.length; i++)
                {
                    double value = Expression.getValue(this.getModel(), entity, this.myExpressionArray[i]);
                    if (value != 0.0)
                    {
                        getProcessExecutor().jumpTo(this.myCommandIndex[i]);
                        return;
                    }
                }
                getProcessExecutor().jumpTo(this.getMyCommandIndexIfFalse());
            } catch (ParseException ex) {
                Logger.getLogger(Decide.class.getName()).log(Level.SEVERE, null, ex);
                //ErrorMsg.displayMsg("expression_error", myExpression, getName(), ex);
                //this.getModel().getReplication().end();
            }
        
    }
    
    
    
    
}