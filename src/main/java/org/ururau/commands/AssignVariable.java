/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Entity;
import jsl.modeling.elements.variable.Variable;
import org.ururau.ErrorMsg;
import org.ururau.IdentifierMap;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;
import org.ururau.variable.CustomVariable;
/**
 *
 * @author tulio
 */
public class AssignVariable extends ProcessCommand {
    String myVariableName;
    String myExpression;
    Variable myVariable;
    CustomVariable myResponseVariable;
    
    public AssignVariable(ModelElement parent, String variable, String expression, String name) {
        super(parent, name);
        myVariableName = variable;
        
        if( parent.getModel().getModelElement(variable) == null ){
            
            myVariable = new Variable(parent, variable);
            myResponseVariable = new CustomVariable(parent,variable+".RespVar");
             myResponseVariable.setInitialValue(0);
            
        }else{
            
            myVariable = (Variable) parent.getModel().getModelElement(variable);
            //myResponseVariable = new CustomVariable(parent.getModel().getModelElement(variable+".RespVar"));        
            myResponseVariable = (CustomVariable) parent.getModel().getModelElement(variable+".RespVar");
        }
        
        
        
       
        myResponseVariable.setDefaultReportingOption(true);
        myExpression = expression;
        java.util.StringTokenizer t = new java.util.StringTokenizer(name,".");
        if (t != null)
        {
            IdentifierMap.add(t.nextToken(), name);
        }
        
    }
    
    private double eval(Entity e) throws ParseException
    {
        if (myExpression  != null)
        {
            return Expression.getValue(this.getModel(), e, myExpression);
        }
        
        throw new UnsupportedOperationException("Not supported yet.");
    }
        
    @Override
    public void execute() {
        Entity e = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(e))
            return;

        Variable var;
        if (myVariable == null)
        {
            var = this.getVariable(myVariableName);
        } else {
            var = myVariable;
        }
        try {
            double value = this.eval(e);
            var.setValue(value);
            myResponseVariable.setValue(value);
            
        } catch (ParseException ex) {
            Logger.getLogger(AssignVariable.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("expression_error",myExpression, myVariableName, ex);
            this.getModel().stopReplication();
        }
    }
}
