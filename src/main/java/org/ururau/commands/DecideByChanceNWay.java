/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;
import jsl.utilities.random.distributions.DEmpirical;

/**
 *
 * @author jhonathan
 */
public class DecideByChanceNWay extends DecideByChance{
    
    public DecideByChanceNWay(ModelElement parent, double probability[], int commandIndex[],
            int jumpToCommandIndexIfFalse, String name) {
        super(parent, name, jumpToCommandIndexIfFalse);

        DEmpirical demp = new DEmpirical();
        for(int i=0; i < probability.length; i++)
        {
            demp.addProbabilityPoint(commandIndex[i], probability[i]);
        }
        demp.addLastProbabilityPoint((double)jumpToCommandIndexIfFalse);
        this.setMyDecideByChanceRV(parent, demp);
    }

    @Override
    public void execute() {
        
        Entity entity = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(entity))
            return;
        
        
        int value = (int)this.getMyDecideByChanceRV().getValue();
            getProcessExecutor().jumpTo(value);
            return;
        
    }
    
    
    

}
