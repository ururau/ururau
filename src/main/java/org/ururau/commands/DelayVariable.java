/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import invapibasics40library.ApiComClass;
import invapibasics40library.IApiComClass;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;
import jsl.modeling.elements.variable.Variable;
import net.sf.jni4net.Bridge;

/**
 *
 * @author jhonathan
 */
public class DelayVariable extends Delay{
    
    private Variable myDelay;
  
    public DelayVariable(ModelElement parent, Variable delay) {
        this(parent, delay, null);	
    }
   
    public DelayVariable(ModelElement parent, Variable delay, String name)  {
        super(parent, name);
	
        this.setDelay(delay);	
    }
    
    public Variable getDelay() {

        return this.myDelay;
    }

    public void setDelay(Variable delay) {

        if(delay == null)	
            throw new IllegalArgumentException("Variable delay must be non-null!");
	
        this.myDelay = delay;
    }

    @Override
    public void execute() {
        
        Entity entity = getProcessExecutor().getCurrentEntity();
        entity.setProcessExecutor(getProcessExecutor());
        
        if (!Batch.isVisible(entity))
            return;

        
        double t = 0;
        
        if (this.myDelay != null){

            t = this.myDelay.getValue();
            
            t = (t<0.0)?0:t;
        }
        super.executeCommon(t);
    }

    
    
    
}