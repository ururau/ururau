/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import invapibasics40library.ApiComClass;
import invapibasics40library.IApiComClass;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;
import jsl.modeling.elements.variable.Variable;
import net.sf.jni4net.Bridge;

/**
 *
 * @author jhonathan
 */
public class DelayVariableInventor extends Delay{
    
    private Variable myDelay;
    private IApiComClass myApi; 
    public DelayVariableInventor(ModelElement parent, Variable delay) {
        this(parent, delay, null);	
    }
   
    public DelayVariableInventor(ModelElement parent, Variable delay, String name) {
        super(parent, name);
	
        this.setDelay(delay);	
    }
    
    public Variable getDelay() throws IOException {
        File file = new File ("net/lib/jni4net.n.w64.v40-0.8.8.0.dll").getAbsoluteFile();
        Bridge.init(file);
        System.out.println("Load Assembly...");
        Bridge.LoadAndRegisterAssemblyFrom(new java.io.File("net/lib/InvApiBasics40Library.j4n.dll"));
        ApiComClass myApi = new ApiComClass();
        myApi.AppLoad();
        return this.myDelay;
    }

    public void setDelay(Variable delay) {

        if(delay == null)	
            throw new IllegalArgumentException("Variable delay must be non-null!");
	
        this.myDelay = delay;
    }

    @Override
    public void execute() {
        
        Entity entity = getProcessExecutor().getCurrentEntity();
        entity.setProcessExecutor(getProcessExecutor());
        
        if (!Batch.isVisible(entity))
            return;

        
        double t = 0;
        
        if (this.myDelay != null){

            t = this.myDelay.getValue();
            t = (t<0.0)?0:t;            
                int maxSteps = myApi.GetSimulationNumberOfSteps();
            
            double ts = myApi.GetSimulationLength(1, maxSteps);
            if (ts > t)
            {
                try {
                    throw new Exception("Ururau expression simulator time is more then Inventor simulation time");
                } catch (Exception ex) {
                    Logger.getLogger(DelayVariable.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            int steps = (int) (maxSteps*(t/ts));
            if (myApi != null){
                System.out.println("Run Inventor Simulation ...");
                t = myApi.RunSimulation(1, steps);
                t = (t<0.0)?0:t;
                System.out.println("t = "+t);
            }        

        }

        super.executeCommon(t);
    }

    
    
    
}