/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;
import org.ururau.ErrorMsg;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

/**
 *
 * @author jhonathan
 */
public class DecideByConditionTwoWay extends DecideByCondition{
    
    private int myCommandIndexIfTrue = -1;
    private String myExpression;
    
    
    public DecideByConditionTwoWay(ModelElement parent, String expression, int jumpToCommandIndexIfTrue,
            int jumpToCommandIndexIfFalse, String name)
    {
        super(parent, name, jumpToCommandIndexIfFalse);

        myCommandIndexIfTrue = jumpToCommandIndexIfTrue;
        myExpression = expression;
    }

    @Override
    public void execute() {
        
        Entity entity = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(entity))
            return;
        
        try {
                double value = Expression.getValue(this.getModel(), entity, this.myExpression);
                if (value != 0.0)
                    getProcessExecutor().jumpTo(this.myCommandIndexIfTrue);
                else
                    getProcessExecutor().jumpTo(this.getMyCommandIndexIfFalse());
            } catch (ParseException ex) {
                Logger.getLogger(Decide.class.getName()).log(Level.SEVERE, null, ex);
                ErrorMsg.displayMsg("expression_error", myExpression, "expression", ex);
                //this.getModel().getReplication().end();
            }
        
    }
    
    

    
}
