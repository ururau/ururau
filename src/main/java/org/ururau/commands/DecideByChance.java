/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import jsl.modeling.ModelElement;
import jsl.modeling.elements.variable.RandomVariable;
import jsl.utilities.random.distributions.DEmpirical;

/**
 *
 * @author jhonathan
 */
public abstract class DecideByChance extends Decide{

    private int myCommandIndexIfFalse;
    private RandomVariable myDecideByChanceRV;

    public DecideByChance(ModelElement parent, String name, int myCommandIndexIfFalse) {
        super(parent, name);
        
        this.myCommandIndexIfFalse = myCommandIndexIfFalse;
    }

    
    
    protected void setMyDecideByChanceRV(ModelElement parent, DEmpirical demp){
        
        this.myDecideByChanceRV = new RandomVariable(parent, demp);
    }
    
    protected RandomVariable getMyDecideByChanceRV(){
        
        return this.myDecideByChanceRV;
    }
        
    
    protected  int getMyCommandIndexIfFalse(){
    
        return this.myCommandIndexIfFalse;
    }
    
    
    
}