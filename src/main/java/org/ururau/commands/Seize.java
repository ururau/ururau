/*
 * Copyright (c) 2007, Manuel D. Rossetti (rossetti@uark.edu)
 *
 * Contact:
 *	Manuel D. Rossetti, Ph.D., P.E.
 *	Department of Industrial Engineering
 *	University of Arkansas
 *	4207 Bell Engineering Center
 *	Fayetteville, AR 72701
 *	Phone: (479) 575-6756
 *	Email: rossetti@uark.edu
 *	Web: www.uark.edu/~rossetti
 *
 * This file is part of the JSL (a Java Simulation Library). The JSL is a framework
 * of Java classes that permit the easy development and execution of discrete event
 * simulation programs.
 *
 * The JSL is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The JSL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the JSL (see file COPYING in the distribution);
 * if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA, or see www.fsf.org
 *
 */
package org.ururau.commands;

import java.util.ArrayList;
import java.util.List;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;

import jsl.modeling.elements.processview.description.ProcessExecutor;
import jsl.modeling.elements.queue.*;
import jsl.modeling.elements.resource.*;
import jsl.modeling.elements.variable.*;

/**
 *
 */
public class Seize extends ProcessCommand {

    private Variable myAmtRequested;
	
	private Resource myResource;
	
	private int myPriority;
	
	private UQueue myQueue;
        
        private boolean myWaitStats;
        
        private boolean mySizeStats;

	public Seize(ModelElement parent, Variable amountRequested, Resource resource, UQueue queue){
		this(parent, amountRequested, resource, queue, 1, null);
	}	
	
	public Seize(ModelElement parent, Variable amountRequested, Resource resource, UQueue queue, int priority){
		this(parent, amountRequested, resource, queue, priority, null);
	}	

	public Seize(ModelElement parent, Variable amountRequested, Resource resource, UQueue queue, String name){
		this(parent, amountRequested, resource, queue, 1, name);
	}
        
	public Seize(ModelElement parent, Variable amountRequested, Resource resource, UQueue queue, int priority, String name) {
            this(parent, amountRequested, resource, queue, priority,true, true, name);
        }
        
	public Seize(ModelElement parent, Variable amountRequested, Resource resource, UQueue queue, int priority, boolean waitStats, String name) {
            this(parent, amountRequested, resource, queue, priority,waitStats, true, name);
        }
        
	public Seize(ModelElement parent, Variable amountRequested, Resource resource, UQueue queue, int priority, boolean waitStats, boolean sizeStats, String name) {
        
		super(parent, name);
		setAmountRequested(amountRequested);
		setResource(resource);
		setQueue(queue);
		setPriority(priority);
                setWaitStats(waitStats);
                setSizeStats(sizeStats);
  
	}
	
	/** Gets the queueing priority associated with this QObject
	 * @return The priority as an int
	 */    
	public final int getPriority() {
		return(myPriority);
	}

    /** Returns the queue that the QObject was last enqueued within
     *  
     * @return The UQueue, or null if no queue
    */ 
	public final UQueue getQueue(){
		return(myQueue);
	}
	
	/* (non-Javadoc)
	 * @see jsl.modeling.elements.processview.description.ProcessCommand#execute()
	 */
	public void execute() {
		
		int amt = (int)myAmtRequested.getValue();
		Entity entity = getProcessExecutor().getCurrentEntity();
		if (!Batch.isVisible(entity))
                    return;
		// create the request on the resource
		Request request = Request.create(amt, getPriority(), entity);
                
		// enqueue arriving customer
		myQueue.enqueue(request, getPriority(),mySizeStats);
                
                if (myWaitStats == false) {
                    getWaitStats();
                }
		if (myResource.isIdle()){
			Request r = (Request)myQueue.peekNext();
			if (r == request){
				if (r.getInitialAmountRequested() <= myResource.getNumberIdle()){
					myQueue.removeNext(myWaitStats, mySizeStats);
					myResource.allocate(r);
				}				
			}
		}
		
		// check if request was queued
		if (request.isQueued()){ // suspend the executor at this seize command, otherwise continue
			getProcessExecutor().suspend();
		} 
	}
	
    /** Sets the queueing priority for this Seize
     *  Changing the priority while the object is in a queue
     *  has no effect on the ordering of the queue.  This priority is
     *  only used to determine the ordering in the queue when the item
     *  enters the queue.
     * 
     * @param priority lower priority implies earlier ranking in the queue
     */
	protected final void setPriority(int priority){
		myPriority = priority;
	}

        public boolean getWaitStats() {
            return myWaitStats;
        }

        public void setWaitStats(boolean waitStats) {
            this.myWaitStats = waitStats;
        }

        public boolean getSizeStats() {
            return mySizeStats;
        }

        public void setSizeStats(boolean sizeStats) {
            this.mySizeStats = sizeStats;
        }

	protected void setAmountRequested(Variable amountRequested){
		
    	if (amountRequested == null)
    		throw new IllegalArgumentException("Variable amountRequested was equal to null!");
    	
    	if (amountRequested.getInitialValue() <= 0)
    		throw new IllegalArgumentException("Amount requested was less or equal to zero!");
    	
    	myAmtRequested = amountRequested;
	}

	protected void setResource(Resource resource){
    	if (resource == null)
    		throw new IllegalArgumentException("Resource was equal to null!");
    	myResource = resource;
	}
	
	protected void setQueue(UQueue queue){
    	if (queue == null)
    		throw new IllegalArgumentException("Queue was equal to null!");
    	myQueue = queue;
	}
}
