/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.commands;

import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Entity;
import jsl.modeling.elements.variable.RandomVariable;
import jsl.modeling.elements.variable.Variable;
import jsl.utilities.random.distributions.DEmpirical;
import org.ururau.ErrorMsg;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

/**
 *
 * @author tulio
 */

public class Decide extends ProcessCommand {
    private int myCommandIndexIfFalse;
    private int myCommandIndexIfTrue = -1;
    private DecideType myDecideType;
    private String myExpression;
    private String myExpressionArray[];
    RandomVariable myDecideByChanceRV;
    int myCommandIndex[];

    public static enum DecideType {TWO_WAY_BY_CHANCE, N_WAY_BY_CHANCE, 
                                   TWO_WAY_BY_CONDITION, N_WAY_BY_CONDITION, EXTERNAL, TWO_WAY_BY_RNA};

    public Decide(ModelElement parent, String name) {
       super(parent, name);
    }
    
    public Decide(ModelElement parent, double probability, int jumpToCommandIndexIfFalse) {
        this(parent, probability, jumpToCommandIndexIfFalse, null);
    }

    /**
     * Decide 2-way by chance
     *
     * @param parent
     * @param probability       probability to continue to next command
     * @param jumpToCommandIndexIfFalse jumps to line command i with (1-prob) chance to happen
     * @param name
     */
    public Decide(ModelElement parent, double probability, int jumpToCommandIndexIfFalse, String name) {
        this(parent, probability, -1, jumpToCommandIndexIfFalse, name);
    }

    public Decide(ModelElement parent, double probability, int jumpToCommandIndexIfTrue,
            int jumpToCommandIndexIfFalse, String name) {
        super(parent, name);
        myDecideType = DecideType.TWO_WAY_BY_CHANCE;
        myCommandIndexIfFalse = jumpToCommandIndexIfFalse;
        myCommandIndexIfTrue = jumpToCommandIndexIfTrue;
        DEmpirical demp = new DEmpirical();
        demp.addProbabilityPoint(1, probability);
        demp.addLastProbabilityPoint(0);
        myDecideByChanceRV = new RandomVariable(parent, demp);
    }

    public Decide(ModelElement parent, double probability[], int commandIndex[],
            int jumpToCommandIndexIfFalse, String name) {
        super(parent, name);
        myDecideType = DecideType.N_WAY_BY_CHANCE;
        myCommandIndexIfFalse = jumpToCommandIndexIfFalse;
        DEmpirical demp = new DEmpirical();
        for(int i=0; i < probability.length; i++)
        {
            demp.addProbabilityPoint(commandIndex[i], probability[i]);
        }
        demp.addLastProbabilityPoint((double)jumpToCommandIndexIfFalse);
        myDecideByChanceRV = new RandomVariable(parent, demp);
    }
    
    public Decide(ModelElement parent, String expression, int jumpToCommandIndexIfTrue,
            int jumpToCommandIndexIfFalse, String name)
    {
        super(parent, name);
        myDecideType = DecideType.TWO_WAY_BY_CONDITION;
        myCommandIndexIfFalse = jumpToCommandIndexIfFalse;
        myCommandIndexIfTrue = jumpToCommandIndexIfTrue;
        myExpression = expression;
    }
    
    public Decide(ModelElement parent, String expression[], int commandIndex[],
            int jumpToCommandIndexIfFalse, String name) {
        super(parent, name);
        myDecideType = DecideType.N_WAY_BY_CHANCE;
        myCommandIndexIfFalse = jumpToCommandIndexIfFalse;
        myExpressionArray = expression;
        myCommandIndex = commandIndex;
    }
        
    public void execute() {
       Entity entity = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(entity))
            return;
        if (myDecideType == DecideType.TWO_WAY_BY_CHANCE)
        {
            double value = myDecideByChanceRV.getValue();
            if (value == 0) {
                getProcessExecutor().jumpTo(myCommandIndexIfFalse);
            } else {
                if (myCommandIndexIfTrue != -1)
                {
                    getProcessExecutor().jumpTo(myCommandIndexIfTrue);
                }
            }
            return;
        }
        else if (myDecideType == DecideType.N_WAY_BY_CHANCE)
        {
            int value = (int)myDecideByChanceRV.getValue();
            getProcessExecutor().jumpTo(value);
            return;
        }

        else if (myDecideType == DecideType.TWO_WAY_BY_CONDITION) {
            try {
                double value = Expression.getValue(this.getModel(), entity, myExpression);
                if (value != 0.0)
                    getProcessExecutor().jumpTo(myCommandIndexIfTrue);
                else
                    getProcessExecutor().jumpTo(myCommandIndexIfFalse);
            } catch (ParseException ex) {
                Logger.getLogger(Decide.class.getName()).log(Level.SEVERE, null, ex);
                ErrorMsg.displayMsg("expression_error", myExpression, "expression", ex);
                this.getModel().stopReplication();
            }
        }
        else if (myDecideType == DecideType.N_WAY_BY_CONDITION) {
            try {
                
                for(int i = 0; i < myExpressionArray.length; i++)
                {
                    double value = Expression.getValue(this.getModel(), entity, myExpressionArray[i]);
                    if (value != 0.0)
                    {
                        getProcessExecutor().jumpTo(myCommandIndex[i]);
                        return;
                    }
                }
                getProcessExecutor().jumpTo(myCommandIndexIfFalse);
            } catch (ParseException ex) {
                Logger.getLogger(Decide.class.getName()).log(Level.SEVERE, null, ex);
                ErrorMsg.displayMsg("expression_error", myExpression, getName(), ex);
                this.getModel().stopReplication();
            }
            
        } else {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}