/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.commands;

import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.MacroCommand;
import jsl.modeling.elements.queue.Queue;

/**
 *
 * @author tulio
 */
public class Hold extends MacroCommand {

    public Hold(ModelElement parent, Queue queue, String expression) {
        this(parent, queue, expression, null);
    }

    public Hold(ModelElement parent, Queue queue, String expression, String name) {
        super(parent, name);
        this.addSubCommand(new PreHold(parent, queue, expression));
        this.addSubCommand(new Free(parent, expression));
    }
}