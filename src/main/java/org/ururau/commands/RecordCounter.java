/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;
import jsl.modeling.elements.variable.Counter;
import org.ururau.ErrorMsg;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

/**
 *
 * @author jhonathan
 */
public class RecordCounter extends Record{
    
    private Counter myCounter;
    
    public RecordCounter(ModelElement parent, String expression) {
        this(parent, expression, null);
    }
    
    public RecordCounter(ModelElement parent, String expression, String name) {
        super(parent, name, expression);
        
        this.myCounter = new Counter(parent,getName()+".RecordCounterVar");
        this.myCounter.setDefaultReportingOption(true);
    }

    @Override
    public void execute() {
        Entity e = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(e))
            return;

        try {
        
            this.myCounter.increment((long)Expression.getValue(this.getModel(), e, this.getMyExpression())) ;
            
        } catch (ParseException ex) {
        
            Logger.getLogger(Record.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("expression_error", this.getMyExpression(), getName(), ex);
            this.getModel().stopReplication();    
        }        

        return;
    }
    

    

}
