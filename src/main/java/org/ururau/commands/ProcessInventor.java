/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.commands;

import java.io.IOException;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.queue.UQueue;
import jsl.modeling.elements.resource.Resource;
import jsl.modeling.elements.variable.Variable;
import jsl.modeling.elements.variable.RandomVariable;
import org.ururau.description.MacroCommand;

/**
 *
 * @author tulio
 */
public class ProcessInventor extends MacroCommand {
    
    public ProcessInventor(ModelElement parent, Variable v,
                   Resource r, UQueue q, RandomVariable rv) {
        this(parent,v,r,q,rv,null);
    }

    public ProcessInventor(ModelElement parent, Variable v,
                   Resource r, UQueue q, String exp) throws IOException {
        this(parent,v,r,q,exp,null);
    }

    public ProcessInventor(ModelElement parent, Variable v,
                   Resource r, UQueue q, RandomVariable rv, String name) {
        super(parent, name+".process");
        this.addSubCommand(new Seize(parent, v, r, q));
        this.addSubCommand(new DelayVariableInventor(parent, rv));
        this.addSubCommand(new Release(parent, r, q));
    }
    
    public ProcessInventor(ModelElement parent, Variable v,
                   Resource r, UQueue q, String exp, String name) throws IOException {
        super(parent, name+".process");
        this.addSubCommand(new Seize(parent, v, r, q));
        this.addSubCommand(new DelayExpressionInventor(parent, exp));
        this.addSubCommand(new Release(parent, r, q));
    }
    public ProcessInventor(ModelElement parent, Variable v,
                   Resource r, UQueue q, int minStep, int maxStep, String name) throws IOException {
        super(parent, name+".process");
        this.addSubCommand(new Seize(parent, v, r, q));
        this.addSubCommand(new DelayStepInventor(parent, minStep, maxStep));
        this.addSubCommand(new Release(parent, r, q));
    }
}
