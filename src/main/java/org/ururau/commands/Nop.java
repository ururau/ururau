/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.commands;

import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;

/**
 *
 * @author tulio
 */
public class Nop extends ProcessCommand {

    public Nop(ModelElement parent) {
        this(parent, null);
    }
    public Nop(ModelElement parent, String name) {
        super(parent, name);
    }

    public void execute() {
        
    }
}
