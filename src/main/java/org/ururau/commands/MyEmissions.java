/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Attribute;
import jsl.modeling.elements.resource.Entity;
import jsl.modeling.elements.variable.Variable;
import org.ururau.ErrorMsg;
import org.ururau.IdentifierMap;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;
import org.ururau.variable.CustomVariable;

/**
 *
 * @author Fabio, Eder
 */
public class MyEmissions extends ProcessCommand {
    String myTime;
    String myEc;
    String myPower;
    String myPbt;
    String myLoad;
    String myPercentage;
    String myExpression;
    String myVariableName;
    Variable myVariable;
    CustomVariable myResponseVariable;
    
    public MyEmissions (ModelElement parent, String variable, double ec, double power, String time, double load, double pbt, double percentage, String name) {
        super(parent, name);
        myVariableName = variable;
        
         if( parent.getModel().getModelElement(variable) == null ){
            
            myVariable = new Variable(parent, variable);
            myResponseVariable = new CustomVariable(parent,variable+".RespVar");
             myResponseVariable.setInitialValue(0);
            
        }else{
            
            myVariable = (Variable) parent.getModel().getModelElement(variable);
            //myResponseVariable = new CustomVariable(parent.getModel().getModelElement(variable+".RespVar"));   
            myResponseVariable = (CustomVariable) parent.getModel().getModelElement(variable+".RespVar");
        }
       
       
        myResponseVariable.setDefaultReportingOption(true);
        //
        myEc = ec+"";
        myPower=power+"";
        myLoad=load+"";
        myPbt=pbt+"";
        myPercentage=percentage+"";
        myTime=time+"";
        java.util.StringTokenizer t = new java.util.StringTokenizer(name,".");
        if (t != null)
        {
            IdentifierMap.add(t.nextToken(), name);
        }
        
    }
    
    private double eval(Entity e, String expression) throws ParseException
    {
        if (expression  != null)
        {
            return Expression.getValue(this.getModel(), e, expression);
        }
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
     @Override
    public void execute() {
        Entity e = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(e))
            return;
          Variable var;
        if (myVariable == null)
        {
            var = this.getVariable(myVariableName);
        } else {
            var = myVariable;
        }
        try {
            double t = this.eval(e,myTime);
             if(e.containsAttribute("EMISSION_LEAD_TIME") == false){
                e.addAttribute("EMISSION_LEAD_TIME", new Attribute(t,"EMISSION_LEAD_TIME"));
            } else { 
                 double eLeadtime = e.getAttributeValue("EMISSION_LEAD_TIME");
                e.setAttributeValue("EMISSION_LEAD_TIME", eLeadtime+t);
             }
            myExpression = "("+myEc+"*"+myPercentage+"/"+100+")*"+myPower+"*"+t+"*"+"("+myLoad+"/"+myPbt+")";
            double value = this.eval(e,myExpression);
            var.setValue(var.getValue()+value);
            
            myResponseVariable.setValue(var.getValue());
            
        scheduleResume(getProcessExecutor(), t, 1, "Resume Delay " + getName());
            getProcessExecutor().suspend();
        } catch (ParseException ex) {
            Logger.getLogger(MyEmissions.class.getName()).log(Level.SEVERE, null, ex);
           
            this.getModel().stopReplication();
        }
    }
}
