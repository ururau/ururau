/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import invapibasics40library.ApiComClass;
import invapibasics40library.IApiComClass;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jni4net.Bridge;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;
import jsl.modeling.elements.variable.Variable;

/**
 *
 * @author jhonathan
 */
public class DelayStepInventor extends Delay{
    
    private int myMinStep;
    private int myMaxStep;
    private IApiComClass myApi;   
    public DelayStepInventor(ModelElement parent, int minStep, int maxStep) throws IOException {
        this(parent, minStep, maxStep, null);	
    }
   
    public DelayStepInventor(ModelElement parent, int minStep, int maxStep, String name) throws IOException {
        super(parent, name);
        File file = new File ("net/lib/jni4net.n.w64.v40-0.8.8.0.dll").getAbsoluteFile();
	Bridge.init(file);
        System.out.println("Load Assembly...");
        Bridge.LoadAndRegisterAssemblyFrom(new java.io.File("net/lib/InvApiBasics40Library.j4n.dll"));
        myApi = new ApiComClass();
	myApi.AppLoad();
        this.setDelay(minStep, maxStep);	
    }
    
    public int getMinStep() {

        return this.myMinStep;
    }

    public int getMaxStep() {

        return this.myMaxStep;
    }

    public void setDelay(int minStep, int maxStep) {

        if(minStep <= 0 || maxStep <=0)	
            throw new IllegalArgumentException("minStep and maxStep must be positive!");
	
        this.myMinStep = minStep;
        this.myMaxStep = maxStep;
    }

    @Override
    public void execute() {
        
        Entity entity = getProcessExecutor().getCurrentEntity();
        entity.setProcessExecutor(getProcessExecutor());
        
        if (!Batch.isVisible(entity))
            return;

        
        double t = 0;
        
        if (myApi != null){
            System.out.println("Run Inventor Simulation ...");
            t = myApi.RunSimulation(myMinStep, myMaxStep);
            t = (t<0.0)?0:t;
            System.out.println("t = "+t);
        }

        super.executeCommon(t);
    }

    
    
    
}