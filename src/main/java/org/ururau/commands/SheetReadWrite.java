/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import com.mxgraph.util.mxResources;
import java.util.List;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Entity;
import org.apache.poi.ss.usermodel.Row;
import org.ururau.ReadWriteVariable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.variable.Variable;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellReference;
import org.ururau.variable.CustomVariable;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fábio
 */
public class SheetReadWrite extends ProcessCommand  {
    

    private List<ReadWriteVariable> myVariable;
    String myExpression;
    String myFilename;
    public SheetReadWrite(ModelElement parent, List<ReadWriteVariable> variable , String filename){
        this(parent,variable,filename,null);}
   
    
    public SheetReadWrite(ModelElement parent, List<ReadWriteVariable> variable , String filename, String name) {
        super(parent, name);
        this.myFilename=filename;
        this.myVariable = variable;
        for (int i =0; i < variable.size();  i++)
        {
            if (parent.getModel().getModelElement(variable.get(i).getVariableName())== null)
            {
                new Variable(parent.getModel(), variable.get(i).getVariableName());
                new CustomVariable(parent.getModel(), variable.get(i).getVariableName()+".RespVar");   
            }
        }
    }
    
    
    @Override 
    public void execute(){
        String variable;
        try {
            Entity entity = getProcessExecutor().getCurrentEntity();
            entity.setProcessExecutor(getProcessExecutor());
            if (!Batch.isVisible(entity))
               return;
                 
            FileInputStream file = null;
                 file = new FileInputStream(myFilename);
                 Workbook wb = WorkbookFactory.create(file);
                 Sheet sheet1 = wb.getSheetAt(0);
                 for (int i=0; i < myVariable.size(); i++)
                 {
                     
                     String inputVarName = myVariable.get(i).getVariableName();
                     Variable v = this.getVariable(inputVarName);

                     ///String cname = "A3";
                     String cname =  myVariable.get(i).getCellName();
                     double value =0;

                     if (!myVariable.get(i).isWriteToFile()) {
                         if (wb.getName(cname) != null)
                         {
                             String refersToFormula = wb.getName(cname).getRefersToFormula();
                             CellReference cellRef = new CellReference(refersToFormula);
                             Cell c = sheet1.getRow(cellRef.getRow()).getCell(cellRef.getCol());
                              value = c.getNumericCellValue();
                         } else {
                             CellReference cellRef = new CellReference(cname);
                             if (cellRef != null)
                             {
                                 short col = cellRef.getCol();
                                 int row = cellRef.getRow();
                                 Row row1 = sheet1.getRow(row);
                                 if ((row1 == null) || (row1.getCell(col, MissingCellPolicy.RETURN_BLANK_AS_NULL) == null))
                                 {
                                    JOptionPane.showMessageDialog(null, "Celula vazia "+cname, "Read/Write", JOptionPane.ERROR_MESSAGE);
                                    file.close();
                                    wb.close();
                                    this.stopReplication();
                                    //this.getSimulation().stop("Cell value is null"); // as defined in jsl-core
                                    //this.getSimulation().end("Cell value is null");
                                    return;
                                 }
                                 Cell c = row1.getCell(col, MissingCellPolicy.RETURN_BLANK_AS_NULL);
                                 value = c.getNumericCellValue();
                             }
                         }
                         ModelElement modelElement = this.getModel().getModelElement(v.getName()+".RespVar");
                         if (v != null)
                         {       
                             v.setValue(value);
                             ((CustomVariable)modelElement).setValue(value);
                         }
                     } else {
                     
                       if (v != null)
                       {
                              
                               value = v.getValue();
                               ModelElement modelElement = this.getModel().getModelElement(v.getName()+".RespVar");
                               ((CustomVariable)modelElement).setValue(value);
                               if (wb.getName(cname) != null)
                               {
                                    String refersToFormula = wb.getName(cname).getRefersToFormula();
                                    CellReference cellRef = new CellReference(refersToFormula);
                                    Cell c = sheet1.getRow(cellRef.getRow()).getCell(cellRef.getCol());
                                    c.setCellValue(value);
                                } else {
                                    CellReference cellRef = new CellReference(cname);
                                    if (cellRef != null)
                                    {
                                        Cell c = sheet1.getRow(cellRef.getRow()).getCell(cellRef.getCol(), MissingCellPolicy.CREATE_NULL_AS_BLANK);
                                        c.setCellValue(value);
                                    }
                                } 
                             }
                       
                    }
                 }
         
                 
            file.close();
            FileOutputStream fileOut = new FileOutputStream(myFilename);
            wb.write(fileOut);
            fileOut.close();
            wb.close();

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getLocalizedMessage(), mxResources.get("modelError") + ": "+this.getName(), JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(SheetReadWrite.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidFormatException ex) {
            JOptionPane.showMessageDialog(null, ex.getLocalizedMessage(), mxResources.get("modelError") + ": "+this.getName(), JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(SheetReadWrite.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EncryptedDocumentException ex) {
            JOptionPane.showMessageDialog(null, ex.getLocalizedMessage(), mxResources.get("modelError") + ": "+this.getName(), JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(SheetReadWrite.class.getName()).log(Level.SEVERE, null, ex);
        } 
         
    } 
    @Override
    public void afterExperiment()
    {
    }         
 }