/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;

/**
 *
 * @author jhonathan
 */
public class RecordTimeBetween extends RecordTime{
    
    private double lastTime = 0;
    
    public RecordTimeBetween(ModelElement parent, String expression){
        super(parent, expression);
    }
    
    public RecordTimeBetween(ModelElement parent, String expression, String name) {
        super(parent, expression, name);
    }

    @Override
    public void execute() {

        Entity e = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(e))
            return;
        
        double between = getTime() - this.lastTime;
        this.lastTime = getTime();
        this.getMyResponse().setValue(between);
        return;        
    }
    
    @Override
    protected void beforeReplication() {
        super.beforeReplication();
        this.lastTime = 0;
    }
    
    

}
