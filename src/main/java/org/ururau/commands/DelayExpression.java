/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Attribute;
import jsl.modeling.elements.resource.Entity;
import jsl.modeling.elements.variable.ResponseVariable;
import org.ururau.ErrorMsg;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;
import org.ururau.variable.CustomVariable;

/**
 *
 * @author jhonathan
 */
public class DelayExpression extends Delay{

        private String myExpression;
        private CustomVariable myTotalDelay;
        
        public DelayExpression(ModelElement parent, String expression) {	
            this(parent, expression, null);
	}
	
        /**
	 * 
	 */
	public DelayExpression(ModelElement parent, String expression, String name) {	
            super(parent, name);
	
            this.setDelay(expression);

        }
	
       
        /**
	 * @param delay The delay to set.
	 */
	public void setDelay(String expression) {
		
            if(expression == null)
                throw new IllegalArgumentException("Expression delay must be non-null!");
		
            this.myExpression = expression;
	}

    @Override
    public void execute() {
    
        Entity entity = getProcessExecutor().getCurrentEntity();
        entity.setProcessExecutor(getProcessExecutor());
        if (!Batch.isVisible(entity)) 
            return;
        double t = 0;
        
        try {
            t = Expression.getValue(this.getModel(), entity, this.myExpression);
            t = (t<0.0)?0:t;
            
        } catch (ParseException ex) {
        
            Logger.getLogger(Delay.class.getName()).log(Level.SEVERE, "An error in expression: "+ this.myExpression, ex);
            ErrorMsg.displayMsg("expression_error", this.myExpression, getName(), ex);
            this.getModel().stopReplication();  
        }
        
        super.executeCommon(t);
    }   
}
