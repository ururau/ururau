package org.ururau.commands;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Entity;
import jsl.modeling.elements.variable.Variable;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fábio
 */
public class MyWrite extends ProcessCommand  {
    private String expression;
    public static enum MyWriteType {VARIABLE,ATTRIBUTE};
    
    private MyWriteType myWriteType;
    String myExpression;
    BufferedWriter myWriter;
    String myFilename;
    public MyWrite(ModelElement parent,MyWriteType wtype, String filename, String expression){
        this(parent,wtype,filename,expression,null);}
   
    
    public MyWrite(ModelElement parent, MyWriteType wtype, String filename, String expression, String name) {
        super(parent, name);
        this.myExpression=expression;
        this.myFilename=filename;
        this.myWriteType= wtype;
        try {
            myWriter= new BufferedWriter (new OutputStreamWriter(new FileOutputStream(filename),"utf-8"));
            
        } catch (IOException ex){ Logger.getLogger(MyWrite.class.getName()).log(Level.SEVERE, "An error to write file"+myFilename,ex);}
        
    }
    
    
    @Override 
    public void execute(){
        try{
            Entity entity = getProcessExecutor().getCurrentEntity();
            entity.setProcessExecutor(getProcessExecutor());
            if (!Batch.isVisible(entity))
                return;
             double value = 0.0;
              if (myWriteType == MyWriteType.ATTRIBUTE)
            {
                    value = entity.getAttributeValue(myExpression);
            } 
            else if (myWriteType == MyWriteType.VARIABLE)
            {
                Variable var = (Variable) this.getModel().getModelElement(myExpression);
                value = var.getValue();
            }
            myWriter.write(value+"\n");
        } catch (IOException ex) {
            Logger.getLogger(MyWrite.class.getName()).log(Level.SEVERE, "An error to write value to file: "+myFilename, ex);

        }
    } 
    @Override
    public void afterExperiment()
    {
        try {
            super.afterExperiment();
            myWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(MyWrite.class.getName()).log(Level.SEVERE, "An error to close file: "+myFilename, ex);
        }
    }
             
} 
    
    
        


