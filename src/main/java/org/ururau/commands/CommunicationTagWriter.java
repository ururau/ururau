/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import org.ururau.IdentifierMap;

/**
 *
 * @author tulio
 */
public abstract class CommunicationTagWriter extends ProcessCommand  {
   
    private String myTagName;
    private String myExpression;
    
    
    public static enum TagType {BOOLEAN, INTERGER, REAL};
    
    public CommunicationTagWriter(ModelElement parent, String tag, String expression, String name) {
        super(parent, name);
        myExpression = expression;
        myTagName = tag;

        java.util.StringTokenizer t = new java.util.StringTokenizer(name,".");
        if (t != null)
        {
            IdentifierMap.add(t.nextToken(), name);
        }

    }
    
    protected String getMyTagName(){
    
        return this.myTagName;
    }
    
    protected String getMyExpression(){
    
        return this.myExpression;
    }
    
    @Override
    public abstract void execute();
}

