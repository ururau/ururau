/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import invapibasics40library.ApiComClass;
import invapibasics40library.IApiComClass;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;
import net.sf.jni4net.Bridge;
import org.ururau.ErrorMsg;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

/**
 *
 * @author jhonathan
 */
public class DelayExpressionInventor extends Delay{

        private String myExpression;
	private IApiComClass myApi;   


        public DelayExpressionInventor(ModelElement parent, String expression) throws IOException {	
            this(parent, expression, null);
	}
	
        /**
	 * 
	 */
	public DelayExpressionInventor(ModelElement parent, String expression, String name) throws IOException {	
            super(parent, name);
            File file = new File ("net/lib/jni4net.n.w64.v40-0.8.8.0.dll").getAbsoluteFile();
            Bridge.init(file);
            System.out.println("Load Assembly...");
            Bridge.LoadAndRegisterAssemblyFrom(new java.io.File("net/lib/InvApiBasics40Library.j4n.dll"));
            myApi = new ApiComClass();
            myApi.AppLoad();
            this.setDelay(expression);
	}
        

        /**
	 * @param delay The delay to set.
	 */
	public void setDelay(String expression) {
		
            if(expression == null)
                throw new IllegalArgumentException("Expression delay must be non-null!");
		
            this.myExpression = expression;
	}

    @Override
    public void execute() {
    
        Entity entity = getProcessExecutor().getCurrentEntity();
        entity.setProcessExecutor(getProcessExecutor());
        if (!Batch.isVisible(entity)) 
            return;

        double t = 0;
        
        try {

            t = Expression.getValue(this.getModel(), entity, this.myExpression);
            
            t = (t<0.0)?0:t;
            int maxSteps = myApi.GetSimulationNumberOfSteps();
            
            double ts = myApi.GetSimulationLength(1, maxSteps);
            if (t>ts)
            {
                throw new Exception("Ururau expression simulator time "+t+" is more then Inventor simulation time "+ts);
            }
            int steps = (int) (maxSteps*(t/ts));
            if (myApi != null){
                System.out.println("Run Inventor Simulation ...");
                t = myApi.RunSimulation(1, steps);
                t = (t<0.0)?0:t;
                System.out.println("t = "+t);
            }

        } catch (ParseException ex) {
        
            Logger.getLogger(Delay.class.getName()).log(Level.SEVERE, "An error in expression: "+ this.myExpression, ex);
            ErrorMsg.displayMsg("expression_error", this.myExpression, getName(), ex);
            this.getModel().stopReplication();
        }   catch (Exception ex) {
                Logger.getLogger(DelayExpressionInventor.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        
        super.executeCommon(t);
    }
        
        
    
    
}
