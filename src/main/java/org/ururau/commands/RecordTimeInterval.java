/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;

/**
 *
 * @author jhonathan
 */
public class RecordTimeInterval extends RecordTime{
    
    public RecordTimeInterval(ModelElement parent, String expression){
        super(parent, expression);
    }
    
    public RecordTimeInterval(ModelElement parent, String expression, String name) {
        super(parent, expression, name);
    }

    @Override
    public void execute() {
    
        Entity e = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(e))
            return;

            double interval = getTime() - e.getAttributeValue(this.getMyExpression());
            this.getMyResponse().setValue(interval);
            return;

    }
    
    
}
