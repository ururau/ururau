/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;
import org.encog.neural.networks.BasicNetwork;
import org.ururau.ErrorMsg;
import org.ururau.ANNDecideEncog;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

/**
 *
 * @author jhonathan
 */
public class DecideByANNEncog extends Decide {
    
    private BasicNetwork net;
    private Map<String, Integer> myTemplatesMaps;
    private String myExpression1;
    private String myExpression2;
    private String myExpression3;
    private String myExpression4;
    private String myExpression5;
    private int camada1;
    private int camada2;
    private List<Double> myValoresAcumulados;
    private ANNDecideEncog RNA;
    private String numeroCamadaOculta;
    
    public DecideByANNEncog(ModelElement parent, String expression1, String expression2,  String expression3,String expression4, String expression5, Map<String,Integer> templatesMaps, String name, BasicNetwork net1, List<Double> valoresAcumulados, ANNDecideEncog RNA, String numeroCamadaOculta, int Cmd1 ){
        super(parent, name);
    
        this.myTemplatesMaps = templatesMaps;
        this.myExpression1 = expression1;
        this.myExpression2 = expression2;
        this.myExpression3 = expression3;
        this.myExpression4 = expression4;
        this.myExpression5 = expression5;
        this.camada1 = Cmd1;
        this.net = net1;
        this.myValoresAcumulados = valoresAcumulados;
        this.RNA = RNA;
        this.numeroCamadaOculta = numeroCamadaOculta;
        
    }

    @Override
    public void execute() {
        
        Entity entity = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(entity))
            return;
                        
        try {
                    
            double value1 = Expression.getValue(this.getModel(), entity, this.myExpression1);
            double value2 = Expression.getValue(this.getModel(), entity, this.myExpression2);
            double value3 = Expression.getValue(this.getModel(), entity, this.myExpression3);
            double value4 = Expression.getValue(this.getModel(), entity, this.myExpression4);
            double value5 = Expression.getValue(this.getModel(), entity, this.myExpression5);
                    
            double resultado = this.RNA.consulta(this.net,value1, value2, value3,value4,value5,this.camada1);                 
            

           //if( resultado == 1 ){

            //}else{
               
          //getProcessExecutor().jumpTo(this.myTemplatesMaps.get( String.valueOf( ( (int) Math.floor( resultado * this.myTemplatesMaps.size() ) ) + 1 ) ) );
                getProcessExecutor().jumpTo(this.myTemplatesMaps.get( String.valueOf( (int)resultado + 1) ) );


            
        } catch (ParseException ex) {
                    
            Logger.getLogger(Decide.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("expression_error", myExpression1, "expression", ex);
            //this.getModel().getReplication().end();    
        }

        
    }

}