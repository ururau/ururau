/*
 * Copyright (c) 2007, Manuel D. Rossetti (rossetti@uark.edu)
 *
 * Contact:
 *	Manuel D. Rossetti, Ph.D., P.E.
 *	Department of Industrial Engineering
 *	University of Arkansas
 *	4207 Bell Engineering Center
 *	Fayetteville, AR 72701
 *	Phone: (479) 575-6756
 *	Email: rossetti@uark.edu
 *	Web: www.uark.edu/~rossetti
 *
 * This file is part of the JSL (a Java Simulation Library). The JSL is a framework
 * of Java classes that permit the easy development and execution of discrete event
 * simulation programs.
 *
 * The JSL is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The JSL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the JSL (see file COPYING in the distribution);
 * if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA, or see www.fsf.org
 *
 */
package org.ururau.commands;

import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.processview.description.ProcessExecutor;
import jsl.modeling.elements.queue.UQueue;
import jsl.modeling.elements.resource.*;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.variable.ResponseStatisticsIfc;

/**
 *
 */
public class Release extends ProcessCommand {

    private Resource myResource;

    private UQueue myQueue;
    
    private boolean myWaitStats;
        
    private boolean mySizeStats;

   public Release(ModelElement parent, Resource resource, UQueue queue){
		this(parent, resource, queue, null);
	}
	
	public Release(ModelElement parent, Resource resource, UQueue queue, String name){
            this(parent, resource, queue, true, true,name);
	}
        
        public Release(ModelElement parent, Resource resource, UQueue queue, boolean waitStats, String name) {
            this(parent, resource, queue, waitStats, true,name);
        }
        
        public Release(ModelElement parent, Resource resource, UQueue queue, boolean waitStats, boolean sizeStats, String name){
		super(parent, name);
		setResource(resource);
		setQueue(queue);
                setWaitStats(waitStats);
                setSizeStats(sizeStats);
	}
	
        public boolean getWaitStats() {
            return myWaitStats;
        }

        public void setWaitStats(boolean waitStats) {
            this.myWaitStats = waitStats;
        }

        public boolean getSizeStats() {
            return mySizeStats;
        }

        public void setSizeStats(boolean sizeStats) {
            this.mySizeStats = sizeStats;
        }
        
	protected void setResource(Resource resource){
    	if (resource == null)
    		throw new IllegalArgumentException("Resource was equal to null!");
    	myResource = resource;
	}

	protected void setQueue(UQueue queue){
    	if (queue == null)
    		throw new IllegalArgumentException("Queue was equal to null!");
    	myQueue = queue;
	}

	/* (non-Javadoc)
	 * @see jsl.modeling.elements.processview.description.ProcessCommand#execute()
	 */
	public void execute() {
		// get the entity that is releasing the resource
		Entity entity = getProcessExecutor().getCurrentEntity();
		if (!Batch.isVisible(entity))
                    return;
		
		// This entity needs to release the resource
		// Get the entity's request
		Request request = entity.getRequest(myResource);
		
		// tell the resource to release the request
		myResource.release(request);
		
		if (!myQueue.isEmpty()) {
			Request r = (Request)myQueue.peekNext();
			if (r.getInitialAmountRequested() <= myResource.getNumberIdle()){

				myQueue.removeNext(myWaitStats, mySizeStats);
                                
				myResource.allocate(r);
				// get the request's entity
				Entity e = r.getEntity();

				// get the entity's process executor
				ProcessExecutor pe = e.getProcessExecutor();
				
				// schedule the entity's process executor to resume, now
				scheduleResume(pe, 0.0, 1, "Resume Seize");				
			}
                    
		}
		
	}
}
