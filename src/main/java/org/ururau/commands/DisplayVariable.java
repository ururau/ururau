/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource;
import com.mxgraph.view.mxGraphView;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JFrame;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Entity;
import jsl.modeling.elements.variable.Variable;
import org.ururau.ErrorMsg;
import org.ururau.GraphEditor;
import org.ururau.GraphEditor.CustomGraph;
import org.ururau.IdentifierMap;
import org.ururau.editor.BasicGraphEditor;
import org.ururau.editor.EditorActions;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;
import org.ururau.modules.UVariable;
import org.ururau.variable.CustomVariable;

/**
 *
 * @author tl
 */
public class DisplayVariable extends ProcessCommand {
    String myVariableName;
    String myExpression;
    Variable myVariable;
    CustomVariable myResponseVariable;
    CustomGraph myGraph;
    Object myCell;
    JFrame myJFrame;
    JLabel myLabel;
    
    public DisplayVariable(ModelElement parent, CustomGraph graph, Object cell, String variable, String expression, String name) throws Exception {
        super(parent, name);
        
        myVariableName = variable;
        myGraph = graph;
        myCell = cell;
        myJFrame = new JFrame();
        myJFrame.setSize(200, 50);
        myJFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        mxCell mc = (mxCell)cell;
        myJFrame.setLocation((int)mc.getGeometry().getCenterX(), (int)mc.getGeometry().getCenterY());
        myLabel = new JLabel();
        myJFrame.add(myLabel);
        myJFrame.setVisible(true);
        if( parent.getModel().getModelElement(variable) == null ){
            
            
            myVariable = new Variable(parent, variable);
            myResponseVariable = new CustomVariable(parent,variable+".RespVar");
            myResponseVariable.setInitialValue(0);
            
            //throw new Exception("Variable ["+myVariableName+"] not defined.");
            
            
        }else{
            
            myVariable = (Variable) parent.getModel().getModelElement(variable);
            //myResponseVariable = new CustomVariable(parent.getModel().getModelElement(variable+".RespVar"));        
            myResponseVariable = (CustomVariable) parent.getModel().getModelElement(variable+".RespVar");
        }
        
        
        
       
        myResponseVariable.setDefaultReportingOption(true);
        myExpression = expression;
        java.util.StringTokenizer t = new java.util.StringTokenizer(name,".");
        if (t != null)
        {
            IdentifierMap.add(t.nextToken(), name);
        }
        
    }
    
    private double eval(Entity e) throws ParseException
    {
        if (myExpression  != null)
        {
            return Expression.getValue(this.getModel(), e, myExpression);
        }
        
        throw new UnsupportedOperationException("Expression: Not supported yet.");
    }
        
    @Override
    public void execute() {
        Entity e = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(e))
            return;

        Variable var;
        if (myVariable == null)
        {
            var = this.getVariable(myVariableName);
        } else {
            var = myVariable;
        }
        {
            //double value = this.eval(e);
            //var.setValue(value);
            //myResponseVariable.setValue(value);
        if (var != null) {
            if (myGraph != null) {
            myGraph.getModel().beginUpdate();
            UVariable uvar = (UVariable)((mxCell)myCell).getValue();
            String val = myVariableName+": "+var.getValue();
            uvar.setValue(val);
            
            myLabel.setText(val);
            
            myGraph.getModel().endUpdate();
            myGraph.getView().reload();
            myGraph.refresh();
            myGraph.repaint();
            
            // TODO check why graph becomes null?
            
                //myGraph.convertValueToString(myCell);
                //myGraph.cellLabelChanged(myCell, uvar, true);
                /*
                myGraph.refresh();
                
                myGraph.getView().revalidate();
                
                myGraph.repaint();
                myGraph.getView().createState(myCell);
                */
                //myGraph.getView().reload();
                //GraphEditor.editor.
                //GraphEditor.editor.changeTracker;
                //mxCellRenderer
            }
            //myGraph.convertValueToString(myCell);
        }
            /* TODO set variable value into display box */
            
        /*} catch (ParseException ex) {
            Logger.getLogger(DisplayVariable.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("expression_error",myExpression, myVariableName, ex);
            this.getModel().stopReplication();*/
        }
    }  
}
