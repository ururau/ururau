/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.commands;

import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Entity;
import jsl.modeling.elements.variable.Counter;
import jsl.modeling.elements.variable.ResponseVariable;
import jsl.utilities.statistic.Statistic;
import org.ururau.ErrorMsg;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

/**
 *
 * @author tulio
 */
public abstract class Record extends ProcessCommand {

    public static enum RecordType { TIME_INTERVAL, COUNTER, TIME_BETWEEN};

    private String myExpression;


    public Record(ModelElement parent, String name, String myExpression) {
        super(parent, name);
        
        this.myExpression = myExpression;
    }

    protected String getMyExpression(){
    
        return this.myExpression;
    }
    
        
    public abstract void execute();
    

}
