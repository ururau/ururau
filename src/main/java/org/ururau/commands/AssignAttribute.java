/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.commands;

import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Attribute;
import jsl.modeling.elements.resource.Entity;
import org.ururau.ErrorMsg;
import org.ururau.IdentifierMap;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

/**
 *
 * @author tulio
 */
public class AssignAttribute extends ProcessCommand {

    String myAttributeName;
    String myExpression;
    Attribute myAttribute;


    public AssignAttribute(ModelElement parent, String attribute, String expression, String name) {
        super(parent, name);
        myAttributeName = attribute;
        
        if( parent.getModel().containsModelElement(attribute) == true){
        
            myAttribute = null;
        
        }else{

            myAttribute = new Attribute(attribute);

        }
        
        myExpression = expression;
        
        
        
        java.util.StringTokenizer t = new java.util.StringTokenizer(name,".");
        if (t != null)
        {
            IdentifierMap.add(t.nextToken(), name);
        }
    }


    private double eval(Entity e) throws ParseException
    {
        if (myExpression  != null)
        {
            return Expression.getValue(this.getModel(), e, myExpression);
        }
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void execute() {
        Entity e = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(e))
            return;
        try {
            if(e.containsAttribute(myAttributeName) == false){
                e.addAttribute(myAttributeName, myAttribute);
            }
            e.setAttributeValue(myAttributeName, this.eval(e));

        } catch (ParseException ex) {
            Logger.getLogger(AssignAttribute.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("expression_error",myExpression, myAttributeName, ex);
            this.getModel().stopReplication();
        }

        
    }

}
