/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import jsl.modeling.elements.processview.description.ProcessCommand;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
/**
 *
 * @author Fraga
 */
public class RunMacro extends ProcessCommand {
    private String myFilename;
    private String myMacro;
    public RunMacro(ModelElement parent, String filename, String macro){
        this(parent,filename,macro,null);
    }
   
    
    public RunMacro(ModelElement parent, String filename, String macro, String name) {
        super(parent, name);
        this.myFilename = filename;
        this.myMacro = macro;
    }

    @Override
    public void execute() {
        try {
            /* run solver */
               File file2 = new File(myFilename);
               java.lang.Process p = Runtime.getRuntime().exec("cscript.exe runsolver.vbs \""+file2.getAbsolutePath()+"\" "+file2.getName()+"!"+myMacro);
               BufferedReader input = new BufferedReader (new InputStreamReader(p.getInputStream()));
               String line;
               
               while((line = input.readLine()) != null) {
                  System.out.println(line);
               }
               input.close();
        } catch (IOException ex) {
            Logger.getLogger(RunMacro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
