/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.commands;

import java.io.IOException;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.queue.UQueue;
import jsl.modeling.elements.resource.Resource;
import jsl.modeling.elements.variable.Variable;
import jsl.modeling.elements.variable.RandomVariable;
import org.ururau.description.MacroCommand;

/**
 *
 * @author tulio
 */
public class Process extends MacroCommand {
    
    public Process(ModelElement parent, Variable v,
                   Resource r, UQueue q, RandomVariable rv) {
        this(parent,v,r,q,rv,null);
    }

    public Process(ModelElement parent, Variable v,
                   Resource r, UQueue q, String exp) {
        this(parent,v,r,q,exp,null);
    }

    public Process(ModelElement parent, Variable v,
                   Resource r, UQueue q, RandomVariable rv, String name) {
        this(parent,v,r,q,rv,1,name);
    }
    
    public Process(ModelElement parent, Variable v,
                   Resource r, UQueue q, String exp, String name) {
        this(parent,v,r,q,exp,1,true,true,name);
    }
    public Process(ModelElement parent, Variable v,
                   Resource r, UQueue q, RandomVariable rv, int priority, String name) {
        this(parent,v,r,q,rv, priority,true,true,name);
        
    }
    
    public Process(ModelElement parent, Variable v,
                   Resource r, UQueue q, RandomVariable rv, int priority, boolean waitStats, String name) {
        this(parent,v,r,q,rv,priority,waitStats,true,name);
    }
    
    public Process(ModelElement parent, Variable v,
                   Resource r, UQueue q, RandomVariable rv, int priority, boolean waitStats, boolean sizeStats, String name) {
        super(parent, name+".process");
        this.addSubCommand(new Seize(parent, v, r, q, priority, waitStats, sizeStats, name+".seize"));
        this.addSubCommand(new DelayVariable(parent, rv, name+".delay"));
        this.addSubCommand(new Release(parent, r, q, waitStats, sizeStats, name+".release"));
    }
    
    public Process(ModelElement parent, Variable v,
                   Resource r, UQueue q, String exp, int priority, boolean waitStats, boolean sizeStats, String name) {
        super(parent, name+".process");
        this.addSubCommand(new Seize(parent, v, r, q, priority, waitStats, sizeStats, name+".seize"));
        this.addSubCommand(new DelayExpression(parent, exp, name+".delay"));
        this.addSubCommand(new Release(parent, r, q, waitStats, sizeStats, name+".release"));
    }

}
