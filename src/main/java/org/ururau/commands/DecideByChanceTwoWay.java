/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;
import jsl.utilities.random.distributions.DEmpirical;

/**
 *
 * @author jhonathan
 */
public class DecideByChanceTwoWay extends DecideByChance{
        
    private int myCommandIndexIfTrue = -1;
    
    
    public DecideByChanceTwoWay(ModelElement parent, double probability, int jumpToCommandIndexIfTrue,
            int jumpToCommandIndexIfFalse, String name) {
        super(parent, name, jumpToCommandIndexIfFalse);
        
        myCommandIndexIfTrue = jumpToCommandIndexIfTrue;
        DEmpirical demp = new DEmpirical();
        demp.addProbabilityPoint(1, probability);
        demp.addLastProbabilityPoint(0);
        this.setMyDecideByChanceRV(parent, demp);
    }

    @Override
    public void execute() {
        
        Entity entity = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(entity))
            return;

        double value = this.getMyDecideByChanceRV().getValue();
           
        if (value == 0){
        
            getProcessExecutor().jumpTo(this.getMyCommandIndexIfFalse());
        
        } else {
            if (myCommandIndexIfTrue != -1){
                 
                getProcessExecutor().jumpTo(myCommandIndexIfTrue); 
            }    
        }
          
        return;       
    }
    
}