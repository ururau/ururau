/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.commands;

import java.util.Iterator;

import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.queue.QObject;
import jsl.modeling.elements.queue.Queue;
import jsl.modeling.elements.resource.Attribute;
import jsl.modeling.elements.resource.Entity;
/**
 *
 * @author tulio
 */
public class Batch extends ProcessCommand {

    public static enum BatchType { PERMANENT_BATCH, TEMPORARY_BATCH};
    
    //O certo seria ter um conjunto de Entity relacionado com o batch N pra 1
    private int myBatchSize;
    private BatchType myBatchType;
    private Queue myQueue;
    
    public static int getNesting(Entity entity) {
        if (!entity.containsAttribute("BATCH_NESTING"))
            return 0;
        return (int) entity.getAttributeValue("BATCH_NESTING");
    }
    
    public static  boolean isPermanent(Entity entity)
    {
        if (!entity.containsAttribute("BATCH_TYPE"))
        {
            return true;
        }
        return entity.getAttributeValue("BATCH_TYPE") == Batch.BatchType.PERMANENT_BATCH.ordinal();
    }
    
    public static void incrementNesting(Entity entity)
    {
        if (!entity.containsAttribute("BATCH_NESTING"))
        {
            entity.addAttribute("BATCH_NESTING", new Attribute(0,"BATCH_NESTING"));
        }
        double value = entity.getAttributeValue("BATCH_NESTING");
        value++;
        entity.setAttributeValue("BATCH_NESTING", value);
    }
     
    public static void turnOnVisible(Entity entity)
    {
        if (!entity.containsAttribute("VISIBLE"))
        {
            entity.addAttribute("VISIBLE", new Attribute(1,"VISIBLE"));
        }
        entity.setAttributeValue("VISIBLE",1);
    }

    public static void turnOffVisible(Entity entity)
    {
        if (!entity.containsAttribute("VISIBLE"))
        {
            entity.addAttribute("VISIBLE", new Attribute(0,"VISIBLE"));
        }
        entity.setAttributeValue("VISIBLE",0);
    }

    public static boolean isVisible(Entity entity)
    {
        if (!entity.containsAttribute("VISIBLE"))
        {
            return true;
        }
        return (entity.getAttributeValue("VISIBLE") == 1);
    }


    //Voltar a mecher nele com a ajuda do tulio
    public static void setBatchType(Entity entity, BatchType type)
    {
        if (!entity.containsAttribute("BATCH_TYPE"))
        {
            entity.addAttribute("BATCH_TYPE", new Attribute(type.ordinal(),"BATCH_TYPE"));
        }
        entity.setAttributeValue("BATCH_TYPE", type.ordinal());
    }
    
    public static BatchType getBatchType(Entity entity)
    {
        BatchType type;
        if (!entity.containsAttribute("BATCH_TYPE"))
        {
            type = BatchType.PERMANENT_BATCH;
        }
        if ((entity.getAttributeValue("BATCH_TYPE") == BatchType.PERMANENT_BATCH.ordinal()))
        {
            type = BatchType.PERMANENT_BATCH;
        } else {
            type = BatchType.TEMPORARY_BATCH;
        }
        return type;
    }
    
    public static boolean isPermanentBatch(Entity entity)
    {
        if (!entity.containsAttribute("BATCH_TYPE"))
        {
            return true;
        }
        return (entity.getAttributeValue("BATCH_TYPE") == BatchType.PERMANENT_BATCH.ordinal());
    }
    
    public static boolean isTemporaryBatch(Entity entity)
    {
        if (!entity.containsAttribute("BATCH_TYPE"))
        {
            return false;
        }
        return (entity.getAttributeValue("BATCH_TYPE") == BatchType.TEMPORARY_BATCH.ordinal());
    }
    

    public Batch(ModelElement parent, int batchSize, Queue queue, BatchType type, String name) {
        super(parent, name);
        myBatchSize = batchSize;
        myQueue = queue;
        myBatchType = type;
    }

    public void execute() {
        
        Entity entity = getProcessExecutor().getCurrentEntity();
        entity.setProcessExecutor(getProcessExecutor());
        
        if (!Batch.isVisible(entity))
        {
            return;
        }
            if (myBatchType.equals(BatchType.TEMPORARY_BATCH))
            {
                if (entity.containsAttribute("SEPARATE_HOLD")) {
                    entity.setAttributeValue("SEPARATE_HOLD",1);
                } else {
                    entity.addAttribute("SEPARATE_HOLD", new Attribute(1,"SEPARATE_HOLD"));
                }
            }

        if (myQueue.size() < myBatchSize - 1 )
        {
            Batch.setBatchType(entity, myBatchType);
            Batch.incrementNesting(entity);
            Batch.turnOffVisible(entity);

            if (entity.getQueue() == null) {
                myQueue.enqueue(entity);
                entity.getProcessExecutor().suspend();
                timedUpdate();
            }
        } else {
//SEPARATEHOLD
            Batch.incrementNesting(entity);
            Iterator<QObject> i = myQueue.iterator();
            while(i.hasNext()) {
                Entity e = (Entity) myQueue.removeFirst();
                e.getProcessExecutor().resume();
            }

            timedUpdate();
            
        }
        
    }
}

