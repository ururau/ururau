/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Entity;
import jsl.modeling.elements.variable.Variable;
import org.ururau.ErrorMsg;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

/**
 *
 * @author tulio
 */
public class Write extends ProcessCommand {
    
    public static enum WriteType {VARIABLE, ATTRIBUTE, EXPRESSION};

    private WriteType myWriteType;
    String myExpression;
    BufferedWriter myWriter;
    String myFilename;
    public Write(ModelElement parent, WriteType wtype, String filename, String expression) {
        this(parent, wtype, filename, expression, null);
    }
    
    public Write(ModelElement parent, WriteType wtype, String filename, String expression, String name) {
        super(parent, name);
        this.myExpression = expression;
        this.myFilename = filename;
        this.myWriteType = wtype;
        try {
        myWriter = new BufferedWriter(new OutputStreamWriter(
          new FileOutputStream(filename), "utf-8"));
            
        } catch (IOException ex){
            Logger.getLogger(Write.class.getName()).log(Level.SEVERE, "An error to write file: "+myFilename, ex);
        }
            /*} finally {
               try {writer.close();} catch (Exception ex) {}
            }*/
    }
    
    
    @Override
    public void execute() {
        try {
            Entity entity = getProcessExecutor().getCurrentEntity();
            entity.setProcessExecutor(getProcessExecutor());
            if (!Batch.isVisible(entity))
                return;
            double value = 0.0;
            if (myWriteType == WriteType.EXPRESSION)
            {
                try {
                    value = Expression.getValue(this.getModel(), entity, myExpression);
                } catch (ParseException ex) {
                     Logger.getLogger(Write.class.getName()).log(Level.SEVERE, "An error in expression: "+myExpression, ex);
                     ErrorMsg.displayMsg("expression_error", myExpression, getName(), ex);
                }
            }
            else if (myWriteType == WriteType.ATTRIBUTE)
            {
                    value = entity.getAttributeValue(myExpression);
            } 
            else if (myWriteType == WriteType.VARIABLE)
            {
                Variable var = (Variable) this.getModel().getModelElement(myExpression);
                value = var.getValue();
            }
            myWriter.write(value+"\r\n");
        } catch (IOException ex) {
            Logger.getLogger(Write.class.getName()).log(Level.SEVERE, "An error to write value to file: "+myFilename, ex);

        }
    }
    
    @Override
    public void afterExperiment()
    {
        try {
            super.afterExperiment();
            myWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(Write.class.getName()).log(Level.SEVERE, "An error to close file: "+myFilename, ex);
        }
    }
    
}
