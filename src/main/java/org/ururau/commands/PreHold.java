/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.processview.description.ProcessExecutor;
import jsl.modeling.elements.queue.QObject;
import jsl.modeling.elements.queue.Queue;
import jsl.modeling.elements.resource.Entity;
import org.ururau.ErrorMsg;
import org.ururau.commands.Batch.BatchType;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

/**
 *
 * @author tulio
 */
public class PreHold extends ProcessCommand {
    private Queue myQueue;
    private String myExpression;

    public PreHold(ModelElement parent, Queue queue, String expression) {
        this(parent, queue, expression, null);
    }

    public PreHold(ModelElement parent, Queue queue, String expression, String name) {
        super(parent, name);
        myQueue = queue;
        myExpression = expression;
    }

    public void execute() {
        Entity entity = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(entity))
            return;
        double value = 0.0;
        Batch.setBatchType(entity, BatchType.TEMPORARY_BATCH);
        try {
            value = Expression.getValue(this.getModel(), entity, myExpression);
        } catch (ParseException ex) {
            Logger.getLogger(PreHold.class.getName()).log(Level.SEVERE, "Error in expression: "+myExpression, ex);
            ErrorMsg.displayMsg("expression_error", myExpression, getName(), ex);
            this.getModel().stopReplication();
        }
        if (value == 0.0)
        {
            Batch.turnOffVisible(entity);
            if (!entity.isQueued())
            {
                myQueue.enqueue(entity);
                if (entity.isQueued()) { // suspend the executor at this hold command, otherwise continue
                    getProcessExecutor().suspend();
                }
            }
        } else {
            Iterator<QObject>  i = myQueue.iterator();    
            while(i.hasNext()) {
                Entity ent = (Entity) myQueue.removeFirst();
                ProcessExecutor pe = ent.getProcessExecutor();
                    if (pe.isSuspended()) {
                        // schedule the entity's process executor to resume, now
                        scheduleResume(pe, 0.0, 1, "Resume Hold");
                    }
                
            }
        }
        timedUpdate();

    }
    
}
