/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Entity;
import org.ururau.Communication;
import org.ururau.ErrorMsg;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

/**
 *
 * @author jhonathan
 */
public class CommunicationTagWriterReal extends CommunicationTagWriter{
    
    public CommunicationTagWriterReal(ModelElement parent, String tag, String expression, String name) {
        super(parent, tag, expression, name);        
    }
    
    @Override
    public void execute() {
        
        Entity e = getProcessExecutor().getCurrentEntity();
        if (!Batch.isVisible(e))
            return;
        double value = 0.0;
        try {
            value = Expression.getValue(this.getModel(), e, this.getMyExpression());
            Communication com = Communication.getInstance();
            if (com.isEnabled()) {
      
                    com.setTagValue(this.getMyTagName(), value);
            }
        }catch (ParseException ex) {
            Logger.getLogger(CommunicationTagWriter.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("expression_error", this.getMyExpression(), getName(), ex);
            this.getModel().stopReplication();
        }
    
    }

}
