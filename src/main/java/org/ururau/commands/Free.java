/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Entity;
import org.ururau.ErrorMsg;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

/**
 *
 * @author tulio
 */
public class Free extends ProcessCommand  {
    private int nesting = 100;
    
    String myExpression;
    
    //Isso não pode estar aqui
    public static void decrementNesting(Entity entity)
    {
        double value = entity.getAttributeValue("BATCH_NESTING");
        value--;
        entity.setAttributeValue("BATCH_NESTING", value);
    }

    //Isso não pode estar aqui
    public static int getNesting(Entity entity)
    {
        return (int) entity.getAttributeValue("BATCH_NESTING");
    }
    
    public Free(ModelElement parent, String expression)
    {
        this(parent,expression, null);
    }
    
    public Free(ModelElement parent, String expression, String name)
    {
        super(parent, name);
        this.myExpression = expression;
    }
    
    @Override
    public void execute() {
        Entity entity = getProcessExecutor().getCurrentEntity();
        double value = 0.0;
        try {
            value = Expression.getValue(this.getModel(), entity, this.myExpression);
        } catch (ParseException ex) {
            Logger.getLogger(Free.class.getName()).log(Level.SEVERE, "Error in expression: "+ this.myExpression, ex);
            ErrorMsg.displayMsg("expression_error", this.myExpression, getName(), ex);
            this.getModel().stopReplication();
        }
        if (Batch.isTemporaryBatch(entity) && (value != 0.0))
        {
            if ( this.nesting > Separate.getNesting(entity))
            {
                this.nesting = Separate.getNesting(entity);
                Separate.decrementNesting(entity);
                Batch.turnOnVisible(entity);
                Batch.setBatchType(entity, Batch.BatchType.PERMANENT_BATCH);
            }
            if ( this.nesting == Separate.getNesting(entity))
            {
                Batch.turnOnVisible(entity);
                Batch.setBatchType(entity, Batch.BatchType.PERMANENT_BATCH);                
            }
        }
    }
}
