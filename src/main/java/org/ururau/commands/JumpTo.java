/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.commands;

import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Entity;

/**
 *
 * @author tulio
 */
public class JumpTo extends ProcessCommand {

    private int myCommandIndex;

    public JumpTo(ModelElement parent, int commandIndex) {
        this(parent, commandIndex, null);
    }
    
    public JumpTo(ModelElement parent, int commandIndex, String name) {
        super(parent, name);
        this.myCommandIndex = commandIndex;
    }

    public void execute() {
        
        this.getProcessExecutor().jumpTo(myCommandIndex);
    }

}
