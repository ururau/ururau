/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import jsl.modeling.ModelElement;
import jsl.modeling.elements.variable.ResponseVariable;

/**
 *
 * @author jhonathan
 */
public abstract class RecordTime extends Record {
    
    private ResponseVariable myResponse;
    
    public RecordTime(ModelElement parent, String expression) {
        this(parent, expression, null);
    }
    
    public RecordTime(ModelElement parent, String expression, String name) {
        super(parent, name, expression);
        
        myResponse = new ResponseVariable(this,getName()+".RecordRespVar");
        myResponse.setValue(0);
    }

    protected ResponseVariable getMyResponse(){
    
        return this.myResponse;
    }
    
    
    
}
