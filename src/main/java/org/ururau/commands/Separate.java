/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Attribute;
import jsl.modeling.elements.resource.Entity;
/**
 *
 * @author tulio
 */
public class Separate extends ProcessCommand  {
    
    private Queue separateQueue;
    int nesting = 0;
    
    //Tem que colocar isso no EntiryUrurau
    public static void decrementNesting(Entity entity)
    {
        if (!entity.containsAttribute("BATCH_NESTING"))
        {
            entity.addAttribute("BATCH_NESTING", new Attribute(0,"BATCH_NESTING"));
            return;
        }
        double value = entity.getAttributeValue("BATCH_NESTING");
        value--;
        entity.setAttributeValue("BATCH_NESTING", value);
    }
    
    public static int getNesting(Entity entity)
    {
        if (!entity.containsAttribute("BATCH_NESTING"))
        {
            entity.addAttribute("BATCH_NESTING", new Attribute(0,"BATCH_NESTING"));
            return 0;
        }
        return (int) entity.getAttributeValue("BATCH_NESTING");
    }
    
    public Separate(ModelElement parent)
    {
        this(parent,null);
        separateQueue = new LinkedBlockingQueue();
    }
    
    public Separate(ModelElement parent, String name)
    {
        super(parent,name);
        separateQueue = new LinkedBlockingQueue();
    }
    
    @Override
    public void execute() {
        Entity entity = getProcessExecutor().getCurrentEntity();
        entity.setProcessExecutor(getProcessExecutor());
        
        if (Batch.isTemporaryBatch(entity) )
        {
                 
            if (!Batch.isVisible(entity)) {
                    if (entity.containsAttribute("SEPARATE_HOLD")) {
                    if (entity.getAttributeValue("SEPARATE_HOLD") == 0)
                    {  
//                        separateQueue.add(entity);
                        //entity.getProcessExecutor().suspend();
                    }
                }
                
                    if (entity.containsAttribute("SEPARATE_HOLD")) {
                    if (entity.getAttributeValue("SEPARATE_HOLD") == 1)
                    {
//                        entity.setAttributeValue("SEPARATE_HOLD",0);

  //                      while(!separateQueue.isEmpty()) 
                        {
  //                          Entity e = (Entity) separateQueue.remove();

                            if ( Separate.getNesting(entity) > 0 )
                            {

                                Separate.decrementNesting(entity);
                                Batch.turnOnVisible(entity);
                                if (Separate.getNesting(entity) == 0)
                                    Batch.setBatchType(entity, Batch.BatchType.PERMANENT_BATCH);

                                //e.getProcessExecutor().resume();
                            }
                        }
                        nesting = Separate.getNesting(entity);
                        Separate.decrementNesting(entity);
                        timedUpdate();
                    }

                    }
        
        }
        
        }
        
        
    }
    
}
