/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.commands;

import jsl.modeling.ModelElement;

/**
 *
 * @author jhonathan
 */
public abstract class DecideByCondition extends Decide{
   
    private int myCommandIndexIfFalse;
    
    public DecideByCondition(ModelElement parent, String name, int myCommandIndexIfFalse) {
        super(parent, name);
        
        this.myCommandIndexIfFalse = myCommandIndexIfFalse;
    }

    
    protected  int getMyCommandIndexIfFalse(){
    
        return this.myCommandIndexIfFalse;
    }


}
