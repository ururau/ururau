/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.entity;

import jsl.modeling.elements.processview.description.EntityType;
import jsl.modeling.elements.resource.Attribute;
import jsl.modeling.elements.resource.Entity;
import org.ururau.commands.Batch;

/**
 *
 * @author jhonathan
 */
public class EntityUrurau extends Entity{

    public EntityUrurau(EntityType entityType, String name) {
        super(name, entityType);
    }
    
        public int getNesting() {
        if (!this.containsAttribute("BATCH_NESTING"))
            return 0;
        return (int) this.getAttributeValue("BATCH_NESTING");
    }
    
    public boolean isPermanent()
    {
        if (!this.containsAttribute("BATCH_TYPE"))
        {
            return true;
        }
        return this.getAttributeValue("BATCH_TYPE") == Batch.BatchType.PERMANENT_BATCH.ordinal();
    }
    
    public void incrementNesting()
    {
        if (!this.containsAttribute("BATCH_NESTING"))
        {
            this.addAttribute("BATCH_NESTING", new Attribute(0,"BATCH_NESTING"));
        }
        double value = this.getAttributeValue("BATCH_NESTING");
        value++;
        this.setAttributeValue("BATCH_NESTING", value);
    }
     
    public void turnOnVisible()
    {
        if (!this.containsAttribute("VISIBLE"))
        {
            this.addAttribute("VISIBLE", new Attribute(1,"VISIBLE"));
        }
        this.setAttributeValue("VISIBLE",1);
    }

    public void turnOffVisible()
    {
        if (!this.containsAttribute("VISIBLE"))
        {
            this.addAttribute("VISIBLE", new Attribute(0,"VISIBLE"));
        }
        this.setAttributeValue("VISIBLE",0);
    }

    public boolean isVisible()
    {
        if (!this.containsAttribute("VISIBLE"))
        {
            return true;
        }
        return (this.getAttributeValue("VISIBLE") == 1);
    }

    
}
