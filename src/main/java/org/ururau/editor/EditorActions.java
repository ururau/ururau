/*
 * $Id: EditorActions.java,v 1.35 2011-02-14 15:45:58 gaudenz Exp $
 * Copyright (c) 2001-2010, Gaudenz Alder, David Benson
 * 
 * All rights reserved.
 * 
 * See LICENSE file for license details. If you are unable to locate
 * this file please contact info (at) jgraph (dot) com.
 */
package org.ururau.editor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import org.w3c.dom.Document;

import com.mxgraph.analysis.mxDistanceCostFunction;
import com.mxgraph.analysis.mxGraphAnalysis;
import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.canvas.mxICanvas;
import com.mxgraph.canvas.mxSvgCanvas;
import com.mxgraph.io.mxCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.shape.mxStencilShape;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.mxGraphOutline;
import com.mxgraph.swing.handler.mxConnectionHandler;
import com.mxgraph.swing.util.mxGraphActions;
import com.mxgraph.swing.view.mxCellEditor;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.util.mxCellRenderer.CanvasFactory;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxResources;
import com.mxgraph.util.mxUtils;
import com.mxgraph.util.png.mxPngEncodeParam;
import com.mxgraph.util.png.mxPngImageEncoder;
import com.mxgraph.util.png.mxPngTextDecoder;
import com.mxgraph.view.mxGraph;
import com.sun.xml.internal.ws.encoding.xml.XMLCodec;
import java.awt.Dialog;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.prefs.Preferences;
import javax.swing.JDialog;
import javax.swing.JPanel;
import jsl.modeling.Experiment;
import jsl.modeling.Model;
import org.ururau.Communication;
import org.ururau.Configuration;
import org.ururau.view.CommunicationView;
import org.ururau.ErrorMsg;
import org.ururau.Optimizer;
import org.ururau.view.OptimizerView;
import org.ururau.view.GoodnessOfFitView;
import org.ururau.GraphEditor;
import org.ururau.OptimizerConfiguration;
import org.ururau.ReportGenerator;
import org.ururau.modules.UAssign;
import org.ururau.view.UAssignView;
import org.ururau.modules.UBatch;
import org.ururau.view.UBatchView;
import org.ururau.modules.UCreate;
import org.ururau.view.UCreateView;
import org.ururau.Template;
import org.ururau.UFitnessFunction;
import org.ururau.modules.USimulation;
import org.ururau.view.USimulationView;
import org.ururau.modules.UFunction;
import org.ururau.modules.UModel;
import org.ururau.view.UFunctionView;
import org.ururau.modules.UHold;
import org.ururau.view.UHoldView;
import org.ururau.modules.UJump;
import org.ururau.view.UJumpView;
import org.ururau.modules.ULocal;
import org.ururau.view.ULocalView;
import org.ururau.modules.URecord;
import org.ururau.view.URecordView;
import org.ururau.modules.UResource;
import org.ururau.view.UResourceView;
import org.ururau.modules.USeparate;
import org.ururau.view.USeparateView;
import org.ururau.modules.UWrite;
import org.ururau.view.UWriteView;
import org.ururau.modules.UXor;
import org.ururau.modules.UXorBranch;
import org.ururau.view.UXorBranchChanceView;
import org.ururau.view.UXorBranchConditionView;
import org.ururau.view.UXorView;
import org.ururau.commands.Decide.DecideType;
import org.ururau.commands.Delay;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;
import org.ururau.modules.UAssign;
import org.ururau.modules.UBatch;
import org.ururau.modules.UEmissions;
import org.ururau.modules.UInventorFunction;
import org.ururau.modules.UMyWrite;
import org.ururau.modules.URunMacro;
import org.ururau.modules.USheetReadWrite;
import org.ururau.modules.UVariable;
import org.ururau.utils.CustomDialog;
import org.ururau.view.ConfigurationView;
import org.ururau.view.GoView;
import org.ururau.view.UAssignView;
import org.ururau.view.UBatchView;
import org.ururau.view.UEmissionsView;
import org.ururau.view.UInventorFunctionView;
import org.ururau.view.URunMacroView;
import org.ururau.view.USheetReadWriteView;
import org.ururau.view.UVariableView;
import org.ururau.view.UXorBranchRnaView;

/**
 *
 */
public class EditorActions
{
	/**
	 * 
	 * @param e
	 * @return Returns the graph for the given action event.
	 */
	public static final BasicGraphEditor getEditor(ActionEvent e)
	{
		if (e.getSource() instanceof Component)
		{
			Component component = (Component) e.getSource();

			while (component != null
					&& !(component instanceof BasicGraphEditor))
			{
				component = component.getParent();
			}

			return (BasicGraphEditor) component;
		}

		return null;
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleRulersItem extends JCheckBoxMenuItem
	{
		/**
		 * 
		 */
		public ToggleRulersItem(final BasicGraphEditor editor, String name)
		{
			super(name);
			setSelected(editor.getGraphComponent().getColumnHeader() != null);

			addActionListener(new ActionListener()
			{
				/**
				 * 
				 */
				public void actionPerformed(ActionEvent e)
				{
					mxGraphComponent graphComponent = editor
							.getGraphComponent();

					if (graphComponent.getColumnHeader() != null)
					{
						graphComponent.setColumnHeader(null);
						graphComponent.setRowHeader(null);
					}
					else
					{
						graphComponent.setColumnHeaderView(new EditorRuler(
								graphComponent,
								EditorRuler.ORIENTATION_HORIZONTAL));
						graphComponent.setRowHeaderView(new EditorRuler(
								graphComponent,
								EditorRuler.ORIENTATION_VERTICAL));
					}
				}
			});
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleGridItem extends JCheckBoxMenuItem
	{
		/**
		 * 
		 */
		public ToggleGridItem(final BasicGraphEditor editor, String name)
		{
			super(name);
			setSelected(true);

			addActionListener(new ActionListener()
			{
				/**
				 * 
				 */
				public void actionPerformed(ActionEvent e)
				{
					mxGraphComponent graphComponent = editor
							.getGraphComponent();
					mxGraph graph = graphComponent.getGraph();
					boolean enabled = !graph.isGridEnabled();

					graph.setGridEnabled(enabled);
					graphComponent.setGridVisible(enabled);
					graphComponent.repaint();
					setSelected(enabled);
				}
			});
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleOutlineItem extends JCheckBoxMenuItem
	{
		/**
		 * 
		 */
		public ToggleOutlineItem(final BasicGraphEditor editor, String name)
		{
			super(name);
			setSelected(true);

			addActionListener(new ActionListener()
			{
				/**
				 * 
				 */
				public void actionPerformed(ActionEvent e)
				{
					final mxGraphOutline outline = editor.getGraphOutline();
					outline.setVisible(!outline.isVisible());
					outline.revalidate();

					SwingUtilities.invokeLater(new Runnable()
					{
						/*
						 * (non-Javadoc)
						 * @see java.lang.Runnable#run()
						 */
						public void run()
						{
							if (outline.getParent() instanceof JSplitPane)
							{
								if (outline.isVisible())
								{
									((JSplitPane) outline.getParent())
											.setDividerLocation(editor
													.getHeight() - 300);
									((JSplitPane) outline.getParent())
											.setDividerSize(6);
								}
								else
								{
									((JSplitPane) outline.getParent())
											.setDividerSize(0);
								}
							}
						}
					});
				}
			});
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ExitAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			BasicGraphEditor editor = getEditor(e);

			if (editor != null)
			{
				editor.exit();
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class StylesheetAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected String stylesheet;

		/**
		 * 
		 */
		public StylesheetAction(String stylesheet)
		{
			this.stylesheet = stylesheet;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				mxGraph graph = graphComponent.getGraph();
				mxCodec codec = new mxCodec();
				Document doc = mxUtils.loadDocument(EditorActions.class
						.getResource(stylesheet).toString());

				if (doc != null)
				{
					codec.decode(doc.getDocumentElement(),
							graph.getStylesheet());
					graph.refresh();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ZoomPolicyAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected int zoomPolicy;

		/**
		 * 
		 */
		public ZoomPolicyAction(int zoomPolicy)
		{
			this.zoomPolicy = zoomPolicy;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				graphComponent.setPageVisible(true);
				graphComponent.setZoomPolicy(zoomPolicy);
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class GridStyleAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected int style;

		/**
		 * 
		 */
		public GridStyleAction(int style)
		{
			this.style = style;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				graphComponent.setGridStyle(style);
				graphComponent.repaint();
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class GridColorAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				Color newColor = JColorChooser.showDialog(graphComponent,
						mxResources.get("gridColor"),
						graphComponent.getGridColor());

				if (newColor != null)
				{
					graphComponent.setGridColor(newColor);
					graphComponent.repaint();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ScaleAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected double scale;

		/**
		 * 
		 */
		public ScaleAction(double scale)
		{
			this.scale = scale;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				double scale = this.scale;

				if (scale == 0)
				{
					String value = (String) JOptionPane.showInputDialog(
							graphComponent, mxResources.get("value"),
							mxResources.get("scale") + " (%)",
							JOptionPane.PLAIN_MESSAGE, null, null, "");

					if (value != null)
					{
						scale = Double.parseDouble(value.replace("%", "")) / 100;
					}
				}

				if (scale > 0)
				{
					graphComponent.zoomTo(scale, graphComponent.isCenterZoom());
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class PageSetupAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				PrinterJob pj = PrinterJob.getPrinterJob();
				PageFormat format = pj.pageDialog(graphComponent
						.getPageFormat());

				if (format != null)
				{
					graphComponent.setPageFormat(format);
					graphComponent.zoomAndCenter();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class PrintAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				PrinterJob pj = PrinterJob.getPrinterJob();

				if (pj.printDialog())
				{
					PageFormat pf = graphComponent.getPageFormat();
					Paper paper = new Paper();
					double margin = 36;
					paper.setImageableArea(margin, margin, paper.getWidth()
							- margin * 2, paper.getHeight() - margin * 2);
					pf.setPaper(paper);
					pj.setPrintable(graphComponent, pf);

					try
					{
						pj.print();
					}
					catch (PrinterException e2)
					{
						System.out.println(e2);
					}
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class SaveAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected boolean showDialog;

		/**
		 * 
		 */
		protected String lastDir = null;

		/**
		 * 
		 */
		public SaveAction(boolean showDialog)
		{
			this.showDialog = showDialog;
		}

		/**
		 * Saves XML+PNG format.
		 */
		protected void saveXmlPng(BasicGraphEditor editor, String filename,
				Color bg) throws IOException
		{
			mxGraphComponent graphComponent = editor.getGraphComponent();
			mxGraph graph = graphComponent.getGraph();

			// Creates the image for the PNG file
			BufferedImage image = mxCellRenderer.createBufferedImage(graph,
					null, 1, bg, graphComponent.isAntiAlias(), null,
					graphComponent.getCanvas());

			// Creates the URL-encoded XML data
			mxCodec codec = new mxCodec();
			String xml = URLEncoder.encode(
					mxUtils.getXml(codec.encode(graph.getModel())), "UTF-8");
			mxPngEncodeParam param = mxPngEncodeParam
					.getDefaultEncodeParam(image);
			param.setCompressedText(new String[] { "mxGraphModel", xml });

			// Saves as a PNG file
			FileOutputStream outputStream = new FileOutputStream(new File(
					filename));
			try
			{
				mxPngImageEncoder encoder = new mxPngImageEncoder(outputStream,
						param);

				if (image != null)
				{
					encoder.encode(image);

					editor.setModified(false);
					editor.setCurrentFile(new File(filename));
				}
				else
				{
					JOptionPane.showMessageDialog(graphComponent,
							mxResources.get("noImageData"));
				}
			}
			finally
			{
				outputStream.close();
			}
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			BasicGraphEditor editor = getEditor(e);

			if (editor != null)
			{
				mxGraphComponent graphComponent = editor.getGraphComponent();
				mxGraph graph = graphComponent.getGraph();
				FileFilter selectedFilter = null;

                /*
				DefaultFileFilter xmlPngFilter = new DefaultFileFilter(".png",
						"PNG+XML " + mxResources.get("file") + " (.png)");*/
				FileFilter vmlFileFilter = new DefaultFileFilter(".html",
						"VML " + mxResources.get("file") + " (.html)");
				String filename = null;
				boolean dialogShown = false;

				if (showDialog || editor.getCurrentFile() == null)
				{
					String wd;

					if (lastDir != null)
					{
						wd = lastDir;
					}
					else if (editor.getCurrentFile() != null)
					{
						wd = editor.getCurrentFile().getParent();
					}
					else
					{
						wd = System.getProperty("user.dir");
					}

					JFileChooser fc = new JFileChooser(wd);

					// Adds the default file format
					FileFilter defaultFilter = new DefaultFileFilter(".sip",
							"Ururau " + mxResources.get("file")
									+ " (.sip)");
					fc.addChoosableFileFilter(defaultFilter);

					// Adds special vector graphics formats and HTML
                    /*
					fc.addChoosableFileFilter(new DefaultFileFilter(".sip",
							"Ururau " + mxResources.get("file")
									+ " (.sip)"));
					fc.addChoosableFileFilter(new DefaultFileFilter(".txt",
							"Graph Drawing " + mxResources.get("file")
									+ " (.txt)"));
					fc.addChoosableFileFilter(new DefaultFileFilter(".svg",
							"SVG " + mxResources.get("file") + " (.svg)"));
					fc.addChoosableFileFilter(vmlFileFilter);
					fc.addChoosableFileFilter(new DefaultFileFilter(".html",
							"HTML " + mxResources.get("file") + " (.html)"));
                            */
					// Adds a filter for each supported image format
					Object[] imageFormats = ImageIO.getReaderFormatNames();

					// Finds all distinct extensions
					HashSet<String> formats = new HashSet<String>();

					for (int i = 0; i < imageFormats.length; i++)
					{
						String ext = imageFormats[i].toString().toLowerCase();
						formats.add(ext);
					}

					imageFormats = formats.toArray();

					for (int i = 0; i < imageFormats.length; i++)
					{
						String ext = imageFormats[i].toString();
						/*fc.addChoosableFileFilter(new DefaultFileFilter("."
								+ ext, ext.toUpperCase() + " "
								+ mxResources.get("file") + " (." + ext + ")"));*/
					}

					// Adds filter that accepts all supported image formats
                    /*
					fc.addChoosableFileFilter(new DefaultFileFilter.ImageFileFilter(
							mxResources.get("allImages")));*/
					fc.setFileFilter(defaultFilter);
					int rc = fc.showDialog(null, mxResources.get("save"));
					dialogShown = true;

					if (rc != JFileChooser.APPROVE_OPTION)
					{
						return;
					}
					else
					{
						lastDir = fc.getSelectedFile().getParent();
					}

					filename = fc.getSelectedFile().getAbsolutePath();
					selectedFilter = fc.getFileFilter();

					if (selectedFilter instanceof DefaultFileFilter)
					{
						String ext = ((DefaultFileFilter) selectedFilter)
								.getExtension();

						if (!filename.toLowerCase().endsWith(ext))
						{
							filename += ext;
						}
					}

					if (new File(filename).exists()
							&& JOptionPane.showConfirmDialog(graphComponent,
									mxResources.get("overwriteExistingFile")) != JOptionPane.YES_OPTION)
					{
						return;
					}
				}
				else
				{
					filename = editor.getCurrentFile().getAbsolutePath();
				}

				try
				{
					String ext = filename
							.substring(filename.lastIndexOf('.') + 1);

					if (ext.equalsIgnoreCase("svg"))
					{
						mxSvgCanvas canvas = (mxSvgCanvas) mxCellRenderer
								.drawCells(graph, null, 1, null,
										new CanvasFactory()
										{
											public mxICanvas createCanvas(
													int width, int height)
											{
												mxSvgCanvas canvas = new mxSvgCanvas(
														mxUtils.createSvgDocument(
																width, height));
												canvas.setEmbedded(true);

												return canvas;
											}

										});

						mxUtils.writeFile(mxUtils.getXml(canvas.getDocument()),
								filename);
					}
					else if (selectedFilter == vmlFileFilter)
					{
						mxUtils.writeFile(mxUtils.getXml(mxCellRenderer
								.createVmlDocument(graph, null, 1, null, null)
								.getDocumentElement()), filename);
					}
					else if (ext.equalsIgnoreCase("html"))
					{
						mxUtils.writeFile(mxUtils.getXml(mxCellRenderer
								.createHtmlDocument(graph, null, 1, null, null)
								.getDocumentElement()), filename);
					}
					else if (ext.equalsIgnoreCase("sip")
							|| ext.equalsIgnoreCase("xml"))
					{
						mxCodec codec = new mxCodec();
                        /*
                         * Saves document in Ururau format
                         */
                                                    String xml_graph = mxUtils.getXml(codec.encode(graph
                                                                    .getModel()));
                        String xml_experiment = mxUtils.getXml(codec.encode(USimulation.getInstance()));
                        String xml_communication = mxUtils.getXml(codec.encode(Communication.getInstance()));
                        String xml_optimization = mxUtils.getXml(codec.encode(Optimizer.getInstance()));
						mxUtils.writeFile("<ururau version=\"1.1\">"
                                +xml_graph+xml_experiment+xml_communication+xml_optimization+
                                "</ururau>", filename);

						editor.setModified(false);
						editor.setCurrentFile(new File(filename));
					}
					else
					{
						Color bg = null;

						if ((!ext.equalsIgnoreCase("gif") && !ext
								.equalsIgnoreCase("png"))
								|| JOptionPane.showConfirmDialog(
										graphComponent, mxResources
												.get("transparentBackground")) != JOptionPane.YES_OPTION)
						{
							bg = graphComponent.getBackground();
						}

                        /*
						if (selectedFilter == xmlPngFilter
								|| (editor.getCurrentFile() != null
										&& ext.equalsIgnoreCase("png") && !dialogShown))
						{
							saveXmlPng(editor, filename, bg);
						}
						else
						{
							BufferedImage image = mxCellRenderer
									.createBufferedImage(graph, null, 1, bg,
											graphComponent.isAntiAlias(), null,
											graphComponent.getCanvas());

							if (image != null)
							{
								ImageIO.write(image, ext, new File(filename));
							}
							else
							{
								JOptionPane.showMessageDialog(graphComponent,
										mxResources.get("noImageData"));
							}
						}
                         *
                         */
					}
				}
				catch (Throwable ex)
				{
					ex.printStackTrace();
					JOptionPane.showMessageDialog(graphComponent,
							ex.toString(), mxResources.get("error"),
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class SelectShortestPathAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected boolean directed;

		/**
		 * 
		 */
		public SelectShortestPathAction(boolean directed)
		{
			this.directed = directed;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				mxGraph graph = graphComponent.getGraph();
				mxIGraphModel model = graph.getModel();

				Object source = null;
				Object target = null;

				Object[] cells = graph.getSelectionCells();

				for (int i = 0; i < cells.length; i++)
				{
					if (model.isVertex(cells[i]))
					{
						if (source == null)
						{
							source = cells[i];
						}
						else if (target == null)
						{
							target = cells[i];
						}
					}

					if (source != null && target != null)
					{
						break;
					}
				}

				if (source != null && target != null)
				{
					int steps = graph.getChildEdges(graph.getDefaultParent()).length;
					Object[] path = mxGraphAnalysis.getInstance()
							.getShortestPath(graph, source, target,
									new mxDistanceCostFunction(), steps,
									directed);
					graph.setSelectionCells(path);
				}
				else
				{
					JOptionPane.showMessageDialog(graphComponent,
							mxResources.get("noSourceAndTargetSelected"));
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class SelectSpanningTreeAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected boolean directed;

		/**
		 * 
		 */
		public SelectSpanningTreeAction(boolean directed)
		{
			this.directed = directed;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				mxGraph graph = graphComponent.getGraph();
				mxIGraphModel model = graph.getModel();

				Object parent = graph.getDefaultParent();
				Object[] cells = graph.getSelectionCells();

				for (int i = 0; i < cells.length; i++)
				{
					if (model.getChildCount(cells[i]) > 0)
					{
						parent = cells[i];
						break;
					}
				}

				Object[] v = graph.getChildVertices(parent);
				Object[] mst = mxGraphAnalysis.getInstance()
						.getMinimumSpanningTree(graph, v,
								new mxDistanceCostFunction(), directed);
				graph.setSelectionCells(mst);
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleDirtyAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				graphComponent.showDirtyRectangle = !graphComponent.showDirtyRectangle;
			}
		}

	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleConnectModeAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				mxConnectionHandler handler = graphComponent
						.getConnectionHandler();
				handler.setHandleEnabled(!handler.isHandleEnabled());
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleCreateTargetItem extends JCheckBoxMenuItem
	{
		/**
		 * 
		 */
		public ToggleCreateTargetItem(final BasicGraphEditor editor, String name)
		{
			super(name);
			setSelected(true);

			addActionListener(new ActionListener()
			{
				/**
				 * 
				 */
				public void actionPerformed(ActionEvent e)
				{
					mxGraphComponent graphComponent = editor
							.getGraphComponent();

					if (graphComponent != null)
					{
						mxConnectionHandler handler = graphComponent
								.getConnectionHandler();
						handler.setCreateTarget(!handler.isCreateTarget());
						setSelected(handler.isCreateTarget());
					}
				}
			});
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class PromptPropertyAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected Object target;

		/**
		 * 
		 */
		protected String fieldname, message;

		/**
		 * 
		 */
		public PromptPropertyAction(Object target, String message)
		{
			this(target, message, message);
		}

		/**
		 * 
		 */
		public PromptPropertyAction(Object target, String message,
				String fieldname)
		{
			this.target = target;
			this.message = message;
			this.fieldname = fieldname;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof Component)
			{
				try
				{
					Method getter = target.getClass().getMethod(
							"get" + fieldname);
					Object current = getter.invoke(target);

					// TODO: Support other atomic types
					if (current instanceof Integer)
					{
						Method setter = target.getClass().getMethod(
								"set" + fieldname, new Class[] { int.class });

						String value = (String) JOptionPane.showInputDialog(
								(Component) e.getSource(), "Value", message,
								JOptionPane.PLAIN_MESSAGE, null, null, current);

						if (value != null)
						{
							setter.invoke(target, Integer.parseInt(value));
						}
					}
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
			}

			// Repaints the graph component
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				graphComponent.repaint();
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class TogglePropertyItem extends JCheckBoxMenuItem
	{
		/**
		 * 
		 */
		public TogglePropertyItem(Object target, String name, String fieldname)
		{
			this(target, name, fieldname, false);
		}

		/**
		 * 
		 */
		public TogglePropertyItem(Object target, String name, String fieldname,
				boolean refresh)
		{
			this(target, name, fieldname, refresh, null);
		}

		/**
		 * 
		 */
		public TogglePropertyItem(final Object target, String name,
				final String fieldname, final boolean refresh,
				ActionListener listener)
		{
			super(name);

			// Since action listeners are processed last to first we add the given
			// listener here which means it will be processed after the one below
			if (listener != null)
			{
				addActionListener(listener);
			}

			addActionListener(new ActionListener()
			{
				/**
				 * 
				 */
				public void actionPerformed(ActionEvent e)
				{
					execute(target, fieldname, refresh);
				}
			});

			PropertyChangeListener propertyChangeListener = new PropertyChangeListener()
			{

				/*
				 * (non-Javadoc)
				 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
				 */
				public void propertyChange(PropertyChangeEvent evt)
				{
					if (evt.getPropertyName().equalsIgnoreCase(fieldname))
					{
						update(target, fieldname);
					}
				}
			};

			if (target instanceof mxGraphComponent)
			{
				((mxGraphComponent) target)
						.addPropertyChangeListener(propertyChangeListener);
			}
			else if (target instanceof mxGraph)
			{
				((mxGraph) target)
						.addPropertyChangeListener(propertyChangeListener);
			}

			update(target, fieldname);
		}

		/**
		 * 
		 */
		public void update(Object target, String fieldname)
		{
			if (target != null && fieldname != null)
			{
				try
				{
					Method getter = target.getClass().getMethod(
							"is" + fieldname);

					if (getter != null)
					{
						Object current = getter.invoke(target);

						if (current instanceof Boolean)
						{
							setSelected(((Boolean) current).booleanValue());
						}
					}
				}
				catch (Exception e)
				{
					// ignore
				}
			}
		}

		/**
		 * 
		 */
		public void execute(Object target, String fieldname, boolean refresh)
		{
			if (target != null && fieldname != null)
			{
				try
				{
					Method getter = target.getClass().getMethod(
							"is" + fieldname);
					Method setter = target.getClass().getMethod(
							"set" + fieldname, new Class[] { boolean.class });

					Object current = getter.invoke(target);

					if (current instanceof Boolean)
					{
						boolean value = !((Boolean) current).booleanValue();
						setter.invoke(target, value);
						setSelected(value);
					}

					if (refresh)
					{
						mxGraph graph = null;

						if (target instanceof mxGraph)
						{
							graph = (mxGraph) target;
                                                        graph.refresh();
						}
						else if (target instanceof mxGraphComponent)
						{
							graph = ((mxGraphComponent) target).getGraph();
                                                        graph.refresh();
						}

						
					}
				}
				catch (Exception e)
				{
					// ignore
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class HistoryAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected boolean undo;

		/**
		 * 
		 */
		public HistoryAction(boolean undo)
		{
			this.undo = undo;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			BasicGraphEditor editor = getEditor(e);

			if (editor != null)
			{
				if (undo)
				{
					editor.getUndoManager().undo();
				}
				else
				{
					editor.getUndoManager().redo();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class FontStyleAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected boolean bold;

		/**
		 * 
		 */
		public FontStyleAction(boolean bold)
		{
			this.bold = bold;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				Component editorComponent = null;

				if (graphComponent.getCellEditor() instanceof mxCellEditor)
				{
					editorComponent = ((mxCellEditor) graphComponent
							.getCellEditor()).getEditor();
				}

				if (editorComponent instanceof JEditorPane)
				{
					JEditorPane editorPane = (JEditorPane) editorComponent;
					int start = editorPane.getSelectionStart();
					int ende = editorPane.getSelectionEnd();
					String text = editorPane.getSelectedText();

					if (text == null)
					{
						text = "";
					}

					try
					{
						HTMLEditorKit editorKit = new HTMLEditorKit();
						HTMLDocument document = (HTMLDocument) editorPane
								.getDocument();
						document.remove(start, (ende - start));
						editorKit.insertHTML(document, start, ((bold) ? "<b>"
								: "<i>") + text + ((bold) ? "</b>" : "</i>"),
								0, 0, (bold) ? HTML.Tag.B : HTML.Tag.I);
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
					}

					editorPane.requestFocus();
					editorPane.select(start, ende);
				}
				else
				{
					mxIGraphModel model = graphComponent.getGraph().getModel();
					model.beginUpdate();
					try
					{
						graphComponent.stopEditing(false);
						graphComponent.getGraph().toggleCellStyleFlags(
								mxConstants.STYLE_FONTSTYLE,
								(bold) ? mxConstants.FONT_BOLD
										: mxConstants.FONT_ITALIC);
					}
					finally
					{
						model.endUpdate();
					}
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class WarningAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				Object[] cells = graphComponent.getGraph().getSelectionCells();

				if (cells != null && cells.length > 0)
				{
					String warning = JOptionPane.showInputDialog(mxResources
							.get("enterWarningMessage"));

					for (int i = 0; i < cells.length; i++)
					{
						graphComponent.setCellWarning(cells[i], warning);
					}
				}
				else
				{
					JOptionPane.showMessageDialog(graphComponent,
							mxResources.get("noCellSelected"));
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class NewAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			BasicGraphEditor editor = getEditor(e);

			if (editor != null)
			{
				if (!editor.isModified()
						|| JOptionPane.showConfirmDialog(editor,
								mxResources.get("loseChanges")) == JOptionPane.YES_OPTION)
				{
					mxGraph graph = editor.getGraphComponent().getGraph();

					// Check modified flag and display save dialog
					mxCell root = new mxCell();
					root.insert(new mxCell());
					graph.getModel().setRoot(root);

					editor.setModified(false);
					editor.setCurrentFile(null);
					editor.getGraphComponent().zoomAndCenter();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ImportAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected String lastDir;

		/**
		 * Loads and registers the shape as a new shape in mxGraphics2DCanvas and
		 * adds a new entry to use that shape in the specified palette
		 * @param palette The palette to add the shape to.
		 * @param nodeXml The raw XML of the shape
		 * @param path The path to the directory the shape exists in
		 * @return the string name of the shape
		 */
		public static String addStencilShape(EditorPalette palette,
				String nodeXml, String path)
		{

			// Some editors place a 3 byte BOM at the start of files
			// Ensure the first char is a "<"
			int lessthanIndex = nodeXml.indexOf("<");
			nodeXml = nodeXml.substring(lessthanIndex);
			mxStencilShape newShape = new mxStencilShape(nodeXml);
			String name = newShape.getName();
			ImageIcon icon = null;

			if (path != null)
			{
				String iconPath = path + newShape.getIconPath();
				icon = new ImageIcon(iconPath);
			}

			// Registers the shape in the canvas shape registry
			mxGraphics2DCanvas.putShape(name, newShape);

			if (palette != null && icon != null)
			{
				palette.addTemplate(name, icon, "shape=" + name, 80, 80, Template.TemplateType.Terminate);
			}

			return name;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			BasicGraphEditor editor = getEditor(e);

			if (editor != null)
			{
				String wd = (lastDir != null) ? lastDir : System
						.getProperty("user.dir");

				JFileChooser fc = new JFileChooser(wd);

				fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

				// Adds file filter for Dia shape import
				fc.addChoosableFileFilter(new DefaultFileFilter(".shape",
						"Dia Shape " + mxResources.get("file") + " (.shape)"));

				int rc = fc.showDialog(null, mxResources.get("importStencil"));

				if (rc == JFileChooser.APPROVE_OPTION)
				{
					lastDir = fc.getSelectedFile().getParent();

					try
					{
						if (fc.getSelectedFile().isDirectory())
						{
							EditorPalette palette = editor.insertPalette(fc
									.getSelectedFile().getName());

							for (File f : fc.getSelectedFile().listFiles(
									new FilenameFilter()
									{
										public boolean accept(File dir,
												String name)
										{
											return name.toLowerCase().endsWith(
													".shape");
										}
									}))
							{
								String nodeXml = mxUtils.readFile(f
										.getAbsolutePath());
								addStencilShape(palette, nodeXml, f.getParent()
										+ File.separator);
							}

							JComponent scrollPane = (JComponent) palette
									.getParent().getParent();
							editor.getLibraryPane().setSelectedComponent(
									scrollPane);

							// FIXME: Need to update the size of the palette to force a layout
							// update. Re/in/validate of palette or parent does not work.
							//editor.getLibraryPane().revalidate();
						}
						else
						{
							String nodeXml = mxUtils.readFile(fc
									.getSelectedFile().getAbsolutePath());
							String name = addStencilShape(null, nodeXml, null);

							JOptionPane.showMessageDialog(editor, mxResources
									.get("stencilImported",
											new String[] { name }));
						}
					}
					catch (IOException e1)
					{
						e1.printStackTrace();
					}
				}
			}
		}
	}

        @SuppressWarnings("serial")
        public static class ConfigurationAction extends AbstractAction 
        
        {
            public ConfigurationAction ()
            {

            }

            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof ConfigurationView)
                {
                    System.err.println("Configuration");
                }

                Configuration conf = new Configuration();
                ConfigurationView confV = new ConfigurationView(conf);
                confV.setVisible(true);
                
            }
            
        }
	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class OpenAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected String lastDir;

		/**
		 * 
		 */
		protected void resetEditor(BasicGraphEditor editor)
		{
			editor.setModified(false);
			editor.getUndoManager().clear();
			editor.getGraphComponent().zoomAndCenter();
		}

		/**
		 * Reads XML+PNG format.
		 */
		protected void openXmlPng(BasicGraphEditor editor, File file)
				throws IOException
		{
			Map<String, String> text = mxPngTextDecoder
					.decodeCompressedText(new FileInputStream(file));

			if (text != null)
			{
				String value = text.get("mxGraphModel");

				if (value != null)
				{
					Document document = mxUtils.parseXml(URLDecoder.decode(
							value, "UTF-8"));
					mxCodec codec = new mxCodec(document);
					codec.decode(document.getDocumentElement(), editor
							.getGraphComponent().getGraph().getModel());
					editor.setCurrentFile(file);
					resetEditor(editor);

					return;
				}
			}

			JOptionPane.showMessageDialog(editor,
					mxResources.get("imageContainsNoDiagramData"));
		}


		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			BasicGraphEditor editor = getEditor(e);

			if (editor != null)
			{
				if (!editor.isModified()
						|| JOptionPane.showConfirmDialog(editor,
								mxResources.get("loseChanges")) == JOptionPane.YES_OPTION)
				{
					mxGraph graph = editor.getGraphComponent().getGraph();

					if (graph != null)
					{
						String wd = (lastDir != null) ? lastDir : System
								.getProperty("user.dir");

						JFileChooser fc = new JFileChooser(wd);

						// Adds file filter for supported file format
                        DefaultFileFilter defaultFilter = new DefaultFileFilter(".sip",
								"Ururau " + mxResources.get("file")
										+ " (.sip)");
                        /*
						DefaultFileFilter defaultFilter = new DefaultFileFilter(
								".sip", mxResources.get("allSupportedFormats")
										+ " (.sip, .png, .vdx)")
						{

							public boolean accept(File file)
							{
								String lcase = file.getName().toLowerCase();

								return super.accept(file)
										|| lcase.endsWith(".sip")
										|| lcase.endsWith(".vdx");
							}
						};*/

						fc.addChoosableFileFilter(defaultFilter);
/*
						fc.addChoosableFileFilter(new DefaultFileFilter(".sip",
								"Ururau " + mxResources.get("file")
										+ " (.sip)"));
                        
						fc.addChoosableFileFilter(new DefaultFileFilter(".png",
								"PNG+XML  " + mxResources.get("file")
										+ " (.png)"));

						// Adds file filter for VDX import
						fc.addChoosableFileFilter(new DefaultFileFilter(".vdx",
								"XML Drawing  " + mxResources.get("file")
										+ " (.vdx)"));

						// Adds file filter for GD import
						fc.addChoosableFileFilter(new DefaultFileFilter(".txt",
								"Graph Drawing  " + mxResources.get("file")
										+ " (.txt)"));
                                        */
						fc.setFileFilter(defaultFilter);

						int rc = fc.showDialog(null,
								mxResources.get("openFile"));

						if (rc == JFileChooser.APPROVE_OPTION)
						{
							lastDir = fc.getSelectedFile().getParent();

							try
							{
								if (fc.getSelectedFile().getAbsolutePath()
										.toLowerCase().endsWith(".png"))
								{
									openXmlPng(editor, fc.getSelectedFile());
								}
								else
								{
                                                                    String xmlString = mxUtils.readFile(fc.getSelectedFile().getAbsolutePath());
                                                                    xmlString = tryConvertFormat(xmlString,editor);
                                                                    if (xmlString.compareTo("") == 0)
                                                                    {
                                                                        return;
                                                                    }
									Document document = mxUtils.parseXml(xmlString);

									{

                                                                                /* Open Ururau document */
										mxCodec codec = new mxCodec(document);
										codec.decode(
    											document.getDocumentElement().getElementsByTagName("mxGraphModel").item(0),
												graph.getModel());
                                                                                if (document.getDocumentElement().getElementsByTagName("org.ururau.modules.USimulation").item(0) != null)
                                                                                {
                                                                                    codec.decode(document.getDocumentElement().getElementsByTagName("org.ururau.modules.USimulation").item(0),
                                                                                        USimulation.getInstance());
                                                                                }
                                                                                if (document.getDocumentElement().getElementsByTagName("org.ururau.Communication").item(0) != null)
                                                                                {
                                                                                    codec.decode(document.getDocumentElement().getElementsByTagName("org.ururau.Communication").item(0),
                                                                                        Communication.getInstance());
                                                                                }
                                                                                if (document.getDocumentElement().getElementsByTagName("org.ururau.Optimizer").item(0) != null)
                                                                                {
                                                                                    codec.decode(document.getDocumentElement().getElementsByTagName("org.ururau.Optimizer").item(0),
                                                                                        Optimizer.getInstance());
                                                                                }
										editor.setCurrentFile(fc
												.getSelectedFile());
									}

									resetEditor(editor);
								}
							}
							catch (IOException ex)
							{
								ex.printStackTrace();
								JOptionPane.showMessageDialog(
										editor.getGraphComponent(),
										ex.toString(),
										mxResources.get("error"),
										JOptionPane.ERROR_MESSAGE);
							}
						}
					}
				}
			}
		}

        private String tryConvertFormat(String xmlString, BasicGraphEditor editor) {
            if (xmlString.contains("ururau version=\"1.2\"")) {
                return xmlString;
            }
            if (xmlString.contains("ururau version=\"1.1\"")) {
                return xmlString;
            }
            else if (xmlString.contains("ururau version=\"1.0\""))
            {
                int option = JOptionPane.showConfirmDialog(editor.getGraphComponent(), 
                        String.format(mxResources.get("convertVersion"),"1.0","1.1" ), "Convert", JOptionPane.YES_NO_OPTION);
                if (option == JOptionPane.YES_OPTION)
                {
                    /* convert to 1.1 */
                    xmlString = xmlString.replace("/ururau/resources/", "/");
                    xmlString = xmlString.replace("ururau.", "org.ururau.");
                    xmlString = xmlString.replace("<U", "<org.ururau.modules.U");
                    xmlString = xmlString.replace("</U", "</org.ururau.modules.U");
                    xmlString = xmlString.replace("<Optimizer", "<org.ururau.Optimizer");
                    xmlString = xmlString.replace("</Optimizer", "</org.ururau.Optimizer");
                    xmlString = xmlString.replace("<GAVariable", "<org.ururau.GAVariable");
                    xmlString = xmlString.replace("</GAVariable", "</org.ururau.GAVariable");
                    xmlString = xmlString.replace("<ReadWriteVariable", "<org.ururau.ReadWriteVariable");
                    xmlString = xmlString.replace("</ReadWriteVariable", "</org.ururau.ReadWriteVariable");
                    xmlString = xmlString.replace("<Communication", "<org.ururau.Communication");
                    xmlString = xmlString.replace("</Communication", "</org.ururau.Communication");
                    xmlString = xmlString.replace("templateCommands", "modules");
                    xmlString = xmlString.replace("UMyEmissions", "UEmissions");
                    xmlString = xmlString.replace("ururau version=\"1.0\"", "ururau version=\"1.1\"");
                    return xmlString;
                } else {
                    return "";
                }
            } else if (xmlString.contains("ururau version=\"0.6\"")) {
                int option = JOptionPane.showConfirmDialog(editor.getGraphComponent(), 
                        String.format(mxResources.get("convertVersion"),"0.6","1.1" ), "Convert", JOptionPane.YES_NO_OPTION);
                if (option == JOptionPane.YES_OPTION)
                {
                    /* convert to 1.1 */
                    xmlString = xmlString.replace("/ururau/resources/", "/");
                    xmlString = xmlString.replace("ururau.", "org.ururau.");
                    xmlString = xmlString.replace("<U", "<org.ururau.modules.U");
                    xmlString = xmlString.replace("</U", "</org.ururau.modules.U");
                    xmlString = xmlString.replace("<Optimizer", "<org.ururau.Optimizer");
                    xmlString = xmlString.replace("</Optimizer", "</org.ururau.Optimizer");
                    xmlString = xmlString.replace("<Communication", "<org.ururau.Communication");
                    xmlString = xmlString.replace("</Communication", "</org.ururau.Communication");
                    xmlString = xmlString.replace("<GAVariable", "<org.ururau.GAVariable");
                    xmlString = xmlString.replace("</GAVariable", "</org.ururau.GAVariable");
                    xmlString = xmlString.replace("templateCommands", "modules");
                    xmlString = xmlString.replace("UMyEmissions", "UEmissions");
                    xmlString = xmlString.replace("ururau version=\"0.6\"", "ururau version=\"1.1\"");
                    return xmlString;
                } else {
                    return "";
                }
            }else if (xmlString.contains("ururau version=\"0.5\"")) {
                int option = JOptionPane.showConfirmDialog(editor.getGraphComponent(), 
                        String.format(mxResources.get("convertVersion"),"0.5","1.1" ), "Convert", JOptionPane.YES_NO_OPTION);
                if (option == JOptionPane.YES_OPTION)
                {
                    /* convert to 1.1 */
                    xmlString = xmlString.replace("/ururau/resources/", "/");
                    xmlString = xmlString.replace("ururau.", "org.ururau.");
                    xmlString = xmlString.replace("<U", "<org.ururau.modules.U");
                    xmlString = xmlString.replace("</U", "</org.ururau.modules.U");
                    xmlString = xmlString.replace("<Communication", "<org.ururau.Communication");
                    xmlString = xmlString.replace("ururau version=\"0.5\"", "ururau version=\"1.1\"");
                    return xmlString;
                } else {
                    return "";
                }
            }
            else {
                return "";
            }
	}
        }

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ToggleAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected String key;

		/**
		 * 
		 */
		protected boolean defaultValue;

		/**
		 * 
		 * @param key
		 */
		public ToggleAction(String key)
		{
			this(key, false);
		}

		/**
		 * 
		 * @param key
		 */
		public ToggleAction(String key, boolean defaultValue)
		{
			this.key = key;
			this.defaultValue = defaultValue;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			mxGraph graph = mxGraphActions.getGraph(e);

			if (graph != null)
			{
				graph.toggleCellStyles(key, defaultValue);
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class SetLabelPositionAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected String labelPosition, alignment;

		/**
		 * 
		 * @param key
		 */
		public SetLabelPositionAction(String labelPosition, String alignment)
		{
			this.labelPosition = labelPosition;
			this.alignment = alignment;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			mxGraph graph = mxGraphActions.getGraph(e);

			if (graph != null && !graph.isSelectionEmpty())
			{
				graph.getModel().beginUpdate();
				try
				{
					// Checks the orientation of the alignment to use the correct constants
					if (labelPosition.equals(mxConstants.ALIGN_LEFT)
							|| labelPosition.equals(mxConstants.ALIGN_CENTER)
							|| labelPosition.equals(mxConstants.ALIGN_RIGHT))
					{
						graph.setCellStyles(mxConstants.STYLE_LABEL_POSITION,
								labelPosition);
						graph.setCellStyles(mxConstants.STYLE_ALIGN, alignment);
					}
					else
					{
						graph.setCellStyles(
								mxConstants.STYLE_VERTICAL_LABEL_POSITION,
								labelPosition);
						graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN,
								alignment);
					}
				}
				finally
				{
					graph.getModel().endUpdate();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class SetStyleAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected String value;

		/**
		 * 
		 * @param key
		 */
		public SetStyleAction(String value)
		{
			this.value = value;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			mxGraph graph = mxGraphActions.getGraph(e);

			if (graph != null && !graph.isSelectionEmpty())
			{
				graph.setCellStyle(value);
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class KeyValueAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected String key, value;

		/**
		 * 
		 * @param key
		 */
		public KeyValueAction(String key)
		{
			this(key, null);
		}

		/**
		 * 
		 * @param key
		 */
		public KeyValueAction(String key, String value)
		{
			this.key = key;
			this.value = value;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			mxGraph graph = mxGraphActions.getGraph(e);

			if (graph != null && !graph.isSelectionEmpty())
			{
				graph.setCellStyles(key, value);
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class PromptValueAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected String key, message;

		/**
		 * 
		 * @param key
		 */
		public PromptValueAction(String key, String message)
		{
			this.key = key;
			this.message = message;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof Component)
			{
				mxGraph graph = mxGraphActions.getGraph(e);

				if (graph != null && !graph.isSelectionEmpty())
				{
					String value = (String) JOptionPane.showInputDialog(
							(Component) e.getSource(),
							mxResources.get("value"), message,
							JOptionPane.PLAIN_MESSAGE, null, null, "");

					if (value != null)
					{
						if (value.equals(mxConstants.NONE))
						{
							value = null;
						}

						graph.setCellStyles(key, value);
					}
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class AlignCellsAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected String align;

		/**
		 * 
		 * @param key
		 */
		public AlignCellsAction(String align)
		{
			this.align = align;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			mxGraph graph = mxGraphActions.getGraph(e);

			if (graph != null && !graph.isSelectionEmpty())
			{
				graph.alignCells(align);
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class AutosizeAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			mxGraph graph = mxGraphActions.getGraph(e);

			if (graph != null && !graph.isSelectionEmpty())
			{
				Object[] cells = graph.getSelectionCells();
				mxIGraphModel model = graph.getModel();

				model.beginUpdate();
				try
				{
					for (int i = 0; i < cells.length; i++)
					{
						graph.updateCellSize(cells[i]);
					}
				}
				finally
				{
					model.endUpdate();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class ColorAction extends AbstractAction
	{
		/**
		 * 
		 */
		protected String name, key;

		/**
		 * 
		 * @param key
		 */
		public ColorAction(String name, String key)
		{
			this.name = name;
			this.key = key;
		}

		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				mxGraph graph = graphComponent.getGraph();

				if (!graph.isSelectionEmpty())
				{
					Color newColor = JColorChooser.showDialog(graphComponent,
							name, null);

					if (newColor != null)
					{
						graph.setCellStyles(key, mxUtils.hexString(newColor));
					}
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class BackgroundImageAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				String value = (String) JOptionPane.showInputDialog(
						graphComponent, mxResources.get("backgroundImage"),
						"URL", JOptionPane.PLAIN_MESSAGE, null, null,
						"http://www.callatecs.com/images/background2.JPG");

				if (value != null)
				{
					if (value.length() == 0)
					{
						graphComponent.setBackgroundImage(null);
					}
					else
					{
						Image background = mxUtils.loadImage(value);
						// Incorrect URLs will result in no image.
						// TODO provide feedback that the URL is not correct
						if (background != null)
						{
							graphComponent.setBackgroundImage(new ImageIcon(
									background));
						}
					}

					// Forces a repaint of the outline
					graphComponent.getGraph().repaint();
				}
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class BackgroundAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				Color newColor = JColorChooser.showDialog(graphComponent,
						mxResources.get("background"), null);

				if (newColor != null)
				{
					graphComponent.getViewport().setOpaque(true);
					graphComponent.getViewport().setBackground(newColor);
				}

				// Forces a repaint of the outline
				graphComponent.getGraph().repaint();
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class PageBackgroundAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				Color newColor = JColorChooser.showDialog(graphComponent,
						mxResources.get("pageBackground"), null);

				if (newColor != null)
				{
					graphComponent.setPageBackgroundColor(newColor);
				}

				// Forces a repaint of the component
				graphComponent.repaint();
			}
		}
	}

	/**
	 *
	 */
	@SuppressWarnings("serial")
	public static class StyleAction extends AbstractAction
	{
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				mxGraph graph = graphComponent.getGraph();
				String initial = graph.getModel().getStyle(
						graph.getSelectionCell());
				String value = (String) JOptionPane.showInputDialog(
						graphComponent, mxResources.get("style"),
						mxResources.get("style"), JOptionPane.PLAIN_MESSAGE,
						null, null, initial);

				if (value != null)
				{
					graph.setCellStyle(value);
				}
			}
		}
	}

    public static class TemplateEditAction extends AbstractAction
    {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() instanceof mxGraphComponent)
			{
				mxGraphComponent graphComponent = (mxGraphComponent) e
						.getSource();
				mxGraph graph = graphComponent.getGraph();
                Object value = graph.getModel().getValue(
						graph.getSelectionCell());
                
                if (value instanceof UCreate)
                {
                    UCreateView c = new UCreateView((UCreate)value);
                    c.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), c.getCreate());
                }

                else if(value instanceof UFunction)
                {
                    UFunctionView f = new UFunctionView((UFunction)value);
                    f.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), f.getFunction());
                }
                else if(value instanceof UInventorFunction)
                {
                    UInventorFunctionView f = new UInventorFunctionView((UInventorFunction)value);
                    f.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), f.getFunction());
                }
                else if(value instanceof ULocal)
                {
                    ULocalView l = new ULocalView((ULocal)value);
                    l.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), l.getLocal());
                }
                else if(value instanceof UResource)
                {
                    UResourceView p = new UResourceView((UResource)value);
                    p.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), p.getResource());
                }
                else if(value instanceof UXor)
                {
                    UXorView x = new UXorView((UXor)value);
                    x.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), x.getXor());
                }
                else if(value instanceof UXorBranch)
                {
                    if ( (((UXorBranch)value).getXorType() == DecideType.TWO_WAY_BY_CHANCE.ordinal())
                        || (((UXorBranch)value).getXorType() == DecideType.N_WAY_BY_CHANCE.ordinal()) )
                    {
                      
                        UXorBranchChanceView x = new UXorBranchChanceView((UXorBranch)value);
                        x.setVisible(true);
                        graph.getModel().setValue(graph.getSelectionCell(), x.getXorBranch());
                    } else if 
                        ( (((UXorBranch)value).getXorType() == DecideType.TWO_WAY_BY_CONDITION.ordinal())
                        || (((UXorBranch)value).getXorType() == DecideType.N_WAY_BY_CONDITION.ordinal()) )
                    {
                        UXorBranchConditionView x = new UXorBranchConditionView((UXorBranch)value);
                        x.setVisible(true);
                        graph.getModel().setValue(graph.getSelectionCell(), x.getXorBranch());                        
                    }  else if 
                        ( (((UXorBranch)value).getXorType() == DecideType.TWO_WAY_BY_RNA.ordinal()))
                    {
                        UXorBranchRnaView x = new UXorBranchRnaView((UXorBranch)value);
                        x.setVisible(true);
                        graph.getModel().setValue(graph.getSelectionCell(), x.getXorBranch());                        
                    }          
                }
                else if(value instanceof URecord)
                {
                    //((Template)value).setGraph(graph);
                    URecordView r = new URecordView((URecord)value);
                    r.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), r.getRecord());
                }
                else if(value instanceof UAssign)
                {
                    UAssignView a = null;
                    a = new UAssignView((UAssign)value);
                    a.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), a.getAssign());
                }
                else if(value instanceof UBatch)
                {
                    UBatchView a = new UBatchView((UBatch)value);
                    a.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), a.getBatch());
                }
                else if(value instanceof USeparate)
                {
                    USeparateView a = new USeparateView((USeparate)value);
                    a.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), a.getSeparate());
                }
                else if(value instanceof UJump)
                {
                    if ( ((mxCell)graph.getSelectionCell()).isVertex()) {
                        UJumpView j = new UJumpView((UJump)value);
                        j.setVisible(true);
                        graph.getModel().setValue(graph.getSelectionCell(), j.getJump());
                    }
                }
                else if(value instanceof UHold)
                {
                    UHoldView h = new UHoldView((UHold)value);
                    h.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), h.getHold());
                }
                else if(value instanceof UWrite)
                {
                    UWriteView w = new UWriteView((UWrite)value);
                    w.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), w.getWrite());
                }
                else if(value instanceof USheetReadWrite)
                {
                    USheetReadWriteView w = new USheetReadWriteView((USheetReadWrite)value);
                    w.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), w.getSheetReadWrite());
                }
                else if(value instanceof URunMacro)
                {
                    URunMacroView w = new URunMacroView((URunMacro)value);
                    w.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), w.getURunMacro());
                }
                else if(value instanceof UEmissions)
                {
                    UEmissionsView w = new UEmissionsView((UEmissions)value);
                    w.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), w.getMyEmissions());
                }
                else if(value instanceof UVariable)
                {
                    UVariableView a = null;
                    a = new UVariableView((UVariable)value);
                    a.setVisible(true);
                    graph.getModel().setValue(graph.getSelectionCell(), a.getVariable());
                }
                graphComponent.updateComponents();
            }
        }

        public static class EndAction extends AbstractAction
        {
            protected BasicGraphEditor editor;
            
            public EndAction(BasicGraphEditor editor)
            {
                this.editor = editor;
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                USimulation.getInstance().end();
            }
        }
        public static class RunAction extends AbstractAction
        {

            protected BasicGraphEditor editor;
            
            public RunAction(BasicGraphEditor editor)
            {
                this.editor = editor;
            }

            @Override
            public void actionPerformed(ActionEvent e) {


                if (e.getSource() instanceof mxGraphComponent)
                {
                    mxGraphComponent graphComponent = (mxGraphComponent) e
                            .getSource();
                    mxGraph graph = graphComponent.getGraph();
                        try {
                            if (USimulation.getThread() != null)
                            {
                                if (USimulation.getThread().isAlive())
                                {
                                    graphComponent.setLocation(800, 900);
                                    JOptionPane.showMessageDialog(graphComponent, mxResources.get("simulationRunning"), mxResources.get("running"), JOptionPane.WARNING_MESSAGE);
                                    //new CustomDialog(graphComponent,false,mxResources.get("simulationRunning"));
                                    return;
                                }
                            }
                            USimulation.getInstance().resetSeed();
                            USimulation.getInstance().setUp(editor.getBasename());
                            UModel umodel = new UModel(graph, USimulation.getInstance().getSimulation());
                            //USimulation.getInstance().resetSeed();
                            USimulation.getInstance().setUModel(umodel);
                            USimulation.resetThread();
                            USimulation.getThread().start();
                            
                            //editor.simulation.run();
                            //System.out.println(mxResources.get("done"));
                        } catch (Exception ex) {
                            Logger.getLogger(EditorActions.class.getName()).log(Level.SEVERE, null, ex);
                            ErrorMsg.displayMsg("run_error", null, null, ex);
                        }
                }
            }
        }

        public static class ExperimentAction extends AbstractAction
        {

            protected BasicGraphEditor editor;

            public ExperimentAction(BasicGraphEditor editor)
            {
                this.editor = editor;
            }

            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof USimulation)
                {
                    System.err.println("TExperiment");
                }

                USimulationView sim = new USimulationView(USimulation.getInstance());
                sim.setVisible(true);
                editor.simulation = sim.getSimulation();
            }

        }

        public static class CommunicationAction extends AbstractAction
        {

            public CommunicationAction()
            {
            }

            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof CommunicationView)
                {
                    System.err.println("Communication");
                }

                CommunicationView com = new CommunicationView();
                com.setVisible(true);
            }

        }

        public static class OptimizeAction extends AbstractAction
        {

            public OptimizeAction()
            {
            }

            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof OptimizerView)
                {
                    System.err.println("Optimize");
                }

                OptimizerView gap = new OptimizerView();
                gap.setVisible(true);
            }

        }
        
        public static class RunWithOptimizerAction extends AbstractAction
        {

            protected BasicGraphEditor editor;
            
            public RunWithOptimizerAction(BasicGraphEditor editor)
            {
                this.editor = editor;
            }

            public void actionPerformed(ActionEvent e) {


                if (e.getSource() instanceof mxGraphComponent)
                {
                    GoView goView = new GoView();
                    mxGraphComponent graphComponent = (mxGraphComponent) e
                            .getSource();
                    mxGraph graph = graphComponent.getGraph();
                    UModel model;
                        try {
                            
                            goView.setVisible(true);
                            USimulation.getInstance().setUp(editor.getBasename());
                            UModel umodel = new UModel(graph, USimulation.getInstance().getSimulation());
                            //* Get Optimizer Params */
                            /* Add Variables in the Model */
                            /* Build Optmization Model */
                            /* Run Model until stop */
                            Optimizer optmizerModel = Optimizer.getInstance();
                            optmizerModel.addVariablesToModel(umodel);

                            OptimizerConfiguration conf = new OptimizerConfiguration();
                            conf.configure();
                            conf.run();
                            //umodel.run();
                            //editor.simulation.run();
                            goView.setVisible(false);

                            ReportGenerator r = new ReportGenerator();
                            String filename = "";
                            if (umodel.getName() == null)
                            {
                                filename = mxResources.get("noName");
                            } else {
                                filename = umodel.getName();
                            }
                            filename = filename.replace(".sip", "");
                            filename = filename.replace(" ", "_");
                            filename = filename + ".pdf";
                            // TODO: make reports in pdf
                            
                            r.createPdf(filename, USimulation.getInstance().getSimulation());
                            r.showReport(filename);

                            //System.out.println(mxResources.get("done"));
                        } catch (Exception ex) {
                            Logger.getLogger(EditorActions.class.getName()).log(Level.SEVERE, null, ex);
                            ErrorMsg.displayMsg("run_error", null, null, ex);
                            goView.setVisible(false);
                        }
                }
            }
        }

        
        public static class SelectLanguageAction extends AbstractAction
        {
            protected String localeSettings;
            
            public SelectLanguageAction (String languageSettings)
            {
                this.localeSettings = languageSettings;
            }
            
            public void actionPerformed(ActionEvent ae) {
                Preferences prefs = Preferences.userRoot().node("ururau");
                prefs.put("locale", this.localeSettings);
                StringTokenizer tk = new StringTokenizer(localeSettings,"_");
                JOptionPane.showMessageDialog(null,mxResources.get("restartWarning"));
            }
            
        }
        
        public static class GoodnessOfFitAction extends AbstractAction
        {

            public void actionPerformed(ActionEvent ae) {
                GoodnessOfFitView gof = new GoodnessOfFitView();
                gof.setVisible(true);
            }
            
        }

    }
}
