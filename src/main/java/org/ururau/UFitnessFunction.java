/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.resource.Resource;
import jsl.modeling.elements.variable.Variable;
import org.jgap.FitnessFunction;
import org.jgap.IChromosome;
import org.ururau.javacc.parser.Expression;
import org.ururau.javacc.parser.ParseException;

import org.ururau.modules.USimulation;
import org.ururau.variable.CustomVariable;

/**
 *
 * @author tulio
 */
public class UFitnessFunction extends FitnessFunction {

    private Optimizer optimizerModel;
    private String objective;
    private USimulation simulation;
    public UFitnessFunction() {
    }
    
    @Override
    protected double evaluate(IChromosome a_subject) {
        ArrayList<GAVariable> gaVariables = optimizerModel.getGaVariables();
        objective = optimizerModel.getObjective();
        /* Set variables values from chromossome */
        for (int i =0; i < a_subject.size(); i++)
        {
            Integer var = (Integer)a_subject.getGene(i).getAllele();
            String varName = gaVariables.get(i).getName();
            ModelElement modelElement = simulation.getSimulation().getModel().getModelElement(varName);
            ModelElement modelElementRespVar = simulation.getSimulation().getModel().getModelElement(varName+".RespVar");
            if (modelElement != null)
            {
                if (modelElement instanceof Variable)
                {
                    ((Variable)modelElement).setInitialValue(var.doubleValue());
                }
                if (modelElement instanceof Resource) 
                {
                    ((Resource)modelElement).setInitialCapacity(var.intValue());
                }
            }
            if (modelElementRespVar != null)
            {
                ((CustomVariable)modelElementRespVar).setInitialValue(var.doubleValue());
            }
        }
        /* run Simulation */
        simulation.resetSeed();
        
        simulation.getSimulation().runAll();
        /* try evaluate a value in objective function */
        try {
            
            double value = Expression.getValue(simulation.getSimulation().getModel(), 
                    simulation.getSimulation(), 
                    objective);
            if (!optimizerModel.isMinimizing())
            {
                return value;
            } else {
                return 1/value;
            }
        } catch (ParseException ex) {
            Logger.getLogger(UFitnessFunction.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public Optimizer getOptimizerModel() {
        return optimizerModel;
    }

    public void setOptimizerModel(Optimizer optimizerModel) {
        this.optimizerModel = optimizerModel;
    }

    public USimulation getSimulation() {
        return simulation;
    }

    public void setSimulation(USimulation simulation) {
        this.simulation = simulation;
    }

}
