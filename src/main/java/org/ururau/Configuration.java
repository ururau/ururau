/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau;

import java.util.prefs.Preferences;

/**
 *
 * @author tulio
 */
public class Configuration {
    public String jnaPath;

    public Configuration()
    {
        Preferences prefs = Preferences.userRoot().node("ururau");
        jnaPath = prefs.get("jnaLibraryPath","/usr/local/lib");
 
    }
    
    public String getJnaPath() {
        return jnaPath;
    }

    public void setJnaPath(String jnaPath) {
        this.jnaPath = jnaPath;
    }
    
    public void save()
    {
        Preferences prefs = Preferences.userRoot().node("ururau");
        prefs.put("jnaLibraryPath", jnaPath);
    }
}
