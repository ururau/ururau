/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau;

import com.mxgraph.util.mxResources;
import javax.swing.JOptionPane;
import jsl.modeling.Experiment;

/**
 *
 * @author tulio
 */
public class ErrorMsg {

    public static boolean isDisplayErrorInDialog() {
        return displayErrorInDialog;
    }

    public static void setDisplayErrorInDialog(boolean displayErrorInDialog) {
        ErrorMsg.displayErrorInDialog = displayErrorInDialog;
    }
    
    static boolean displayErrorInDialog = false;
    
    public static void displayMsg(String msg, String param, String title, Exception ex)
    {
        if (ErrorMsg.isDisplayErrorInDialog())
            JOptionPane.showMessageDialog(null, "<html>"+mxResources.get(msg)+param+"</html>\n\nError: "
                        +ex.getMessage(), title, JOptionPane.ERROR_MESSAGE);
    }
}
