/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau;

import com.mxgraph.util.mxResources;
import com.mxgraph.view.mxGraph;
import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import org.ururau.commands.Batch.BatchType;
import org.ururau.commands.Decide.DecideType;
import org.ururau.commands.MyWrite.MyWriteType;
import org.ururau.commands.Write.WriteType;


/**
 *
 * @author tulio
 */
public abstract class Template implements Serializable {
   
    private static final long serialVersionUID = 7521471112122479111L;
    private static int lineCounter = 0;
    protected String idsim;
    protected String name;
    protected TemplateType type ;
    protected int lineCodeNumber = -1;
    protected mxGraph graph;
    protected boolean commandInserted;

    public static enum TemplateType {Create, Local, Function, Terminate, 
                                     Resource, And, Xor, Jump, Assign, Record,
                                     Batch, Separate, Hold, Write, MyWrite,
                                     MyEmissions, SheetReadWrite, RunMacro, 
                                     InventorFunction, Variable};

    public Template(String id, String name)
    {
        this.idsim = id;
        this.name = name;
    }

    public Template()
    {
    }

    public TemplateType getType() {
        return type;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(TemplateType type) {
        this.type = type;
    }

    public String getIdsim() {
        return idsim;
    }

    public void setIdsim(String idsim) {
        this.idsim = idsim;
    }

    public int getLineCodeNumber() {
        return lineCodeNumber;
    }

    public void setLineCodeNumber(int lineCodeNumber) {
        this.lineCodeNumber = lineCodeNumber;
    }

    public mxGraph getGraph() {
        return graph;
    }

    public void setGraph(mxGraph graph) {
        this.graph = graph;
    }

    public static void resetLineCounter()
    {
        lineCounter = -1;
    }

    public static int incrementLineCounter()
    {
        lineCounter++;
        return lineCounter;
    }
    
    public static int decrementLineCounter()
    {
        lineCounter--;
        return lineCounter;
    }
    
    public static int getLineCounter()
    {
        return lineCounter;
    }
    
    public boolean isCommandInserted() {
        return commandInserted;
    }

    public void setCommandInserted(boolean commandInserted) {
        this.commandInserted = commandInserted;
    }
    
    public abstract ProcessCommand getProcessCommand(ModelElement parent);

    //Porque esses metodos ?
    
    public static String [] getVariableTypeNames() {
        String vtName [] = new String[VariableType.values().length];
        for (VariableType vt : VariableType.values())
        {
            vtName[vt.ordinal()] = mxResources.get(vt.name());
        }
        return vtName;
    }

    public static String [] getLocalTypeNames() {
        String lName [] = new String[LocalType.values().length];
        for (LocalType l : LocalType.values())
        {
            lName[l.ordinal()] = mxResources.get(l.name());
        }
        return lName;
    }

    public static String [] getXorTypeNames() {
        String xName [] = new String[DecideType.values().length];
        for (DecideType x : DecideType.values())
        {
            xName[x.ordinal()] = mxResources.get(x.name());
        }
        return xName;
    }
    
    public static String [] getBatchTypeNames() {
        String bName [] = new String[BatchType.values().length];
        for (BatchType b : BatchType.values())
        {
            bName[b.ordinal()] = mxResources.get(b.name());
        }
        return bName;
    }
    
    public static String [] getWriteTypeNames() {
        String wName [] = new String[WriteType.values().length];
        for (WriteType w : WriteType.values())
        {
            wName[w.ordinal()] = mxResources.get(w.name());
        }
        
        return wName;
    }
    
    public static String [] getMyWriteTypeNames() {
        String wName [] = new String[MyWriteType.values().length];
        for (MyWriteType w : MyWriteType.values())
        {
            wName[w.ordinal()] = mxResources.get(w.name());
        }
        
        return wName;
    }
}
