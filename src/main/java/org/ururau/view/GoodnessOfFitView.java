/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.view;

import com.mxgraph.util.mxResources;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptException;
import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.ururau.ErrorMsg;
import org.ururau.GoodnessOfFit;
import org.ururau.ViewValidation;

/**
 *
 * @author tulio
 */
public class GoodnessOfFitView extends javax.swing.JFrame {

    /**
     * Creates new form GoodnessOfFitView
     */

    private GoodnessOfFit gof;
    /** Creates new form GoodnessOfFitView */
    public GoodnessOfFitView() {
        initComponents();
        jComboBoxDistribution.setEnabled(false);
    }

    public GoodnessOfFit getGoodnessOfFit()
    {
        return gof;
    }
    ViewValidation validar = new ViewValidation();
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelInputData = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaInputData = new javax.swing.JTextArea();
        jInternalFrameHistogram = new javax.swing.JInternalFrame();
        jPanelChooseDistribution = new javax.swing.JPanel();
        jComboBoxDistribution = new javax.swing.JComboBox();
        jTextFieldExpression = new javax.swing.JTextField();
        jPanelResults = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaResults = new javax.swing.JTextArea();
        jButtonClose = new javax.swing.JToggleButton();
        jButtonGo = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("Bundle"); // NOI18N
        setTitle(bundle.getString("GoodnessOfFitView.title")); // NOI18N

        jPanelInputData.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("GoodnessOfFitView.jPanelInputData.border.title"))); // NOI18N

        jTextAreaInputData.setColumns(20);
        jTextAreaInputData.setRows(5);
        jScrollPane1.setViewportView(jTextAreaInputData);

        javax.swing.GroupLayout jPanelInputDataLayout = new javax.swing.GroupLayout(jPanelInputData);
        jPanelInputData.setLayout(jPanelInputDataLayout);
        jPanelInputDataLayout.setHorizontalGroup(
            jPanelInputDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanelInputDataLayout.setVerticalGroup(
            jPanelInputDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)
        );

        jInternalFrameHistogram.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("GoodnessOfFitView.jInternalFrameHistogram.border.border.title")))); // NOI18N
        jInternalFrameHistogram.setVisible(true);

        javax.swing.GroupLayout jInternalFrameHistogramLayout = new javax.swing.GroupLayout(jInternalFrameHistogram.getContentPane());
        jInternalFrameHistogram.getContentPane().setLayout(jInternalFrameHistogramLayout);
        jInternalFrameHistogramLayout.setHorizontalGroup(
            jInternalFrameHistogramLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jInternalFrameHistogramLayout.setVerticalGroup(
            jInternalFrameHistogramLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanelChooseDistribution.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("GoodnessOfFitView.jPanelChooseDistribution.border.title"))); // NOI18N

        jComboBoxDistribution.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Exponential", "Normal" }));
        jComboBoxDistribution.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxDistributionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelChooseDistributionLayout = new javax.swing.GroupLayout(jPanelChooseDistribution);
        jPanelChooseDistribution.setLayout(jPanelChooseDistributionLayout);
        jPanelChooseDistributionLayout.setHorizontalGroup(
            jPanelChooseDistributionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jComboBoxDistribution, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jTextFieldExpression)
        );
        jPanelChooseDistributionLayout.setVerticalGroup(
            jPanelChooseDistributionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelChooseDistributionLayout.createSequentialGroup()
                .addComponent(jComboBoxDistribution, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldExpression, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 90, Short.MAX_VALUE))
        );

        jPanelResults.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("GoodnessOfFitView.jPanelResults.border.title"))); // NOI18N

        jTextAreaResults.setColumns(20);
        jTextAreaResults.setRows(5);
        jTextAreaResults.setEnabled(false);
        jScrollPane2.setViewportView(jTextAreaResults);

        javax.swing.GroupLayout jPanelResultsLayout = new javax.swing.GroupLayout(jPanelResults);
        jPanelResults.setLayout(jPanelResultsLayout);
        jPanelResultsLayout.setHorizontalGroup(
            jPanelResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
        );
        jPanelResultsLayout.setVerticalGroup(
            jPanelResultsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );

        jButtonClose.setText(bundle.getString("GoodnessOfFitView.jButtonClose.text")); // NOI18N
        jButtonClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCloseActionPerformed(evt);
            }
        });

        jButtonGo.setText(bundle.getString("GoodnessOfFitView.jButtonGo.text")); // NOI18N
        jButtonGo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanelChooseDistribution, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanelInputData, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanelResults, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jInternalFrameHistogram)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonGo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonClose)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanelInputData, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jInternalFrameHistogram))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanelChooseDistribution, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 148, Short.MAX_VALUE))
                    .addComponent(jPanelResults, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonClose)
                    .addComponent(jButtonGo)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBoxDistributionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxDistributionActionPerformed
    double [] obsFreq = gof.getObservedFrequency(jComboBoxDistribution.getSelectedIndex());
    double [] expFreq = gof.getExpectedFrequency(jComboBoxDistribution.getSelectedIndex());
    jInternalFrameHistogram.setContentPane(this.doHistogram(obsFreq, expFreq));
    jInternalFrameHistogram.pack();
    jInternalFrameHistogram.setVisible(true);
    jTextFieldExpression.setText(gof.getExpression(jComboBoxDistribution.getSelectedIndex()));
    }//GEN-LAST:event_jComboBoxDistributionActionPerformed

    private void jButtonGoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGoActionPerformed
    if(validar.goodnessFit(jTextAreaInputData, mxResources.get("input")))
    {        
        doTest();
        jTextAreaResults.setText(mxResources.get("otherDistributions")+":\n"+gof.getOtherDistributions()+"\n"+mxResources.get("bestFit")+":\n"+gof.toString());
        double [] obsFreq = gof.getObservedFrequency(gof.getBestDistributionIndex());
        double [] expFreq = gof.getExpectedFrequency(gof.getBestDistributionIndex());
        jInternalFrameHistogram.setContentPane(this.doHistogram(obsFreq, expFreq));
        jInternalFrameHistogram.pack();
        jInternalFrameHistogram.setVisible(true);
        jComboBoxDistribution.setSelectedIndex(gof.getBestDistributionIndex());
        jTextFieldExpression.setText(gof.getExpression(gof.getBestDistributionIndex()));
    }
    
    }//GEN-LAST:event_jButtonGoActionPerformed

    private void jButtonCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCloseActionPerformed
            if (gof != null)    
            gof.setDistributionSelectedIndex(jComboBoxDistribution.getSelectedIndex());
        this.dispose();
    }//GEN-LAST:event_jButtonCloseActionPerformed
    private void doTest()
    {
        String tarea = jTextAreaInputData.getText().replaceAll(" ", "\n");
        
        StringTokenizer tk = new StringTokenizer(tarea,"\n");
        String[] result = tarea.split("\n");
        ArrayList<Double> dataset = new ArrayList<Double>();
        for (int i =0 ; i< result.length; i++)
        {
            if (!result[i].isEmpty())
                dataset.add(Double.parseDouble(result[i]));
        }
        double []data = new double[dataset.size()];
        for (int i = 0; i < data.length; i++)
            data[i]=dataset.get(i);
        
        try {
            gof = new GoodnessOfFit(data);
        } catch (ScriptException ex) {
             ErrorMsg.displayMsg("expression_error", ex.getMessage(), getName(), ex);
        }
        jComboBoxDistribution.setModel(new javax.swing.DefaultComboBoxModel(gof.getEnabledDistributions()));
        jComboBoxDistribution.setEnabled(true);
        

    }
    
    public JPanel doHistogram(double [] observedFrequency, double [] expectedFrequency)
    {
        
        NumberAxis domainAxis = new NumberAxis("");
        domainAxis.setVisible(false);
        domainAxis.setAutoTickUnitSelection(true);
        ValueAxis rangeAxis = new NumberAxis("");
        rangeAxis.setVisible(false);
        XYSeriesCollection data1 = new XYSeriesCollection();
        XYSeries series1 = new XYSeries(mxResources.get("observed"));

        XYSeriesCollection data2 = new XYSeriesCollection();
        XYSeries series2 = new XYSeries(mxResources.get("expected"));
       
        for (int i = 0; i < expectedFrequency.length ; i++)
        {
            series2.add(i, expectedFrequency[i]);
        }
        for (int i = 0; i < observedFrequency.length ; i++)
        {
            series1.add(i, observedFrequency[i]);
        }
                    
        /*
        HistogramDataset data = new HistogramDataset();
        data.addSeries("H1", observedFrequency, 300);
         * 
         */
        data1.addSeries(series1);
        data2.addSeries(series2);
        XYItemRenderer renderer1 = new XYSplineRenderer();
        renderer1.setBaseItemLabelsVisible(false);
        renderer1.setSeriesStroke(0, XYSplineRenderer.DEFAULT_OUTLINE_STROKE);
        renderer1.setSeriesShape(0, new Rectangle(0,0,0,0));
        XYItemRenderer renderer2 = new XYBarRenderer(0.10);
        
        XYItemRenderer [] itrender = new XYItemRenderer[2];
        itrender[0] = renderer1;
        itrender[1] = renderer2;

        XYPlot plot = new XYPlot();
        plot.setDataset(1, data1);
        plot.setDataset(0, data2);
        plot.setRenderers(itrender);
        plot.setDomainAxis(domainAxis);
        plot.setRangeAxis(rangeAxis);
                
        JFreeChart chart2 = new JFreeChart(mxResources.get("goodnessOfFit"), 
                JFreeChart.DEFAULT_TITLE_FONT, plot, true);
        ChartPanel chartPanel = new ChartPanel(chart2);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        

        return chartPanel;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GoodnessOfFitView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GoodnessOfFitView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GoodnessOfFitView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GoodnessOfFitView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GoodnessOfFitView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton jButtonClose;
    private javax.swing.JToggleButton jButtonGo;
    private javax.swing.JComboBox jComboBoxDistribution;
    private javax.swing.JInternalFrame jInternalFrameHistogram;
    private javax.swing.JPanel jPanelChooseDistribution;
    private javax.swing.JPanel jPanelInputData;
    private javax.swing.JPanel jPanelResults;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextAreaInputData;
    private javax.swing.JTextArea jTextAreaResults;
    private javax.swing.JTextField jTextFieldExpression;
    // End of variables declaration//GEN-END:variables
}
