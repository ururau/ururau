/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau;

import com.mxgraph.util.mxResources;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.openscada.opc.lib.common.AlreadyConnectedException;
import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.common.NotConnectedException;
import org.openscada.opc.lib.da.AddFailedException;
import org.openscada.opc.lib.da.DuplicateGroupException;
import org.openscada.opc.lib.da.Server;
import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.common.JISystem;
import org.jinterop.dcom.core.JIVariant;
import org.openscada.opc.dcom.list.ClassDetails;
import org.openscada.opc.lib.da.Group;
import org.openscada.opc.lib.da.Item;
import org.openscada.opc.lib.da.ItemState;
import org.openscada.opc.lib.da.browser.Branch;
import org.openscada.opc.lib.da.browser.Leaf;
import org.openscada.opc.lib.list.Categories;
import org.openscada.opc.lib.list.Category;
import org.openscada.opc.lib.list.ServerList;
import org.ururau.modules.USimulation;

/**
 *
 * @author tulio
 */
public class Communication implements Serializable {
    private static final long serialVersionUID = -6561613071112577140L;
    private static Communication instance = null;
    public static Communication getInstance()
    {
        if (Communication.instance == null)
            Communication.instance = new Communication();
        return Communication.instance;
    }
    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProgId() {
        return progId;
    }

    public void setProgId(String progId) {
        this.progId = progId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isEnabled() {
        /*
        if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0)
        {
            return enabled;
        } else {
            return false;
        }*/
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isRealTime() {
        return realTime;
    }

    public void setRealTime(boolean realTime) {
        this.realTime = realTime;
    }
    
    boolean realTime = true;
    boolean enabled = false;
    String host = "127.0.0.1";
    String user = "ururau";
    String password = "test123";
    String domain = "WORKGROUP";
    String progId = "Matrikon.OPC.Simulation.1";
    ServerList serverList = null;
    Server server = null;
    ArrayList<String> tag = null;
    Group group;
    Map<String, Item> itemMap;

    public void setServerList(ServerList serverList) {
        this.serverList = serverList;
    }

    
    public Communication () {
    }
    
    public String [] getServerList() throws IllegalArgumentException, UnknownHostException, JIException
    {
        String [] progIdList = null;
        if (enabled)
        {
            serverList = new ServerList ( host, user, password, domain );
        
            //serverList.listServers(implemented, required);
            final Collection<ClassDetails> detailsList = serverList.listServersWithDetails(
                        new Category[] { Categories.OPCDAServer20 }, new Category[] {} );
            progIdList = new String[detailsList.size()];
            int i = 0;
            for ( final ClassDetails details : detailsList )
            {
                progIdList[i] = details.getProgId ();
                i++;
            }
        }
        return progIdList;
    }
    
    public void connect() throws JIException, IllegalArgumentException, UnknownHostException, AlreadyConnectedException
    {
        System.out.println("\nTentando conectar");
        if (serverList == null)
            return;
        if (progId == null)
            return;
            
        System.out.println("\nConectando...");
        final ConnectionInformation ci = new ConnectionInformation();
        ci.setHost( getHost() );
        ci.setDomain( getDomain() );
        ci.setUser( getUser() );
        ci.setPassword( getPassword() );
        String cls = serverList.getClsIdFromProgId( getProgId() );
        System.out.println("\nCLSIDs: "+cls);
        ci.setClsid( cls );
        if (server == null)
        {
            System.out.println("CLSIDs: "+cls);
            
            server = new Server( ci, Executors.newSingleThreadScheduledExecutor () );
            server.connect();
        }
    }
    
    public String [] getTags() 
    {
        String [] tags = null;
        System.out.println("try auto-registration");
        JISystem.setAutoRegisteration(true);
        System.out.println("COMVersion: "+JISystem.getCOMVersion().getMajorVersion());
        try {
            if (server == null)
            {
                try {
                    connect();
                } catch (AlreadyConnectedException ex) {
                    Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            tag = new ArrayList<String>();
            if (server == null)
            {
                return tags;
            }
            
            if (server.getTreeBrowser() == null)
            {
                return tags;
            }
            
            
            dumpTree( server.getTreeBrowser().browse(), 0 );
            //addMyItems();
            tags = new String[tag.size()];
            for (int i = 0; i<tags.length; i++)
            {
                tags[i] = tag.get(i);
            }
            if (group == null)
            {
                group = server.addGroup( "ururau" );
            }

            itemMap = group.addItems(tags) ;  
                 
             group = server.addGroup( );
           
            return tags;
        } catch (AddFailedException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("add_group_error","ururau", "add_group", ex);
        } catch (NotConnectedException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("not_connected_error",null, "connection", ex);
        } catch (DuplicateGroupException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("duplicate_group_error","ururau", "connection", ex);
        } catch (JIException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, mxResources.get("topicConfigurationError"),mxResources.get("opcServerError"),JOptionPane.ERROR_MESSAGE);
            ErrorMsg.displayMsg("ji_error",null, "connection", ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("illegal_argument",null, "connection", ex);
        } catch (UnknownHostException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("unknown_host",this.host, "connection", ex);
        }
        return tags;
    }
    
    public double getTagValue(String itemName) 
    {
        double ret = 0.0;
        final ItemState itemState;
        try {
            if (itemMap == null)
            {
                getTags();
            }
            if (itemMap.get(itemName) == null)
            {
                JOptionPane.showMessageDialog(null, mxResources.get("tagNotFound")+": "+itemName, mxResources.get("opcServerError"),JOptionPane.ERROR_MESSAGE);
                USimulation.getInstance().getSimulation().end();
                return 0;
            }
            itemState = itemMap.get(itemName).read( true );
            System.out.println(">>>> tag:"+itemName+" = value: "+VariantDumper.dumpValue( itemState.getValue() ));
            
            return VariantDumper.dumpValue( itemState.getValue() );
        } catch (JIException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("ji_error",null, "connection", ex);
        }
        return ret;
    }
    
    public void setTagValue(String itemName, double value)
    {
        try {
            if (itemMap == null)
            {
                getTags();
            }
            if (itemMap.get(itemName) != null)
            {
                itemMap.get(itemName).write(new JIVariant ( value ));
                System.out.println(">>>> Escreveu double: "+value);
            }
        } catch (JIException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            ErrorMsg.displayMsg("ji_error",null, "connection", ex);
        }
    }

    public void setTagValue(String itemName, int value)
    {
        try {
            if (itemMap == null)
            {
                getTags();
            }
            if (itemMap.get(itemName) != null)
            {
                itemMap.get(itemName).write(new JIVariant ( value ));
                System.out.println(">>>> Escreveu int: "+value);
            }
        } catch (JIException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            ErrorMsg.displayMsg("ji_error",null, "connection", ex);
        }
    }

    public void setTagValue(String itemName, boolean value)
    {
        try {
            if (itemMap == null)
            {
                getTags();
            }
            if (itemMap.get(itemName) != null)
            {
                itemMap.get(itemName).write(new JIVariant ( value ));
                System.out.println(">>>> Escreveu boolean: "+value);
            }
        } catch (JIException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            ErrorMsg.displayMsg("ji_error",null, "connection", ex);
        }
    }

    public void disconnect()
    {
        if (server!=null)
        {
            server.disconnect();
        }
        server = null;
        itemMap = null;
        group = null;
    }
    
    private void dumpTree ( final Branch branch, final int level )
    {
        final StringBuilder sb = new StringBuilder ();
        for ( int i = 0; i < level; i++ )
        {
            //sb.append ( "  " );
        }
        final String indent = sb.toString ();

        for ( final Leaf leaf : branch.getLeaves () )
        {
            //System.out.println ( indent + "Leaf: " + leaf.getName () + " [" + leaf.getItemId () + "]" );
            //if (leaf.getItemId().contains(".U."))
            tag.add(leaf.getItemId());
        }
        for ( final Branch subBranch : branch.getBranches () )
        {
            //System.out.println ( indent + "Branch: " + subBranch.getName () );
            dumpTree ( subBranch, level + 1 );
        }
    }
    
    private void addMyItems()
    {
        tag.add("C.D.Group1.X_0");
        tag.add("C.D.Group1.AVANCA_X");
    }

}
