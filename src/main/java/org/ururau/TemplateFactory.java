/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau;

import org.ururau.modules.UWrite;
import org.ururau.modules.UResource;
import org.ururau.modules.ULocal;
import org.ururau.modules.UJump;
import org.ururau.modules.UHold;
import org.ururau.modules.UXor;
import org.ururau.modules.UFunction;
import org.ururau.modules.UBatch;
import org.ururau.modules.UTerminate;
import org.ururau.modules.USeparate;
import org.ururau.modules.UAnd;
import org.ururau.modules.UCreate;
import org.ururau.modules.UAssign;
import org.ururau.modules.UEmissions;
import org.ururau.modules.UInventorFunction;
import org.ururau.modules.UMyWrite;
import org.ururau.modules.URecord;
import org.ururau.modules.URunMacro;
import org.ururau.modules.USheetReadWrite;
import org.ururau.modules.UVariable;

/**
 *
 * @author tulio
 */
public class TemplateFactory {

    public static Object getInstance(Template.TemplateType type)
    {
        return getInstance(type, false, true);
    }


    public static Object getInstance(Template.TemplateType type, boolean enableDefault, boolean enableCounter)
    {
        if (type == Template.TemplateType.Create)
        {
            if (enableDefault)
            {
                return new UCreate();
            } else {
                return new UCreate(enableCounter);
            }
        }
        else if(type == Template.TemplateType.Function)
        {
            if (enableDefault)
            {
                return new UFunction();
            } else {
                return new UFunction(enableCounter);
            }
        }
        else if(type == Template.TemplateType.Variable)
        {
            if (enableDefault)
            {
                return new UVariable();
            } else {
                return new UVariable(enableCounter);
            }
        }
        else if(type == Template.TemplateType.Terminate)
        {
            if (enableDefault)
            {
                return new UTerminate();
            } else {
                return new UTerminate(enableCounter);
            }
        }
        else if(type == Template.TemplateType.Resource)
        {
            if (enableDefault)
            {
                return new UResource();
            } else {
                return new UResource(enableCounter);
            }
        }
        else if(type == Template.TemplateType.Local)
        {
            if (enableDefault)
            {
                return new ULocal();
            } else {
                return new ULocal(enableCounter);
            }
        }
        else if(type == Template.TemplateType.And)
        {
            if (enableDefault)
            {
                return new UAnd();
            } else {
                return new UAnd(enableCounter);
            }
        }
        else if(type == Template.TemplateType.Xor)
        {
            if (enableDefault)
            {
                return new UXor();
            } else {
                return new UXor(enableCounter);
            }
        }
        else if(type == Template.TemplateType.Jump)
        {
            if (enableDefault)
            {
                return new UJump();
            } else {
                return new UJump(enableCounter);
            }
        }

        else if(type == Template.TemplateType.Assign)
        {
            if (enableDefault)
            {
                return new UAssign();
            } else {
                return new UAssign(enableCounter);
            }
        }
        else if(type == Template.TemplateType.Record)
        {
            if (enableDefault)
            {
                return new URecord();
            } else {
                return new URecord(enableCounter);
            }
        }
        else if(type == Template.TemplateType.Batch)
        {
            if (enableDefault)
            {
                return new UBatch();
            } else {
                return new UBatch(enableCounter);
            }
        }
        else if(type == Template.TemplateType.Separate)
        {
            if (enableDefault)
            {
                return new USeparate();
            } else {
                return new USeparate(enableCounter);
            }
        }
        else if(type == Template.TemplateType.Hold)
        {
            if (enableDefault)
            {
                return new UHold();
            } else {
                return new UHold(enableCounter);
            }
        }
        else if(type == Template.TemplateType.Write)
        {
            if (enableDefault)
            {
                return new UWrite();
            } else {
                return new UWrite(enableCounter);
            }
        }       
        else if(type == Template.TemplateType.SheetReadWrite)
        {
            if (enableDefault)
            {
                return new USheetReadWrite();
            } else {
                return new USheetReadWrite(enableCounter);
            }
        }
        else if(type == Template.TemplateType.RunMacro)
        {
            if (enableDefault)
            {
                return new URunMacro();
            } else {
                return new URunMacro(enableCounter);
            }
        }       
        else if(type == Template.TemplateType.MyWrite)
        {
            if (enableDefault)
            {
                return new UMyWrite();
            } else {
                return new UMyWrite(enableCounter);
            } 
        } 
        else if(type == Template.TemplateType.MyEmissions)
        {
            if (enableDefault)
            {
                return new UEmissions();
            } else {
                return new UEmissions(enableCounter);
            }
        } 
        else if(type == Template.TemplateType.InventorFunction)
        {
            if (enableDefault)
            {
                return new UInventorFunction();
            } else {
                return new UInventorFunction(enableCounter);
            }
        }
        
        System.err.println("Template do not exist!");
        return new Object();
    }
}
