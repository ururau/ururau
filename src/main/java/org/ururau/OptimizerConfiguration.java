/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau;


import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.jgap.FitnessFunction;
import org.jgap.Gene;
import org.jgap.Genotype;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.impl.DefaultConfiguration;
import org.jgap.impl.IntegerGene;
import org.ururau.modules.USimulation;

/**
 *
 * @author tulio
 */
public class OptimizerConfiguration {
    private Configuration conf;
    private UFitnessFunction myFunc;
    static int MAX_ALLOWED_EVOLUTIONS = 20;
    public void configure() throws InvalidConfigurationException
    {

          if (Optimizer.getInstance() ==null)
          {
              return ;
          }


          DefaultConfiguration.reset();
            conf = new DefaultConfiguration();
          myFunc =
            new UFitnessFunction();
          myFunc.setOptimizerModel(Optimizer.getInstance());
          myFunc.setSimulation(USimulation.getInstance());
          
          conf.setFitnessFunction( myFunc );

  // Set the fitness function we want to use, which is our
  // MinimizingMakeChangeFitnessFunction that we created earlier.
  // We construct it with the target amount of change provided
  // by the user.
  // ------------------------------------------------------------

  

  // Now we need to tell the Configuration object how we want our
  // Chromosomes to be setup. We do that by actually creating a
  // sample Chromosome and then setting it on the Configuration
  // object. As mentioned earlier, we want our Chromosomes to
  // each have four genes, one for each of the coin types. We
  // want the values of those genes to be integers, which represent
  // how many coins of that type we have. We therefore use the
  // IntegerGene class to represent each of the genes. That class
  // also lets us specify a lower and upper bound, which we set
  // to sensible values for each coin type.
  // --------------------------------------------------------------
  int n = Optimizer.getInstance().getGaVariables().size();
  Gene[] sampleGenes = new Gene[ n ];
  ArrayList<GAVariable> gaVariables = Optimizer.getInstance().getGaVariables();
  if (gaVariables == null)
  {
      return;
  }
  
  for (int i=0 ; i < n ; i++)
  {
      
      sampleGenes[i] = new IntegerGene(conf, 
              gaVariables.get(i).getMinValue(), 
              gaVariables.get(i).getMaxValue());
  }  
  Chromosome sampleChromosome = new Chromosome(conf, sampleGenes );

  conf.setSampleChromosome( sampleChromosome );

  // Finally, we need to tell the Configuration object how many
  // Chromosomes we want in our population. The more Chromosomes,
  // the larger the number of potential solutions (which is good
  // for finding the answer), but the longer it will take to evolve
  // the population each round. We'll set the population size to
  // 500 here.
  // --------------------------------------------------------------
  conf.setPopulationSize( 30 );
    }
    
    public void run()
    {
        Genotype population = null;
        try {
            population = Genotype.randomInitialGenotype( conf );
        } catch (InvalidConfigurationException ex) {
            Logger.getLogger(OptimizerConfiguration.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        for( int i = 0; i < MAX_ALLOWED_EVOLUTIONS; i++ )
        {
            population.evolve();
        }
        try {
            conf.verifyStateIsValid();
        } catch (InvalidConfigurationException ex) {
            Logger.getLogger(OptimizerConfiguration.class.getName()).log(Level.SEVERE, null, ex);
        }
        conf = null;
        IChromosome bestSolutionSoFar = population.getFittestChromosome();
        myFunc.evaluate(bestSolutionSoFar);
                
    }
    
}
