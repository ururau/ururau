/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau;

import com.mxgraph.util.mxResources;
import org.ururau.modules.UResource;
import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;

/**
 *
 * @author Arthur
 */
public class ViewValidation {
    
    private String title = "Dados Incompatíveis";
    
    public boolean isEmpty(javax.swing.JTextField value, String field)
    {
        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
        if (value.getText().trim().equals(""))
        {
            value.setBorder(new LineBorder(Color.RED));
            value.requestFocus();
            JOptionPane.showMessageDialog(null, "<html>Entre com um valor para o campo <b>"
                    +field+"</b>.</html>", title, JOptionPane.ERROR_MESSAGE);
            return true;   
        }
        else
            return false;
    }
    
    
    // --------- \/ VALIDACAO DE NUMEROS ABAIXO \/ ---------
    public boolean isZero(javax.swing.JTextField value, String field)
    {
        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
        if(!isEmpty(value, field))
        {
            try
            {
                double valor = Double.parseDouble(value.getText());
                if(valor!=0)
                    return false;
                else
                    throw new Exception("zero");
            }
            catch(Exception e)
            {
                value.setBorder(new LineBorder(Color.RED));
                value.requestFocus();
                JOptionPane.showMessageDialog(null, "<html>O campo <b>"+field+
                        "</b> não aceita <b>valor nulo</b>.<br>Exemplo: <b>"
                        +(int)(Math.random()*100)+"</b></html>\n\nError: "
                        +e.getMessage(), title, JOptionPane.ERROR_MESSAGE);
                return true;
            }
        }
        else
            return false;
    }
    
    public boolean isPositive(javax.swing.JTextField value, String field)
    {
        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
        if(!isEmpty(value, field))
        {
            try
            {
                double valor = Double.parseDouble(value.getText());
                if(valor>=0)
                    return true;
                else
                    throw new Exception("número negativo");
            }
            catch(Exception e)
            {
                value.setBorder(new LineBorder(Color.RED));
                value.requestFocus();
                JOptionPane.showMessageDialog(null, "<html>O campo <b>"+field+
                        "</b> aceita apenas <b>números positivos</b>.<br>Exemplo: <b>"
                        +(int)(Math.random()*100)+"</b></html>\n\nError: "
                        +e.getMessage(), title, JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        else
            return false;
    }
    
    public boolean isInt(javax.swing.JTextField value, String field)
    {
        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
        if(isPositive(value, field))
        {
            try
            {
                Integer.parseInt(value.getText());
                return true;
            }
            catch(Exception e)
            {
                value.setBorder(new LineBorder(Color.RED));
                value.requestFocus();
                JOptionPane.showMessageDialog(null, "<html>O campo <b>"+field+
                        "</b> aceita apenas <b>números inteiros</b>.<br>Exemplo: <b>"
                        +(int)(Math.random()*100)+"</b></html>\n\nError: "
                        +e.getMessage(), title, JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        else
            return false;
    }
    
    public boolean isDouble(javax.swing.JTextField value, String field)
    {
        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
        if(isPositive(value, field))
        {
            try
            {
                Double.parseDouble(value.getText());
                return true;
            }
            catch(Exception e)
            {
                value.setBorder(new LineBorder(Color.RED));
                value.requestFocus();
                JOptionPane.showMessageDialog(null, "<html>O campo <b>"+field+
                        "</b> aceita apenas <b>números reais</b>.<br>Exemplo: <b>"
                        +(int)(Math.random()*99)+"."+(int)(Math.random()*99)+
                        "</b></html>\n\nError: "+e.getMessage()+
                        ".", title, JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        else
            return false;
    }
    // --------- /\ VALIDACAO DE NUMEROS ACIMA /\ ---------
    
    
    // --------- \/ VALIDACAO PERSONALIZADA ABAIXO \/ ---------
    public boolean infinity(javax.swing.JTextField value, String field)
    {
        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
        if(!isEmpty(value, field))
        {
            try
            {
                double valor = Double.parseDouble(value.getText());
                if(valor==0)
                    throw new Exception("zero");
                else if(valor>0)
                    return true;
                else
                    throw new Exception("número negativo");
            }
            catch(Exception e)
            {
                if(value.getText().trim().equalsIgnoreCase("infinito"))
                    return true;
                else
                {
                    value.setBorder(new LineBorder(Color.RED));
                    value.requestFocus();
                    JOptionPane.showMessageDialog(null, "<html>O campo <b>"+field+
                            "</b> aceita apenas <b>números positivos</b> e não "
                            +"<b>nulo</b> ou a palavra <b>infinito</b>.<br>Exemplo: <b>"
                            +(int)(Math.random()*100)+
                            "</b><br>Exemplo: <b>Infinito</b></html>\n\nError: "
                            +e.getMessage(), title, JOptionPane.ERROR_MESSAGE);
                    return false;
                }
            }
        }
        else
            return false;
    }
    
    public boolean requested(javax.swing.JTextField id, UResource amount, javax.swing.JTextField value, String field)
    {
        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
        if(!id.getText().trim().equals(""))
        {
            if(isInt(value, field) && !isZero(value, field) && resource(amount, value, field))
                return true;
            else
                return false;
        }
        else
            return true;
    }
    
    public boolean resource(UResource amount, javax.swing.JTextField value, String field)
    {
        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
        if(amount.getCapacity()>=Integer.parseInt(value.getText()))
            return true;
        else
        {
            value.setBorder(new LineBorder(Color.RED));
            value.requestFocus();
            JOptionPane.showMessageDialog(null, "<html>A <b>"+field+
                    "</b> deve ser inferior ou igual a <b>Capacidade do Recurso</b></html>",
                    title, JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
    
//    public boolean delayTimeOld(javax.swing.JComboBox combo, javax.swing.JTextField value, String field)
//    {
//        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
//        if(combo.getSelectedIndex()==2)
//        {
//            try
//            {
//                Expression expr = new Expression(value.getText());
//                if(expr.isValid())
//                    return true;
//                else
//                    throw new Exception(value.getText());
//            }
//            catch(Exception e)
//            {
//                
//                value.setBorder(new LineBorder(Color.RED));
//                value.requestFocus();
//                JOptionPane.showMessageDialog(null, "<html>A <b>expressão</b> entrada no campo <b>"+field+
//                        "</b> não é <b>válida</b>.<br>Exemplo: <b>NORM("+(int)(Math.random()*10)+
//                        ","+(int)(Math.random()*10)+")</b><br>Exemplo: <b>EXPO("+(int)(Math.random()*10)+
//                        "."+(int)(Math.random()*10)+")</b><br>Exemplo: <b>TRIA("+(int)(Math.random()*3)+
//                        ","+((int)(Math.random()*3)+3)+","+((int)(Math.random()*3)+6)+
//                        ")</b></html>\n\nError: "+e.getMessage(), title, JOptionPane.ERROR_MESSAGE);
//                return false;   
//            }
//        }
//        else
//        {
//            if(apenasReal(value, field) && naoZero(value, field))
//                return true;
//            else
//                return false;
//        }
//    }
    
    public boolean simpleDouble(String valor)
    {
        try
        {
            Double.parseDouble(valor);
            return true;
        }
        catch(NumberFormatException e)
        {
            System.out.println(e.toString());
            return false;
        }
    }
    
    public boolean expression(javax.swing.JTextField value, String field)
    {
        try
        { 
            String expI = "";
            expI = value.getText().trim();
            String[] expL = expI.split("\\(",2);
                        
            if(expL[0].equalsIgnoreCase("norm"))
            {
                String aux[] = expL[1].split("\\)",2);
                
                if(aux[1].length()==0)
                {
                    //TESTAR "NORM" E ARRUMAR!!!!!!!! ERRO NO numeros[1] \/
                    String[] numeros = aux[0].split(",",2);
                    
                    if(simpleDouble(numeros[0]) && simpleDouble(numeros[1]))
                    {
                        if(Double.parseDouble(numeros[0]) > Double.parseDouble(numeros[1]))
                        {
                            return true;
                        }
                        else
                            throw new Exception(mxResources.get("firstNumberGTSecond"));
                    }
                    else
                        throw new Exception(mxResources.get("notANumber"));
                }
                else
                    throw new Exception(mxResources.get("invalidExpression"));
            }
            
            if(expL[0].equalsIgnoreCase("unif"))
            {
                String aux[] = expL[1].split("\\)",2);
                
                if(aux[1].length()==0)
                {
                    //TESTAR "NORM" E ARRUMAR!!!!!!!! ERRO NO numeros[1] \/
                    String[] numeros = aux[0].split(",",2);
                    
                    if(simpleDouble(numeros[0]) && simpleDouble(numeros[1]))
                    {
                        if(Double.parseDouble(numeros[0]) < Double.parseDouble(numeros[1]))
                        {
                            return true;
                        }
                        else
                            throw new Exception(mxResources.get("firstNumberGTSecond"));
                    }
                    else
                        throw new Exception(mxResources.get("notANumber"));
                }
                else
                    throw new Exception(mxResources.get("invalidExpression"));
            }

            else if(expL[0].equalsIgnoreCase("expo"))
            {
                String aux[] = expL[1].split("\\)",2);
                if(aux[1].length()==0)
                {
                    System.out.println("EXPO = "+aux[0]);
                    if(simpleDouble(aux[0]))
                        return true;
                    else
                        throw new Exception(mxResources.get("notANumber"));
                }
                else
                    throw new Exception(mxResources.get("invalidExpression"));
            }

            else if(expL[0].equalsIgnoreCase("tria"))
            {
                String aux[] = expL[1].split("\\)",2);
                
                if(aux[1].length()==0)
                {
                    //PERGUNTAR SE OS 3 NUMEROS PODEM SER IGUAIS
                    //PERGUNTAR SE OS 3 NUMEROS PODEM SER IGUAIS
                    //PERGUNTAR SE OS 3 NUMEROS PODEM SER IGUAIS
                    String[] numeros = aux[0].split(",",3);
                    if(simpleDouble(numeros[0]) && simpleDouble(numeros[1]) && simpleDouble(numeros[2]))
                    {
                        if(Double.parseDouble(numeros[0]) < Double.parseDouble(numeros[1]))
                        {
                            if(Double.parseDouble(numeros[1]) < Double.parseDouble(numeros[2]))
                                return true;
                            else
                                throw new Exception("o terceiro número deve ser maior que o segundo");
                        }
                        else
                            throw new Exception("o segundo número deve ser maior que o primeiro");
                    }
                    else
                        throw new Exception(mxResources.get("notANumber"));
                }
                else
                    throw new Exception(mxResources.get("invalidExpression"));
            }
            
            if(simpleDouble(value.getText().trim()))
            {
                return true;
            }

            else
                throw new Exception("expressão inválida");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "<html>A <b>expressão</b> entrada no campo <b>"+field+
                    "</b> não é <b>válida</b>.<br>Exemplo: <b>NORM("+((int)(Math.random()*5)+5)+
                    ","+(int)(Math.random()*5)+")</b><br>Exemplo: <b>UNIF("+(int)(Math.random()*5)+
                    ","+((int)(Math.random()*5)+5)+")</b><br>Exemplo: <b>EXPO("+(int)(Math.random()*10)+
                    "."+(int)(Math.random()*10)+")</b><br>Exemplo: <b>TRIA("+(int)(Math.random()*3)+
                    ","+((int)(Math.random()*3)+3)+","+((int)(Math.random()*3)+6)+
                    ")</b></html>\n\nError: "+e.toString(), title, JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
    
    public boolean delayTime(javax.swing.JComboBox combo, javax.swing.JTextField value, String field)
    {
        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
        if(combo.getSelectedIndex()==2) // ARRUMAR A VALIDACAO DE EXPRESSAO
        {
            try
            {
                //ARRUMAR A FUNCAO EXPRESSAO
                /*if(!isEmpty(value, field))
                {
                    if(expression(value, field))
                        return true;
                    else
                        throw new Exception(value.getText());
                }
                else*/
                    return true;
            }
            catch(Exception e)
            {
                
                value.setBorder(new LineBorder(Color.RED));
                value.requestFocus();
//                JOptionPane.showMessageDialog(null, "Error: "+e.getMessage(), titulo, JOptionPane.ERROR_MESSAGE);
                return false;   
            }
        }
        else
        {
            if(isDouble(value, field) && !isZero(value, field))
                return true;
            else
                return false;
        }
    }
    
    public boolean isGoodnessFitEmpty(javax.swing.JTextArea value, String field)
    {
        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
        if (value.getText().trim().equals(""))
        {
            value.setBorder(new LineBorder(Color.RED));
            value.requestFocus();
            JOptionPane.showMessageDialog(null, "<html>Entre com um valor para o campo <b>"
                    +field+"</b>.</html>", title, JOptionPane.ERROR_MESSAGE);
            return true;   
        }
        else
            return false;
    }
    
    public boolean isGoodnessFitDouble(javax.swing.JTextArea value, String field)
    {
        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
        String errorNumber = "";
        
        try
        {
            String aux = value.getText().trim();
            aux = aux.replace("\n", " ");
            
            String vet[];
            vet = aux.split(" ");
            
            for (int i=0; i<vet.length; i++)
            {
                if(vet[i].trim() != null)
                    aux = vet[i]+" ";
            }
            
            vet = null;
            vet = aux.split(" ");
            for (int i=0; i<vet.length; i++)
            {
                errorNumber=vet[i];
                Double.parseDouble(vet[i].trim());
            }
            
            return true;
        }
        catch(Exception e)
        {
            value.setBorder(new LineBorder(Color.RED));
            value.requestFocus();
            JOptionPane.showMessageDialog(null, "<html>O campo <b>"+field+
                    "</b> aceita apenas <b>números reais</b>.<br></html>\n\nNumber: "+errorNumber+
                    "\nError: "+e.getMessage()+".", title, JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
    
    public boolean priority(javax.swing.JTextField value, String field)
    {
        return isInt(value, field);
    }
    
    public boolean goodnessFit(javax.swing.JTextArea value, String field)
    {
        value.setBorder(new LineBorder(Color.LIGHT_GRAY));
        //clean.setText("");
        if (!isGoodnessFitEmpty(value, field))
        {
            if(isGoodnessFitDouble(value, field))
            {
                String aux = value.getText().trim();
                aux = aux.replace(" ", ",");
                aux = aux.replace("\n", ",");
                if(aux.split(",").length<15)
                {
                    value.setBorder(new LineBorder(Color.RED));
                    value.requestFocus();
                    JOptionPane.showMessageDialog(null, "<html>O campo <b>"
                            +field+"</b> deve conter 15 números ou mais.</html>"
                            , title, JOptionPane.ERROR_MESSAGE);
                    return false;
                }
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }
    // --------- /\ VALIDACAO PERSONALIZADA ACIMA /\ ---------
    
}
