/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau;

import java.io.Serializable;

/**
 *
 * @author Fraga
 */
public class ReadWriteVariable implements Serializable {
    String variableName;
    String cellName;
    boolean writeToFile;

    public boolean isWriteToFile() {
        return writeToFile;
    }

    public void setWriteToFile(boolean writeToFile) {
        this.writeToFile = writeToFile;
    }
 
    public String getCellName() {
        return cellName;
    }

    public void setCellName(String cellName) {
        this.cellName = cellName;
    }

    public String getVariableName() {
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }
    
}
