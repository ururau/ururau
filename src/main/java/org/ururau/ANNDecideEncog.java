package org.ururau;

import com.mxgraph.util.mxResources;
import java.awt.Component;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
//import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
//import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.neural.networks.training.propagation.back.Backpropagation;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.util.csv.CSVFormat;
import org.encog.util.simple.EncogUtility;
import org.encog.util.simple.TrainingSetUtil;

/**
 *
 * @author Marília
 */
public class ANNDecideEncog {
	        
        public double consulta(BasicNetwork net, double no1,double no2, double no3, double no4, double no5, int cmd1) {
           
            
            MLData input1 = new BasicMLData(cmd1);
            
            
            switch(cmd1)
            {
                case 2:
                    input1.setData(0,no1);
                    input1.setData(1,no2);
                    break;

                case 3:
                    input1.setData(0,no1);
                    input1.setData(1,no2);
                    input1.setData(2,no3);
                    break;

                case 4:
                    input1.setData(0,no1);
                    input1.setData(1,no2);
                    input1.setData(2,no3);
                    input1.setData(3,no4);
                    break;
                case 5:
                    input1.setData(0,no1);
                    input1.setData(1,no2);
                    input1.setData(2,no3);
                    input1.setData(3,no4);
                    input1.setData(4,no5);
                    break;

                default:
                    javax.swing.JDialog f=new javax.swing.JDialog();  
                    f.setSize(250,150); 
                    javax.swing.JOptionPane.showMessageDialog(f,mxResources.get("warning"),mxResources.get("annWarningLimit"),javax.swing.JOptionPane.WARNING_MESSAGE);
            }
            
            MLData output1 = net.compute(input1);
            System.out.println("--------------Valores dinâmicos dos neurônios------------");
            System.out.println("1º Neurônio:"+no1);
            System.out.println("2º Neurônio:"+no2);
            switch(cmd1)
            {
                case 3:
                    System.out.println("3º Neurônio:"+no3);
                    break;
                case 4:
                    System.out.println("3º Neurônio:"+no3);
                    System.out.println("4º Neurônio:"+no4);
                    break;
                case 5:
                    System.out.println("3º Neurônio:"+no3);
                    System.out.println("4º Neurônio:"+no4);
                    System.out.println("5º Neurônio:"+no5);
                    break;
            }           
            System.out.println("Saída: "+output1.getData(0));
             if (output1.getData(0)>0.5) {
                //System.out.println("TRUE");
                return 1.0;                
            } else {
                //System.out.println("FALSE");
                return 0.0;
            }  
           // return output1.getData(0);
        }
        
        public  BasicNetwork Treinamento( String path, int cmd1, int cmd2, int cmd3) {
            System.out.println(path);
            // create a neural network, without using a factory
		BasicNetwork network = new BasicNetwork();
                network.addLayer(new BasicLayer(null,true,cmd1));//nº de neurônios na camada de entrada
		network.addLayer(new BasicLayer(new ActivationSigmoid(),true,cmd2));//nº de neurônios na camada oculta 1               
		if (cmd3!=0){
                    network.addLayer(new BasicLayer(new ActivationSigmoid(),true,cmd3));//nº de neurônios na camada oculta 2
                }
                network.addLayer(new BasicLayer(new ActivationSigmoid(),false,1));//nº de neurônios na camada de saída. FIXO!!!!
		network.getStructure().finalizeStructure();
		network.reset();             

          MLDataSet trainingSet = TrainingSetUtil.loadCSVTOMemory(CSVFormat.ENGLISH, path, false, cmd1, 1);

            // train the neural network
            final ResilientPropagation train = new ResilientPropagation(network, trainingSet);

            int epoch = 1;
            do {
                    train.iteration();
                    System.out.println("Epoch #" + epoch + " Error:" + train.getError());
                    epoch++;
             } while((train.getError() > 0.013) ^ !(epoch < 100000));            
            train.finishTraining(); 
            return network;
        }
        
        public static void main(final String args[]) {
		/*
		// create a neural network, without using a factory
		BasicNetwork network = new BasicNetwork();
		network.addLayer(new BasicLayer(null,true,2));
		network.addLayer(new BasicLayer(new ActivationSigmoid(),true,3));
		network.addLayer(new BasicLayer(new ActivationSigmoid(),false,1));
		network.getStructure().finalizeStructure();
		network.reset();

		// create training data
		MLDataSet trainingSet = new BasicMLDataSet(XOR_INPUT, XOR_IDEAL);
		
		// train the neural network
		final ResilientPropagation train = new ResilientPropagation(network, trainingSet);
                
		int epoch = 1;
		do {
			train.iteration();
			System.out.println("Epoch #" + epoch + " Error:" + train.getError());
			epoch++;
		} while(train.getError() > 0.01);
		train.finishTraining();
                double result = consulta(network,1.0,0.0);                                
		System.out.println(result);			
		Encog.getInstance().shutdown();*/
	}        
}