/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;


import java.io.Serializable;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Resource;
import jsl.modeling.elements.variable.RandomVariable;
import jsl.modeling.elements.queue.UQueue;
import jsl.modeling.elements.variable.Variable;
import jsl.utilities.random.distributions.Constant;
import jsl.utilities.random.distributions.DistributionIfc;
import jsl.utilities.random.distributions.Exponential;
import org.ururau.commands.Nop;
import org.ururau.commands.Process;
import com.mxgraph.util.mxResources;
import org.ururau.ErrorMsg;
import org.ururau.InventorFunctionType;
import org.ururau.Template;
import org.ururau.VariableType;
import org.ururau.commands.Delay;
import org.ururau.commands.DelayExpression;
import org.ururau.commands.DelayExpressionInventor;
import org.ururau.commands.DelayStepInventor;
import org.ururau.commands.DelayVariable;
import org.ururau.commands.DelayVariableInventor;
import org.ururau.commands.ProcessInventor;

/**
 *
 * @author tulio
 */
public class UInventorFunction extends Template implements Serializable {

    private static final long serialVersionUID = 7526471110222716147L;
    private static int count = 0;
    protected String delayTimeValue = "1.0";
    protected VariableType delayTypeE = VariableType.Step;
    protected int delayType = VariableType.Step.ordinal();
    private int amountRequested = 1;
    private int functionType = InventorFunctionType.Delay.ordinal();
    private InventorFunctionType functionTypeE = InventorFunctionType.Delay;
    private UResource r;
    private String queueName;

    public int getMinStep() {
        return minStep;
    }

    public void setMinStep(int minStep) {
        this.minStep = minStep;
    }

    public int getMaxStep() {
        return maxStep;
    }

    public void setMaxStep(int maxStep) {
        this.maxStep = maxStep;
    }
    private int minStep =1;
    private int maxStep =2;

    public UInventorFunction(boolean enableCounter)
    {
        setIdsim("InF"+count);
        setName(mxResources.get("inventorfunction")+count);
        setType(TemplateType.Function);
        if (enableCounter)
        {
            count++;
        }
        queueName = getIdsim()+"."+"queue";
    }

    public UInventorFunction()
    {
        setType(TemplateType.Function);
    }

    public void setDelayTimeValue(String delayTimeValue) {
        this.delayTimeValue = delayTimeValue;
    }

    public int getDelayType() {
        return delayType;
    }

    public void setDelayType(int delayType) {
        VariableType vt[] = VariableType.values();
        this.delayTypeE = vt[delayType];
        this.delayType = delayType;
    }
    
    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }
    
    public String getDelayTimeValue() {
        return delayTimeValue;
    }

    public long getAmountRequested() {
        return amountRequested;
    }

    public void setAmountRequested(int amountRequested) {
        if (amountRequested > 0)
            this.amountRequested = amountRequested;
    }

    public void setTResource(UResource r) {
        this.r = r;
    }

    public UResource getTResource()
    {
        return this.r;
    }

    public int getFunctionType() {
        return functionType;
    }

    public void setFunctionType(int functionType) {
        InventorFunctionType f[] = InventorFunctionType.values();
        this.functionTypeE = f[functionType];
        this.functionType = functionType;
    }

    public String toString() {
        if (this.r != null)
            return getLineCodeNumber()+" "+getIdsim()+" delayType:"+delayTypeE.name()
                +", delayTime: "+delayTimeValue
                +", R->"+r.getIdsim();
        else
            return getLineCodeNumber()+" "+getIdsim()+" delayType:"+delayTypeE.name()
                +", delayTime: "+delayTimeValue;
    }

    public ProcessCommand getProcess(ModelElement m) throws Exception
    {
        if (this.r != null)
        {
            //System.out.println(" "+r.getResource(m).getName());
            return getProcess(m,this.r.getResource(m));
        }
        throw new Exception("Invalid Function Process (no Resource)");
    }

    public ProcessCommand getProcess(ModelElement m, Resource r) throws Exception
    {
        if (delayTypeE == VariableType.Constant)
        {
            DistributionIfc delay = new Constant(Double.parseDouble(delayTimeValue));
            RandomVariable rv = new RandomVariable(m, delay, getIdsim()+"."+getName()+"."+"delayTime");
            UQueue q;
            if (m.getModel().getModelElement(getQueueName()) == null)
                q = new UQueue(m, getQueueName());
            else
                q = (UQueue) m.getModel().getModelElement(getQueueName());
            Variable amountReq = new Variable(m, (double)this.amountRequested, getIdsim()+"."+getName()+"."+"amountReq");
            return new ProcessInventor(m, amountReq, r, q, rv, getIdsim()+"."+getName());
        }
        else if(delayTypeE == VariableType.Exponential)
        {
            DistributionIfc delay = new Exponential(Double.parseDouble(delayTimeValue));
            RandomVariable rv = new RandomVariable(m, delay, getIdsim()+"."+getName()+".delayTime");
                    UQueue q;
            if (m.getModel().getModelElement(getQueueName()) == null)
                q = new UQueue(m, getQueueName());
            else
                q = (UQueue) m.getModel().getModelElement(getQueueName());
            Variable amountReq = new Variable(m, (double)this.amountRequested, getIdsim()+"."+getName()+".amountReq");
            return new ProcessInventor(m, amountReq, r, q, rv,getIdsim()+"."+getName());
            //return new Process(m, amountReq, r, q, rv);
        }

        else if (delayTypeE == VariableType.Expression)
        {
            
            // DExpression exp = new DExpression(delayTimeValue);
            // if (!exp.isValid())
            ///    throw new Exception("Invalid Delay value");
            // DistributionIfc delay = exp.eval();
            // RandomVariable rv = new RandomVariable(m, delay, getIdsim()+"."+getName()+"."+"delayTime");
             
            UQueue q;
            if (m.getModel().getModelElement(getQueueName()) == null)
                q = new UQueue(m, getQueueName());
            else
                q = (UQueue) m.getModel().getModelElement(getQueueName());
            Variable amountReq = new Variable(m, (double)this.amountRequested, getIdsim()+"."+getName()+"."+"amountReq");
            return new ProcessInventor(m, amountReq, r, q, delayTimeValue,getIdsim()+"."+getName());

        }
        else if (delayTypeE == VariableType.Step)
        {
            
            // DExpression exp = new DExpression(delayTimeValue);
            // if (!exp.isValid())
            ///    throw new Exception("Invalid Delay value");
            // DistributionIfc delay = exp.eval();
            // RandomVariable rv = new RandomVariable(m, delay, getIdsim()+"."+getName()+"."+"delayTime");
             
            UQueue q;
            if (m.getModel().getModelElement(getQueueName()) == null)
                q = new UQueue(m, getQueueName());
            else
                q = (UQueue) m.getModel().getModelElement(getQueueName());
            Variable amountReq = new Variable(m, (double)this.amountRequested, getIdsim()+"."+getName()+"."+"amountReq");
            return new ProcessInventor(m, amountReq, r, q, minStep, maxStep,getIdsim()+"."+getName());

        }
        throw new Exception("Invalid Function Process");

    }

    public ProcessCommand getDelay(ModelElement m) throws Exception
    {
        if (delayTypeE == VariableType.Constant)
        {
            DistributionIfc delay = new Constant(Double.parseDouble(delayTimeValue));
            RandomVariable rv = new RandomVariable(m, delay, getIdsim()+"."+getName()+"."+"delayTime");
            return new DelayVariableInventor(m, rv, getIdsim()+"."+getName()+".delay");
        }
        else if(delayTypeE == VariableType.Exponential)
        {
            DistributionIfc delay = new Exponential(Double.parseDouble(delayTimeValue));
            RandomVariable rv = new RandomVariable(m, delay, getIdsim()+" "+getName()+"."+"delayTime");
            return new DelayVariableInventor(m, rv, getIdsim()+"."+getName()+".delay");
        }
        else if (delayTypeE == VariableType.Expression)
        {
            //DistributionIfc delay = new DExpression(delayTimeValue).eval();
            //RandomVariable rv = new RandomVariable(m, delay, getIdsim()+" "+getName()+"."+"delayTime");
            return new DelayExpressionInventor(m, delayTimeValue, getIdsim()+"."+getName()+".delay");
        }
        else if (delayTypeE == VariableType.Step)
        {
            //DistributionIfc delay = new DExpression(delayTimeValue).eval();
            //RandomVariable rv = new RandomVariable(m, delay, getIdsim()+" "+getName()+"."+"delayTime");
            return new DelayStepInventor(m, minStep, maxStep, getIdsim()+"."+getName()+".delay");
        }
        throw new Exception("Invalid Function Delay");
    }

    public ProcessCommand getProcessCommand(ModelElement m)
    {
        try {
            if (functionTypeE == InventorFunctionType.Process)
            {
                setCommandInserted(true);
                return getProcess(m);
            } else if (functionTypeE == InventorFunctionType.Delay) {
                setCommandInserted(true);
                return getDelay(m);
            }
        } catch (Exception ex) {
            Logger.getLogger(UInventorFunction.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("process_command_error", getIdsim()+"."+getName(), "function", ex);
        }
        return new Nop(m,getIdsim()+"."+getName()+".nop");
    }

}
