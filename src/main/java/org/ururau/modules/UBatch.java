/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.modules;

import com.mxgraph.util.mxResources;
import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.queue.Queue;
import org.ururau.Template;
import org.ururau.commands.Batch;
import org.ururau.commands.Batch.BatchType;

/**
 *
 * @author tulio
 */
public class UBatch extends Template implements Serializable  {
    private static final long serialVersionUID = 7526471110122711147L;
    private static int count = 0;
    
    private int batchSize = 10;
    private BatchType batchTypeE = BatchType.PERMANENT_BATCH;
    private int batchType;
    
    public int getBatchType() {
        return batchTypeE.ordinal();
    }

    public void setBatchType(int batchType) {
        BatchType bt[] = BatchType.values();
        this.batchTypeE = bt[batchType];
    }

    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }
    
    public UBatch(boolean enableCounter)
    {
        setIdsim("L"+count);
        setName(mxResources.get("batch")+count);
        setType(TemplateType.Batch);
        if (enableCounter)
        {
            count++;
        }
    }

    public UBatch()
    {
        setType(TemplateType.Batch);
    }
    
    @Override
    public String toString() {
        return getLineCodeNumber()+" "+getType().toString()+" "+getIdsim();
    }

    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        setCommandInserted(true);
        Queue queue = new Queue(parent,getIdsim()+"."+getName()+".queue");
        return new Batch(parent, batchSize, queue, batchTypeE, getIdsim()+" "+getName());
    }
    
}
