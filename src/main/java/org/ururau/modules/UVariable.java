/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;


import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import com.mxgraph.util.mxResources;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.elements.processview.description.ProcessDescription;
import jsl.utilities.random.distributions.Constant;
import jsl.utilities.random.distributions.DistributionIfc;
import org.ururau.ErrorMsg;
import org.ururau.GraphEditor.CustomGraph;
import org.ururau.Template;
import org.ururau.commands.DisplayVariable;
import org.ururau.commands.Nop;
import org.ururau.description.EntityProcessGenerator;

/**
 *
 * @author tulio
 */
public class UVariable extends Template implements Serializable {

    private static final long serialVersionUID = 7526471110122716147L;
    private static int count = 0;
    private String variableName = "var1";
    private String tagName = "tag1";
    private String value;
    protected int variableType = VariableType.Variable.ordinal();
    private VariableType variableTypeE = VariableType.Variable;
    private transient CustomGraph graph;
    private transient java.lang.Object cell;
    public static enum VariableType { Variable, BooleanTag, IntTag, DoubleTag};

    public int getVariableType() {
        return variableType;
    }

    public void setVariableType(int variableType) {
        VariableType rt[] = VariableType.values();
        this.variableTypeE = rt[variableType];
        this.variableType = variableType;
    }
/*
    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
    */
    public String getVariableName() {
        return variableName;
    }
    

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String [] getVariableTypeNames() {
        String vtName [] = new String[VariableType.values().length];
        for (VariableType vt : VariableType.values())
        {
            vtName[vt.ordinal()] = mxResources.get(vt.name());
        }
        return vtName;
    }

    public UVariable(boolean enableCounter)
    {
        setIdsim("V"+count);
        setName(mxResources.get("variable")+count);
        setType(Template.TemplateType.Variable);
        if (enableCounter)
        {
            count++;
        }
    }

    public UVariable()
    {
        setType(Template.TemplateType.Variable);
    }
    
    //@Override
    public String toString() {
        return getLineCodeNumber()+" "+getType().toString()+" "+getIdsim();
    }
    
    /**
     *
     * @param graph
     */
    public void setGraph(CustomGraph graph)
    {
        this.graph = graph;
    }
    
    public void setCell(java.lang.Object cell)
    {
        this.cell = cell;
    }
    
    public Object getCell()
    {
        return this.cell;
    }
    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        
        setCommandInserted(true);
      
        if (variableTypeE == VariableType.Variable)
        {
            
            try {
                return new DisplayVariable(parent, graph, cell, variableName, value, getIdsim()+"."+getName());
            } catch (Exception ex) {
                ErrorMsg.displayMsg("expression_error", "", "variable ["+variableName+"] not defined", ex);
            }
            
        } else {
            return new Nop(parent);
        }
        /*
        else if (variableTypeE == VariableType.DoubleTag) {
            return new CommunicationTagWriterReal(parent, tagName, value, getIdsim()+"."+getName());
        } else if (variableTypeE == VariableType.IntTag) {
            return new CommunicationTagWriterInterger(parent, tagName, value, getIdsim()+"."+getName());
        } else if (variableTypeE == VariableType.BooleanTag) {
            return new CommunicationTagWriterBoolean(parent, tagName, value, getIdsim()+"."+getName());
        } else {
        }
            return new Nop(parent);
        }*/
        return new Nop(parent);
    
    }
    
    public EntityProcessGenerator getEntityProcessGenerator(ModelElement m, ProcessDescription pd, 
                                                                        int commandIndex ) throws Exception
    {
        String timeBetweenArrivalsValue = "100";
        String firstCreation = "0.0";
        String entitiesPerArrival = "1";
        String maxArrivals = mxResources.get("infinity");

        DistributionIfc fc = new Constant(Double.parseDouble(firstCreation));
        return new EntityProcessGenerator(m,pd,fc, timeBetweenArrivalsValue, Long.MAX_VALUE, Double.POSITIVE_INFINITY, commandIndex, idsim+" "+name);
    
    }
   
}
