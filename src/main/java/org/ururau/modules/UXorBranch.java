/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;

import com.mxgraph.util.mxResources;
import java.io.Serializable;
import org.ururau.XorBranchType;
import org.ururau.commands.Decide.DecideType;

/**
 *
 * @author tulio
 */
public class UXorBranch implements Serializable {

    private double probability = 0.0;
    private int xorBranchType = XorBranchType.True.ordinal();
    private DecideType xorTypeE = DecideType.TWO_WAY_BY_CHANCE;
    private int xorType = DecideType.TWO_WAY_BY_CHANCE.ordinal();
    private String expression;
    private String numberOfPath;
    
    public UXorBranch()
    {
    }

    public UXorBranch(int xorType)
    {
        DecideType x[] = DecideType.values();
        this.xorTypeE = x[xorType];
        this.xorType = xorType;
    }

    
    // criado por Jhonathan Camacho
    public void setNumberOfPath( String number ){
        
        this.numberOfPath = number;
    
    }
    
    //Criado por Jhonathan Camacho
    public String getNumberOfPath(){
    
        return this.numberOfPath;
    }
    
    public int getXorType() {
        return xorType;
    }

    public void setXorType(int xorType) {
        DecideType x[] = DecideType.values();
        this.xorTypeE = x[xorType];
        this.xorType = xorType;
    }
    
    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }
    
    public double getProbability() {
        return probability;
    }

    public void setProbability(double probability) {
        if (probability > 0 && probability < 1)
            this.probability = probability;
        else if (probability <= 0)
            probability = 0.0000001;
        else
            probability = 0.9999999;
    }

    public int getXorBranchType() {
        return xorBranchType;
    }

    public void setXorBranchType(int xorBranchType) {
        this.xorBranchType = xorBranchType;
        DecideType x[] = DecideType.values();
    }

    public static String [] getXorBranchTypeNames() {
        String xName [] = new String[XorBranchType.values().length];
        for (XorBranchType x : XorBranchType.values())
        {
            xName[x.ordinal()] = mxResources.get(x.name());
        }
        return xName;
    }

    @Override
    public String toString()
    {
        if (xorBranchType == XorBranchType.True.ordinal())
        {
            if (expression != null)
                return getExpression();
            else
                return getProbability()*100+"%";
                
        }
        return "";
    }
}
