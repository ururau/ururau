/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;

import com.mxgraph.io.mxCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxResources;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxGraph.mxICellVisitor;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.Experiment;
import jsl.modeling.Model;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.processview.description.ProcessDescription;
import org.ururau.commands.Terminate;
import org.w3c.dom.Document;
import org.ururau.GraphEditor;
import org.ururau.GraphEditor.CustomGraph;
import org.ururau.Template;
import org.ururau.VariableType;
import org.ururau.commands.DelayExpression;
import org.ururau.description.EntityProcessGenerator;

/**
 *
 * @author tulio
 */
public final class UModel {
    private mxGraph graph;
    private mxGraph runGraph;
    private String name;
    private Model model;
    static int commandCounter = 0;
    public UModel(mxGraph tmodel, Experiment experiment) throws Exception
    {
        this(tmodel, mxResources.get("model"), experiment);
    }

    public UModel(mxGraph tmodel, String name, Experiment experiment) throws Exception
    {
        this.graph = tmodel;
        this.name = name;
        buildModel(experiment.getModel());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Model getModel()
    {
        return this.model;
    }

    private void makeRunnableGraph()
    {
        try {
            /* Clonando grafo serializando e depois deserializando */
            mxCodec codec = new mxCodec();
            String xml_graph = mxUtils.getXml(codec.encode(graph.getModel()));
            
            Document document = mxUtils.parseXml(URLDecoder.decode(xml_graph, "UTF-8"));
            runGraph = new GraphEditor.CustomGraph();
            mxCodec codec2 = new mxCodec(document);

            codec2.decode(document.getDocumentElement(), runGraph.getModel());
            ((CustomGraph)runGraph).updateModel();
            
            if (runGraph.getChildEdges(runGraph.getDefaultParent()).length < 1 )
            {
                return;
            }

            Object obj [] = (Object[]) runGraph.getChildCells(runGraph.getDefaultParent());
            for( int i = 0; i < obj.length; i++)
            {
                if ( ((mxCell)obj[i]).isVertex())
                {
                    mxCell vertex = (mxCell) obj[i];
                    Template vertext = (Template) vertex.getValue();
                    if (vertext instanceof UJump)
                    {
                        // vertice tem que ser origem, se não, pula
                        if (!((UJump)vertext).isOrigin())
                        {
                            continue;
                        }
                        /* template destino encontrado, varrer todas os vértices para encontrar vértice destino */
                        Template destination = ((UJump)vertext).getDestination();
                        Object obj2 [] = (Object[]) runGraph.getChildCells(runGraph.getDefaultParent());
                        for( int j = 0; j < obj2.length; j++)
                        {
                            if ( ((mxCell)obj2[j]).isVertex())
                            {
                                mxCell vertex2 = (mxCell) obj2[j];
                                Template vertext2 = (Template) vertex2.getValue();
                                
                                // vertice tem que ser destino, se não, pula
                                if (vertext2 instanceof UJump && ((UJump)vertext2).isOrigin())
                                {
                                    continue;
                                }
                                if (vertext2 instanceof UJump && vertext2.getName().compareTo(destination.getName()) == 0)
                                {
                                    // encontrou vértice destino. criar arco entre vértice 
                                    // onde está o jump e o vertice onde está destino
                                    runGraph.insertEdge(runGraph.getDefaultParent()," ", null,vertex,vertex2);
                                }

                            }
                        }
                    }
                        
                 }
             }
             ((CustomGraph)runGraph).updateModel();
            
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(UModel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void buildModel(Model model) throws Exception
    {
        if (this.model != null)
        {
            this.model = null;
        }
        commandCounter = 0;
        this.model = model.getModel();
        
        
        
        
        makeRunnableGraph();
        
        if (runGraph.getChildVertices(runGraph.getDefaultParent()).length < 1 )
        {
            return;
        }
        
        Object[] v = runGraph.getChildVertices(runGraph.getDefaultParent());
        ProcessDescription pd = null;
        for (int i=0; i< v.length; i++)
        {
            Object from = null;
            if (((mxCell)v[i]).getValue() instanceof UVariable)
            {
                from = (mxCell) v[i];
                UVariable variable = (UVariable) ((mxCell)from).getValue();
                pd  = new ProcessDescription(model, "Model for: "+variable.getName());
                            
                EntityProcessGenerator epg = variable.getEntityProcessGenerator(model, pd, 0);
                variable.setCell(from);
                CustomGraph g = (CustomGraph)runGraph;
                variable.setGraph(g); // TODO check why graph becomes null
                pd.addProcessCommand(variable.getProcessCommand(pd));
                pd.addProcessCommand(new Terminate(pd, true, variable.getName()));
                pd.turnOnAutoStart();
                            
            }
            if (((mxCell)v[i]).getValue() instanceof UCreate)
            {
                from = (mxCell) v[i];
                Object[] obj = runGraph.getOutgoingEdges(from); 
                //pegar vertice adjacente
                if (obj!= null)
                {
                    mxCell edge = (mxCell)obj[0];
                    Template t = (Template) ((mxCell)edge.getTarget()).getValue();
                    // ver se vertice adj é TAnd(&)
                    
                    if (t instanceof UAnd)
                    {
                        // se não foi colocado, cria pd e coloca comandos apartir do TAnd.line
                        if (!t.isCommandInserted())
                        {
                            UCreate create = (UCreate) ((mxCell)from).getValue();
                            pd  = new ProcessDescription(model, "Model for"+create.getName());
                            
                            EntityProcessGenerator epg = create.getEntityProcessGenerator(model, pd, t.getLineCodeNumber());
                            
                            //EntityType et = new EntityType(model,epg.getName()+"EntityType");
                            //epg.setEntityType(et);
                            //epg.setSendingOption(EntityType.SendOption.SEQ);
                            //EntityType et = pd.defineEntityType(epg.getName()+"EntityType"); // as defined in jsl-core
                            //epg.setEntityType(et); //as defined in jsl-core
                            putCommands(runGraph, from, model, pd, epg);
                            pd.addProcessCommand(new Terminate(pd, true, create.getName()));
                            pd.turnOnAutoStart();
                        }
                        else {
                            //se TAnd estiver colocado, apenas cria o gerador de entidades pulando para TAnd.line (aproveita pd)
                            UCreate create = (UCreate) ((mxCell)from).getValue();
                            //create.getEntityProcessGenerator(model, pd, t.getLineCodeNumber()).useDefaultEntityType(); as defined in jsl-core
                        }
                    } else {
                       // se não for TAnd, cria pd e coloca comandos apartir de index = 0

                        UCreate create = (UCreate) ((mxCell)from).getValue();
                        pd  = new ProcessDescription(model, "Model for"+create.getName());
                        EntityProcessGenerator epg = create.getEntityProcessGenerator(model, pd, t.getLineCodeNumber());
                        //System.out.println(create.toString());
                        //EntityType et = new EntityType(model,epg.getName()+"EntityType");
                        //epg.setEntityType(et);
                        //epg.setSendingOption(EntityType.SendOption.BY_SENDER);
                        //EntityType et = pd.defineEntityType(epg.getName()+"EntityType"); //as defined in jsl-core
                        ///epg.setEntityType(et); //as defined in jsl-core
                        putCommands(runGraph, from, model, pd, epg);
                        pd.addProcessCommand(new Terminate(model,true, create.getName()));
                        pd.turnOnAutoStart();
                    }
                }
            } 
        }
        // desmarcar comandos inseridos para uma nova simulação
        for (int i=0; i< v.length; i++)
        {
            Object from = null;
            if (((mxCell)v[i]).getValue() instanceof Template)
            {
                from = (mxCell) v[i];
                Template template = (Template) ((mxCell)from).getValue();
                template.setCommandInserted(false);
            } 
        }

    }

    private void putCommands(final mxGraph runGraph, Object from, final ModelElement parent, final ProcessDescription pd, final EntityProcessGenerator epg) {
        
        runGraph.traverse(from, true, new mxICellVisitor() {

        public boolean visit(Object vertex, Object edge) {

            if (! (((mxCell)vertex).getValue() instanceof UCreate))
            {
                Object [] edges = runGraph.getEdges(vertex);
                Object target = ((mxCell)edges[0]).getTarget();
                for (int i = 0; i < edges.length; i++)
                {
                    target = ((mxCell)edges[i]).getTarget();
                    Template ttarget = (Template) ((mxCell)target).getValue();
                    Template tvertex = (Template) ((mxCell)vertex).getValue();
                    // select next command
                    if( !(ttarget.getLineCodeNumber() == -1
                            || ttarget.getLineCodeNumber() == tvertex.getLineCodeNumber()) )
                    {
                        break;
                    }
                }
                Template ttarget = (Template) ((mxCell)target).getValue();
                Template tvertex = (Template) ((mxCell)vertex).getValue();
                Template tedge = null;


                if (ttarget.getLineCodeNumber() >= tvertex.getLineCodeNumber())
                {
                    //System.out.println(tvertex.getLineCodeNumber()+" "+tvertex.getIdsim()+"     lineCounter="+Template.getLineCounter());
                    tvertex.setCommandInserted(true);
                    System.out.print("command counter = "+commandCounter+" ");
                    ProcessCommand pc = tvertex.getProcessCommand(parent);
                    pd.addProcessCommand(pc);
                    //et.addToSequence(pc); // not compile
                    commandCounter++;
                    System.out.println(tvertex.toString());
                } else
                {
                    //System.out.println(tvertex.getLineCodeNumber()+" "+tvertex.getIdsim()+"     lineCounter="+Template.getLineCounter());
                    tvertex.setCommandInserted(true);
                    System.out.print("command counter = "+commandCounter+" ");
                    pd.addProcessCommand(tvertex.getProcessCommand(parent));
                    commandCounter++;
                    System.out.println(tvertex.toString());
                    for (int i = 0; i < runGraph.getEdges(vertex).length; i++)
                    {
                        mxCell e = ((mxCell)runGraph.getEdges(vertex)[i]);
                        if ( ((mxCell)e).getValue() instanceof UJump
                                && (!(((mxCell)vertex).getValue() instanceof UAnd)) )
                        {
                            UJump jump = (UJump) ((mxCell)e).getValue();
                            System.out.print("command counter = "+commandCounter+" ");
                            //System.out.println(jump.getLineCodeNumber()+" "+jump.getIdsim()+"->"+jump.getDestination().getLineCodeNumber());
                            tvertex.setCommandInserted(true);
                            pd.addProcessCommand(jump.getProcessCommand(parent));
                            commandCounter++;
                        }
                    }
                }

            } else {
                
                UCreate create = (UCreate)((mxCell)vertex).getValue();
                
                //System.out.print("command counter = "+commandCounter+" ");
                /*
                if (create.getTimeBetweenArrivalsType() == VariableType.Expression.ordinal())
                {
                    DelayExpression delayTEC = new DelayExpression(parent, create.getTimeBetweenArrivalsValue());
                    pd.addProcessCommand(delayTEC);
                } else if (create.getTimeBetweenArrivalsType() == VariableType.Exponential.ordinal())
                {
                    DelayExpression delaytec = new DelayExpression(
                            parent, "EXPO("+create.getTimeBetweenArrivalsValue()+")");
                    pd.addProcessCommand(delaytec);                    
                } else {
                    DelayExpression delaytec = new DelayExpression(
                            parent, create.getTimeBetweenArrivalsValue());
                    pd.addProcessCommand(delaytec);   
                }
                
                    //et.addToSequence(pc); // not compile
                commandCounter++;*/
            }
            return true;
          }
      });
    }

}


