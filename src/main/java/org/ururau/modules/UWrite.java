/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.modules;

import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import org.ururau.Template;
import org.ururau.commands.Write;
import org.ururau.commands.Write.WriteType;

/**
 *
 * @author tulio
 */
public class UWrite extends Template implements Serializable {

    private static final long serialVersionUID = 75164799992776147L;
    private static int count = 0;
    private String value = "";
    private WriteType writeTypeE = WriteType.VARIABLE;
    private int writeType = 0;
    private String filename = "";
    
    public int getWriteType() {
        return writeType;
    }

    public void setWriteType(int writeType) {
        this.writeType = writeType;
        WriteType wt[] = WriteType.values();
        this.writeTypeE = wt[writeType];
    }
    
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    
    public UWrite(boolean enableCounter)
    {
        super("W"+count, "");
        setType(TemplateType.Write);
        if (enableCounter)
        {
            count++;
        }
    }

    public UWrite()
    {
        setType(TemplateType.Write);
        value = "";
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    @Override
    public String toString() {
        return getLineCodeNumber()+" "+getType().toString()+" "+getIdsim()+", expression/var:="+getValue();
    }
    
    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        setCommandInserted(true);
        return new Write(parent, writeTypeE, filename,value,getIdsim()+"."+getName());
    }
}
