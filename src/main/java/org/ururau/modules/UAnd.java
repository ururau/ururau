/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;

import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import org.ururau.Template;
import org.ururau.commands.Nop;

/**
 *
 * @author tulio
 */
public class UAnd extends Template implements Serializable {

    private static int count  = 0;
    public UAnd(boolean enableCounter)
    {
        setIdsim("AND"+count);
        setType(TemplateType.And);
        if (enableCounter)
        {
            count++;
        }
    }

    public UAnd()
    {
        setType(TemplateType.And);
    }
    
    public String toString()
    {
        return getLineCodeNumber()+" And";
    }
    
    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        setCommandInserted(true);
        return new Nop(parent);
    }

}
