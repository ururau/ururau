/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.modules;

/**
 *
 * @author Fraga
 */
import java.io.Serializable;
import java.util.ArrayList;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import org.ururau.ReadWriteVariable;
import org.ururau.Template;
import org.ururau.commands.MyWrite;
import org.ururau.commands.MyWrite.MyWriteType;
import org.ururau.commands.SheetReadWrite;

/**
 *
 * @author Projeto
 */
public class USheetReadWrite extends Template implements Serializable {
    
    private static final long serialVersionUID = 75164799192776147L;
    private static int count = 0;
    private String filename = "";
    private ArrayList<ReadWriteVariable> variables;
    
    
    
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    
     public USheetReadWrite(boolean enableCounter)
    {
        super("RW"+count, "");
        setType(TemplateType.SheetReadWrite);
        if (enableCounter)
        {
            count++;
        }
        variables = new ArrayList<ReadWriteVariable>();
    }
     
    public USheetReadWrite()
    {
        setType(TemplateType.SheetReadWrite);
        variables = new ArrayList<ReadWriteVariable>();
    }

    public ArrayList<ReadWriteVariable> getVariables() {
        return variables;
    }

    public void setVariables(ArrayList<ReadWriteVariable> variables) {
        this.variables = variables;
    }

    @Override
    public String toString() {
        return getLineCodeNumber()+" "+getType().toString()+" "+getIdsim()+", variables:="+getVariables();
    }
    
    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        setCommandInserted(true);
        return new SheetReadWrite(parent, variables, filename,getIdsim()+"."+getName());
    }
     
}
