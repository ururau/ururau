/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;

import com.itextpdf.text.DocumentException;
import com.mxgraph.util.mxResources;
import java.io.IOException;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import jsl.modeling.Experiment;
import org.apache.xmlbeans.XmlError;
import org.jinterop.dcom.common.JIException;
import org.openscada.opc.lib.common.AlreadyConnectedException;
import org.ururau.Communication;
import org.ururau.ErrorMsg;
import org.ururau.ReportGenerator;
import org.ururau.view.GoView;


/**
 *
 * @author tulio
 */
public class USimulation implements Serializable,Runnable {

    Experiment simulation = null;
    private static final long serialVersionUID = 7527471110122716147L;
    protected int numberOfReplications = 1;
    protected int lenghtOfReplication = 100;
    protected int lenghtOfWarmUp = 0;
    protected int annLibraryType = 0;
    protected UModel myUModel;
    protected static USimulation usimulation = null;
    protected static Thread myThread = null;
    public void setUModel(UModel umodel) {
        myUModel = umodel;
    }
    
    public static enum ANNType {ENCOG, FANN};

    public USimulation()
    {

    }

    @Override
    public void run()
    {
            GoView goView = new GoView();
            try{
                try{
                    this.simulation.turnOnExperimentReport();
                    this.simulation.turnOnExperimentConsoleOutput();
                    this.simulation.setSaveExperimentStatisticsOption(true);
                    goView.setVisible(true);
                    USimulation.getInstance().getSimulation().runAll();
                    goView.setVisible(false);
                }catch(Exception exc)
                {
                    String msg = "";
                    if (exc.getMessage()==null)
                    {
                        msg = "Erro desconhecido";
                    } else {
                        //JOptionPane.showMessageDialog(null,mxResources.get("restartWarning"));
                        msg = exc.getMessage().replaceAll(" ", "_");
                        msg = msg.replaceAll("!", "");
                        msg = msg.replaceAll(">", "GN");
                        msg = msg.replaceAll("<", "LN");
                        msg = msg.replaceAll(">=", "GE");
                        msg = msg.replaceAll("<=", "LE");
                        msg = msg.replaceAll("=", "EQ");
                        msg = msg.replaceAll("0.0", "0");
                        msg = msg.replaceAll("\\..*", ".");
                        msg = msg.replaceAll("\\:.*", ":");
                        msg = msg.replaceAll(":", "");
                        msg = msg.replaceAll("\\.", "");
                        msg = msg.replaceAll("\n", "");
                        msg = msg.replaceAll("\\(", "");
                        msg = msg.replaceAll("\\)", "");
                        msg = msg.replaceAll(",", "");
                    }
                    JOptionPane.showMessageDialog(null,mxResources.get(msg)+"  \n\ndetails: "+ exc.toString());
                }
                Communication com = Communication.getInstance();
                ErrorMsg.setDisplayErrorInDialog(true);
                
                if (com.isEnabled())
                {
                    try {
                        com.connect();
                        com.getTags();
                    } catch (JIException ex) {
                        Logger.getLogger(USimulation.class.getName()).log(Level.SEVERE, null, ex);
                        JOptionPane.showMessageDialog(null,ex.getMessage(),mxResources.get("abortedSimulation"),JOptionPane.WARNING_MESSAGE);
                        JOptionPane.showMessageDialog(null,mxResources.get("abortedSimulation"),"Ururau",JOptionPane.WARNING_MESSAGE);
                        goView.setVisible(false);
                    } catch (IllegalArgumentException ex) {
                        JOptionPane.showMessageDialog(null,ex.getMessage(),mxResources.get("abortedSimulation"),JOptionPane.WARNING_MESSAGE);
                        JOptionPane.showMessageDialog(null,mxResources.get("abortedSimulation"),"Ururau",JOptionPane.WARNING_MESSAGE);
                        Logger.getLogger(USimulation.class.getName()).log(Level.SEVERE, null, ex);
                        goView.setVisible(false);
                    } catch (UnknownHostException ex) {
                        JOptionPane.showMessageDialog(null,ex.getMessage(),mxResources.get("abortedSimulation"),JOptionPane.WARNING_MESSAGE);
                        JOptionPane.showMessageDialog(null,mxResources.get("abortedSimulation"),"Ururau",JOptionPane.WARNING_MESSAGE);
                        Logger.getLogger(USimulation.class.getName()).log(Level.SEVERE, null, ex);
                        goView.setVisible(false);
                    } catch (AlreadyConnectedException ex) {
                        JOptionPane.showMessageDialog(null,ex.getMessage(),mxResources.get("abortedSimulation"),JOptionPane.WARNING_MESSAGE);
                        JOptionPane.showMessageDialog(null,mxResources.get("abortedSimulation"),"Ururau",JOptionPane.WARNING_MESSAGE);
                        Logger.getLogger(USimulation.class.getName()).log(Level.SEVERE, null, ex);
                        goView.setVisible(false);
                    }
                }
                
                if (com.isEnabled())
                {
                    com.disconnect();
                }
                
                    if (myThread != null) 
                    {
                    ReportGenerator r = new ReportGenerator();
                    String filename = "";
                    if (myUModel.getName() == null)
                    {
                        filename = mxResources.get("noName");
                    } else {
                        filename = myUModel.getName();
                    }
                    filename = filename.replace(".sip", "");
                    filename = filename.replace(" ", "_");
                    filename = filename + ".pdf";
                    // TODO: make reports in pdf
                    r.createPdf(filename, USimulation.getInstance().getSimulation());
                    r.showReport(filename);
                }
                
                
            }catch(Exception ex)
            {
                Logger.getLogger(USimulation.class.getName()).log(Level.SEVERE,null, ex);
                JOptionPane.showMessageDialog(null,ex.getMessage(),mxResources.get("pdfGenerationError"),JOptionPane.WARNING_MESSAGE);
            } 
            //Logger.getLogger(USimulation.class.getName()).log(Level.SEVERE, null, ex);
            //JOptionPane.showMessageDialog(null,ex.getMessage(),mxResources.get("pdfGenerationError"),JOptionPane.WARNING_MESSAGE);
        

    }
    
    public void end() {
        if (myThread !=null)
        {
            //USimulation.getInstance().getSimulation().stop();
            USimulation.getInstance().getSimulation().end();
            myThread.interrupt();
            myThread = null;
            JOptionPane.showMessageDialog(null,mxResources.get("abortedSimulation"),"Ururau",JOptionPane.WARNING_MESSAGE);
        }
    }
    

    public Experiment getSimulation() {
        return simulation;
    }
    public int getAnnLibraryType() {
        return annLibraryType;
    }

    public void setAnnLibraryType(int annLibraryType) {
        this.annLibraryType = annLibraryType;
    }
    
    public void resetSeed()
    {
        //long rseed[] = {42398,16239,17193,3648, 63476,11744};
        // as defined in JSLb1
        long rseed[] = {12345, 12345, 12345, 12345, 12345, 12345};
        //jsl.utilities.random.rng.RngStream.getDefaultStream().setAntithetic(true);
        
        jsl.utilities.random.rng.RngStream.setPackageSeed(rseed);      
        jsl.utilities.random.rng.RngStream.resetAllStartStreams();
        jsl.utilities.random.rng.RngStream.resetAllStartSubstreams();
        jsl.utilities.random.rng.RngStream.setPackageSeed(rseed);
        
        // as defined in jsl-core
        //jsl.utilities.random.rng.RngStream.getDefaultStream().resetStartStream();
        //jsl.utilities.random.rng.RngStream.resetAllStartStreams();
        
        //jsl.utilities.random.rng.RngStream.getDefaultStream().resetAllStartSubstreams();
        //jsl.utilities.random.rng.RngStream.getDefaultStream().setFactorySeed(rseed);
              
    }
    
    public void setUp(String simulationName)
    {
        Experiment sim = new Experiment(simulationName);
        // set the parameters of the experiment
        sim.setNumberOfReplications(numberOfReplications);
        sim.setLengthOfReplication(lenghtOfReplication);
        sim.setLengthOfWarmUp(lenghtOfWarmUp);
        this.simulation = sim;
        // turn on the desired reporting
        /*
        sim.getExperiment().turnOnExperimentReport();
        experiment.setSaveExperimentStatisticsOption(true);
        experiment.turnOnExperimentConsoleOutput();
         * 
         */
    }
    
    public static String [] getANNTypeNames() {
        String lName [] = new String[ANNType.values().length];
        for (ANNType l : ANNType.values())
        {
            lName[l.ordinal()] = mxResources.get(l.name());
        }
        return lName;
    }
    
    public int getLenghtOfReplication() {
        return lenghtOfReplication;
    }

    public void setLenghtOfReplication(int lenghtOfReplication) {
        this.lenghtOfReplication = lenghtOfReplication;
    }

    public int getLenghtOfWarmUp() {
        return lenghtOfWarmUp;
    }

    public void setLenghtOfWarmUp(int lenghtOfWarmUp) {
        this.lenghtOfWarmUp = lenghtOfWarmUp;
    }

    public int getNumberOfReplications() {
        return numberOfReplications;
    }

    public void setNumberOfReplications(int numberOfReplications) {
        this.numberOfReplications = numberOfReplications;
    }
    
    public static USimulation getInstance()
    {
        if (USimulation.usimulation == null)
        {
            USimulation.usimulation = new USimulation();
        }
        return USimulation.usimulation;
    }
    
    public static Thread getThread()
    {
        if (myThread == null)
        {
            myThread = new Thread(usimulation);
        }
        return myThread;
    }
    public static void resetThread()
    {
        myThread = null;
    }
}