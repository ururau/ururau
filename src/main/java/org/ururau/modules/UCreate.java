
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;

import com.mxgraph.util.mxResources;
import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.processview.description.ProcessDescription;
import jsl.utilities.random.distributions.Constant;
import jsl.utilities.random.distributions.DistributionIfc;
import jsl.utilities.random.distributions.Exponential;
import org.ururau.Template;
import org.ururau.VariableType;
import org.ururau.commands.Nop;
import org.ururau.description.EntityProcessGenerator;


/**
 *
 * @author tulio
 */
public class UCreate extends Template implements Serializable {

    private static final long serialVersionUID = 7526471155122776147L;
    private static int count = 0;
    
    public String getEntitiesPerArrival() {
        return entitiesPerArrival;
    }

    public void setEntitiesPerArrival(String entitiesPerArrival) {
        this.entitiesPerArrival = entitiesPerArrival;
    }

    public String getFirstCreation() {
        return firstCreation;
    }

    public void setFirstCreation(String firstCreation) {
        this.firstCreation = firstCreation;
    }

    public String getMaxArrivals() {
        return maxArrivals;
    }

    public void setMaxArrivals(String maxArrivals) {
        this.maxArrivals = maxArrivals;
    }

    public String getTimeBetweenArrivalsValue() {
        return timeBetweenArrivalsValue;
    }

    public void setTimeBetweenArrivalsValue(String timeBetweenArrivalsValue) {
        this.timeBetweenArrivalsValue = timeBetweenArrivalsValue;
    }

    public int getTimeBetweenArrivalsType() {
        return timeBetweenArrivalsType;
    }

    public void setTimeBetweenArrivalsType(int timeBetweenArrivalsType) {
        VariableType vt[] = VariableType.values();
        this.timeBetweenArrivalsTypeE = vt[timeBetweenArrivalsType];
        this.timeBetweenArrivalsType = timeBetweenArrivalsType;
    }

    private VariableType timeBetweenArrivalsTypeE = VariableType.Exponential;
    private int timeBetweenArrivalsType = VariableType.Exponential.ordinal();
    private String timeBetweenArrivalsValue = "1";
    private String firstCreation = "0.0";
    private String entitiesPerArrival = "1";
    private String maxArrivals = mxResources.get("infinity");

    public UCreate(boolean enableCounter)
    {
        setIdsim("E"+count);
        setName(mxResources.get("entity")+count);
        setType(TemplateType.Create);
        if (enableCounter)
        {
            count++;
        }
    }

    public UCreate ()
    {
        setType(TemplateType.Create);
    }

    public ProcessCommand getProcessCommand(ModelElement m)
    {
        return new Nop(m,getName());
    }
    
    public String toString() {
        return getLineCodeNumber()+" "+getIdsim()+" maxArrivals:"+maxArrivals
                +", timeBtwnArrivalType: "+timeBetweenArrivalsTypeE.name()
                +", timeBetweenArrivalsValue: "+timeBetweenArrivalsValue
                +", firstCreation: "+firstCreation;
    }

    public EntityProcessGenerator getEntityProcessGenerator(ModelElement m, ProcessDescription pd, 
                                                                        int commandIndex ) throws Exception
    {

        if (maxArrivals.equals(mxResources.get("infinity")))
        {
            if (timeBetweenArrivalsTypeE == VariableType.Constant)
            {
                DistributionIfc fc = new Constant(Double.parseDouble(firstCreation));
                return new EntityProcessGenerator(m,pd,fc, timeBetweenArrivalsValue, Long.MAX_VALUE, Double.POSITIVE_INFINITY, commandIndex, idsim+" "+name);
            }
            else if (timeBetweenArrivalsTypeE == VariableType.Exponential)
            {
                String tba = "EXPO("+timeBetweenArrivalsValue+")";
                DistributionIfc fc = new Constant(Double.parseDouble(firstCreation));
                return new EntityProcessGenerator(m,pd,fc, tba, Long.MAX_VALUE, Double.POSITIVE_INFINITY, commandIndex, idsim+" "+name);
            }
            else if (timeBetweenArrivalsTypeE == VariableType.Expression)
            {
                DistributionIfc fc = new Constant(Double.parseDouble(firstCreation));
                return new EntityProcessGenerator(m,pd,fc, timeBetweenArrivalsValue, Long.MAX_VALUE, Double.POSITIVE_INFINITY, commandIndex, idsim+" "+name);
            }
        } else {
            if (timeBetweenArrivalsTypeE == VariableType.Constant)
            {
                DistributionIfc fc = new Constant(Double.parseDouble(firstCreation));
                long maxNum = Long.parseLong(maxArrivals);
                return new EntityProcessGenerator(m, pd, fc, timeBetweenArrivalsValue, maxNum, Double.POSITIVE_INFINITY, commandIndex, idsim+" "+name);
            }
            else if (timeBetweenArrivalsTypeE == VariableType.Exponential)
            {
                DistributionIfc fc = new Constant(Double.parseDouble(firstCreation));
                long maxNum = Long.parseLong(maxArrivals);
                String tba = "EXPO("+timeBetweenArrivalsValue+")";
                return new EntityProcessGenerator(m, pd, fc, timeBetweenArrivalsValue, maxNum, Double.POSITIVE_INFINITY, commandIndex, idsim+" "+name);
            }
            else if (timeBetweenArrivalsTypeE == VariableType.Expression)
            {
                DistributionIfc fc = new Constant(Double.parseDouble(firstCreation));
                long maxNum = Long.parseLong(maxArrivals);
                return new EntityProcessGenerator(m, pd, fc, timeBetweenArrivalsValue, maxNum, Double.POSITIVE_INFINITY, commandIndex, idsim+" "+name);
            }
        }
        throw new Exception("Invalid Create");
    }
}
