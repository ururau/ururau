/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.modules;

/**
 *
 * @author Fraga
 */
import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import org.ururau.Template;
import org.ururau.commands.RunMacro;

/**
 *
 * @author Projeto
 */
public class URunMacro extends Template implements Serializable {
    
    private static final long serialVersionUID = 75163799192776147L;
    private static int count = 0;
    private String filename = "";
    private String macro ="";
    
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    
     public URunMacro(boolean enableCounter)
    {
        super("X"+count, "");
        setType(TemplateType.SheetReadWrite);
        if (enableCounter)
        {
            count++;
        }
    }
     
    public URunMacro()
    {
        setType(TemplateType.SheetReadWrite);
    }

    public String getMacro() {
        return macro;
    }

    public void setMacro(String macro) {
        this.macro = macro;
    }

    
    @Override
    public String toString() {
        return getLineCodeNumber()+" "+getType().toString()+" "+getIdsim()+", macro:="+getMacro();
    }
    
    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        setCommandInserted(true);
        return new RunMacro(parent, filename, macro,getIdsim()+"."+getName());
    }
     
}
