/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;

import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxResources;
import java.io.Serializable;
import java.util.ArrayList;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import org.ururau.Template;
import org.ururau.commands.Record;
import org.ururau.commands.Record.RecordType;
import org.ururau.commands.RecordCounter;
import org.ururau.commands.RecordTimeBetween;
import org.ururau.commands.RecordTimeInterval;

/**
 *
 * @author tulio
 */
public class URecord extends Template implements Serializable {

    private static final long serialVersionUID = 7526471110122716147L;
    private static int count = 0;

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getVariableValue() {
        return variableValue;
    }

    public void setVariableValue(String variableValue) {
        this.variableValue = variableValue;
    }
    private String attributeName = "atrib1";
    private String variableValue = "1";
    protected int recordType = RecordType.COUNTER.ordinal();
    private RecordType recordTypeE = RecordType.COUNTER;

    public int getRecordType() {
        return recordType;
    }

    public void setRecordType(int recordType) {
        RecordType rt[] = RecordType.values();
        this.recordTypeE = rt[recordType];
        this.recordType = recordType;
    }

    public URecord(boolean enableCounter)
    {
        setIdsim("R"+count);
        setName(mxResources.get("record")+count);
        setType(TemplateType.Record);
        if (enableCounter)
        {
            count++;
        }
    }

    public URecord()
    {
        setType(TemplateType.Record);
    }

    public static String [] getRecordTypeNames() {
        String vtName [] = new String[RecordType.values().length];
        for (RecordType vt : RecordType.values())
        {
            vtName[vt.ordinal()] = mxResources.get(vt.name());
        }
        return vtName;
    }

    @Override
    public String toString() {
        return getLineCodeNumber()+" "+getType().toString()+" "+getIdsim();
    }

    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        setCommandInserted(true);
        if (recordTypeE == RecordType.COUNTER)
        {
            //Variable var = new Variable(parent, Double.parseDouble(variableValue));
            return new RecordCounter(parent, variableValue, getIdsim()+" "+getName());
        }
        else if (recordTypeE == RecordType.TIME_INTERVAL)
        {
            return new RecordTimeInterval(parent, attributeName, getIdsim()+" "+getName());
        }
        else {
            return new RecordTimeBetween(parent, variableValue, getIdsim()+" "+getName());
        }
    }

    public String [] getVariablesNames()
    {
        ArrayList<String> list = new ArrayList<String>();
        if (graph.getChildVertices(graph.getDefaultParent()).length < 1 )
        {
            return (String[]) list.toArray();
        }

        Object[] v = graph.getChildVertices(graph.getDefaultParent());
        for (int i=0; i< v.length; i++)
        {
            if (((mxCell)v[i]).getValue() instanceof UAssign)
            {
                UAssign a = (UAssign) ((mxCell)v[i]).getValue();
                list.add(a.getName());
            }
        }
        String [] strList = new String[list.size()];
        for(int i=0; i< list.size(); i++)
        {
            strList[i] = list.get(i);
        }
        return strList;
    }

    public String [] getAttributesNames()
    {
        ArrayList<String> list = new ArrayList<String>();
        if (graph == null)
        {
            return null;
        }
        if (graph.getChildVertices(graph.getDefaultParent()).length < 1 )
        {
            return (String[]) list.toArray();
        }

        Object[] v = graph.getChildVertices(graph.getDefaultParent());
        for (int i=0; i< v.length; i++)
        {
            if (((mxCell)v[i]).getValue() instanceof UAssign)
            {
                UAssign a = (UAssign) ((mxCell)v[i]).getValue();
                list.add((a.getAttributeName()));
            }
        }
        String [] strList = new String[list.size()];
        for(int i=0; i< list.size(); i++)
        {
            strList[i] = list.get(i);
        }
        return strList;
    }

}
