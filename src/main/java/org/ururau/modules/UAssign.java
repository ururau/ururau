/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;


import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import com.mxgraph.util.mxResources;
import org.ururau.Template;
import org.ururau.commands.AssignAttribute;
import org.ururau.commands.AssignVariable;
import org.ururau.commands.CommunicationTagWriterBoolean;
import org.ururau.commands.CommunicationTagWriterInterger;
import org.ururau.commands.CommunicationTagWriterReal;
import org.ururau.commands.Nop;
/**
 *
 * @author tulio
 */
public class UAssign extends Template implements Serializable {

    private static final long serialVersionUID = 7526471110122716147L;
    private static int count = 0;
    private String attributeName = "atrib1";
    private String variableName = "var1";
    private String tagName = "tag1";
    private String value;
    protected int assignType = AssignType.Attribute.ordinal();
    private AssignType assignTypeE = AssignType.Attribute;

    public static enum AssignType { Variable, Attribute, BooleanTag, IntTag, DoubleTag};

    public int getAssignType() {
        return assignType;
    }

    public void setAssignType(int assignType) {
        AssignType rt[] = AssignType.values();
        this.assignTypeE = rt[assignType];
        this.assignType = assignType;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
    public String getVariableName() {
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String [] getAssignTypeNames() {
        String vtName [] = new String[AssignType.values().length];
        for (AssignType vt : AssignType.values())
        {
            vtName[vt.ordinal()] = mxResources.get(vt.name());
        }
        return vtName;
    }

    public UAssign(boolean enableCounter)
    {
        setIdsim("C"+count);
        setName(mxResources.get("assignment")+count);
        setType(TemplateType.Assign);
        if (enableCounter)
        {
            count++;
        }
    }

    public UAssign()
    {
        setType(TemplateType.Assign);
    }
    
    @Override
    public String toString() {
        return getLineCodeNumber()+" "+getType().toString()+" "+getIdsim();
    }
    
    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        setCommandInserted(true);
        if (assignTypeE == AssignType.Attribute)
        {
            return new AssignAttribute(parent, attributeName, value, getIdsim()+"."+getName());
        } 
        else if (assignTypeE == AssignType.Variable)
        {
            return new AssignVariable(parent, variableName, value, getIdsim()+"."+getName());
        } else if (assignTypeE == AssignType.DoubleTag) {
            return new CommunicationTagWriterReal(parent, tagName, value, getIdsim()+"."+getName());
        } else if (assignTypeE == AssignType.IntTag) {
            return new CommunicationTagWriterInterger(parent, tagName, value, getIdsim()+"."+getName());
        } else if (assignTypeE == AssignType.BooleanTag) {
            return new CommunicationTagWriterBoolean(parent, tagName, value, getIdsim()+"."+getName());
        } else {
            return new Nop(parent);
        }
    }
}
