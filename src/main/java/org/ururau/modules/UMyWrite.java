/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.modules;

import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import org.ururau.Template;
import org.ururau.commands.MyWrite;
import org.ururau.commands.MyWrite.MyWriteType;

/**
 *
 * @author Projeto
 */
public class UMyWrite extends Template implements Serializable {
    
    private static final long serialVersionUID = 75164799992776147L;
    private static int count = 0;
    private String value = "";
    private MyWriteType myWriteTypeE = MyWriteType.VARIABLE;
    private int myWriteType = 0;
    private String filename = ""; 
    
    public int getMyWriteType() {
        MyWriteType wt[] = MyWriteType.values();
        this.myWriteTypeE = wt[myWriteType];
        return myWriteType;
    }
    
    public void setMyWriteType(int myWriteType) {
    this.myWriteType = myWriteType;
    MyWriteType wt[] = MyWriteType.values();
    this.myWriteTypeE = wt[myWriteType];
    }
    
     public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    
     public UMyWrite(boolean enableCounter)
    {
        super("W"+count, "");
        setType(TemplateType.MyWrite);
        if (enableCounter)
        {
            count++;
        }
        
        
    }
     
      public UMyWrite()
    {
        setType(TemplateType.MyWrite);
        value = "";
    }
     public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    @Override
    public String toString() {
        return getLineCodeNumber()+" "+getType().toString()+" "+getIdsim()+", expression/var:="+getValue();
    }
    
    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        setCommandInserted(true);
        return new MyWrite(parent, myWriteTypeE, filename,value,getIdsim()+"."+getName());
    }
     
}
