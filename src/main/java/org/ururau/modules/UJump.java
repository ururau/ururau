/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;

import com.mxgraph.util.mxResources;
import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import org.ururau.Template;
import org.ururau.commands.JumpTo;
import org.ururau.commands.Nop;

/**
 *
 * @author tulio
 */
public final class UJump extends Template implements Serializable {

    private static final long serialVersionUID = 7522371112122479111L;
    private static int count = 0;
    private Template destination;
    boolean origin = false;
    static boolean defaultOrigin = false;

    public UJump(boolean enableCounter)
    {
        setIdsim("J"+count);
        setName("A"+count);
        setType(TemplateType.Jump);
        setOrigin(defaultOrigin);
        defaultOrigin = !defaultOrigin;
        lineCodeNumber = 0;
        if (enableCounter)
        {
            count++;
        }
    }

    public UJump()
    {
        lineCodeNumber = 0;
        //setOrigin(defaultOrigin);  
        setType(TemplateType.Jump);
    }

    public void setDestination(Template destination) {
        this.destination = destination;
    }
    
    public Template getDestination() {
        return destination;
    }
    
    public boolean isOrigin() {
        return origin;
    }

    public void setOrigin(boolean isOrigin) {
        this.origin = isOrigin;
    }
    
    @Override
    public String toString()
    {
        return "";
    }
            
    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        if (!isOrigin())
        {
            System.out.println(getLineCodeNumber()+" "+getIdsim()+"->nop line");
            return new Nop(parent);
        }
        else
        {
            if (destination!=null)
            {
                System.out.println(getLineCodeNumber()+" "+getIdsim()+"->"+(destination.getLineCodeNumber())+"."+getName());
                return new JumpTo(parent, destination.getLineCodeNumber(), getIdsim()+"."+getName());
            } else {
                System.out.println(getLineCodeNumber()+" "+getIdsim()+"->nop2");
                return new Nop(parent);
            }
    
        }
    }

}
