/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;

import com.mxgraph.util.mxResources;
import java.io.Serializable;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.queue.UQueue;
import jsl.modeling.elements.resource.Resource;
import jsl.modeling.elements.variable.Variable;
import org.ururau.ErrorMsg;
import org.ururau.LocalType;
import org.ururau.Template;
import org.ururau.commands.Nop;
import org.ururau.commands.Release;
import org.ururau.commands.Seize;

/**
 *
 * @author tulio
 */
public class ULocal extends Template implements Serializable {

    private static final long serialVersionUID = 7526471119122716147L;
    private static int count = 0;
    private int amountRequested = 1;
    private UResource r;
    private String releasebleQueue;
    private int localType = LocalType.Local.ordinal();
    private LocalType localTypeE = LocalType.Local;
    private String queueName ;
    private int priority = 1;
    private boolean waitStats = true;
    private boolean sizeStats = true;
    
    public ULocal(boolean enableCounter)
    {
        setIdsim("L"+count);
        setName(mxResources.get("local")+count);
        setType(TemplateType.Local);
        if (enableCounter)
        {
            count++;
        }
        queueName = getIdsim()+"."+"queue";
    }

    public ULocal()
    {
        setType(TemplateType.Local);
    }

    public long getAmountRequested() {
        return amountRequested;
    }

    public void setAmountRequested(int amountRequested) {
        if (amountRequested > 0)
            this.amountRequested = amountRequested;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queue) {
        this.queueName = queue;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
    
    public boolean isWaitStats() {
        return waitStats;
    }

    public void setWaitStats(boolean waitStats) {
        this.waitStats = waitStats;
    }

    public boolean isSizeStats() {
        return sizeStats;
    }

    public void setSizeStats(boolean sizeStats) {
        this.sizeStats = sizeStats;
    }
      
    public int getLocalType() {
        return localType;
    }

    public void setLocalType(int localType) {
        LocalType l[] = LocalType.values();
        this.localTypeE = l[localType];
        this.localType = localType;
    }

    public void setTResource(UResource r) {
        this.r = r;
    }

    public UResource getTResource()
    {
        return this.r;
    }

    public void setReleasebleQueue(String releasebleQueue)
    {
        this.releasebleQueue = releasebleQueue;
    }
    
    public String getReleasebleQueue()
    {
        return this.releasebleQueue;
    }
    @Override
    public String toString()
    {
        if (localTypeE.equals(LocalType.Seize) && queueName != null && r !=null)
            return getLineCodeNumber()+" Local "+getIdsim()+" ("+localTypeE.toString()+") "+getQueueName()+" R->"+r.getIdsim();
        else if (localTypeE.equals(LocalType.Release) && releasebleQueue != null && r !=null)
            return getLineCodeNumber()+" Local "+getIdsim()+" ("+localTypeE.toString()+") "+releasebleQueue+" R->"+r.getIdsim();
        else
            return getLineCodeNumber()+" Local "+getIdsim()+" ("+localTypeE.toString()+") ";
    }
    
    public ProcessCommand getSeize(ModelElement m) throws Exception
    {
        if (this.r != null)
        {
            return getSeize(m,this.r.getResource(m));
        }
        throw new Exception("Invalid Seize (no Resource)");
    }

    public ProcessCommand getSeize(ModelElement m, Resource r)
    {
        UQueue q;
        if (m.getModel().getModelElement(getQueueName()) == null)
            q = new UQueue(m, getQueueName());
        else
            q = (UQueue) m.getModel().getModelElement(getQueueName());
        Variable amountReq = new Variable(m, (double)this.amountRequested, getIdsim()+"."+getName()+".amountReq");
        return new Seize(m, amountReq, r, q, getPriority(), waitStats, sizeStats, getIdsim()+" "+getName()+".seize");
    }

    public ProcessCommand getRelease(ModelElement m)
    {
        return getRelease(m,this.r.getResource(m));
    }

    public ProcessCommand getRelease(ModelElement m, Resource r)
    {
        if (r.getModel().containsModelElement(this.releasebleQueue))
        {
            return new Release(m, r, (UQueue) r.getModel().getModelElement(this.releasebleQueue), waitStats, sizeStats,
                    getIdsim()+"."+getName()+".release");
        } else {
            try {
                throw new Exception("Queue "+this.releasebleQueue+" in "+getIdsim() + "." + getName() + ".queue is null");
            } catch (Exception ex) {
                Logger.getLogger(ULocal.class.getName()).log(Level.SEVERE, null, ex);
                return new Nop(m);
            }
        }
    }


    public ProcessCommand getProcessCommand(ModelElement m)
    {
        setCommandInserted(true);
        if (localTypeE == LocalType.Seize)
        {
            try {
                return getSeize(m);
            } catch (Exception ex) {
                Logger.getLogger(ULocal.class.getName()).log(Level.SEVERE, null, ex);
                ErrorMsg.displayMsg("process_command_error", getIdsim()+"."+getName(), "local", ex);
                return new Nop(m,getName());
            }
        } else if (localTypeE == LocalType.Release)
        {
            return getRelease(m);
        } else
        {
            return new Nop(m,getName());
        }
    }
}
