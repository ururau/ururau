/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;

import com.googlecode.fannj.Fann;
import com.googlecode.fannj.Layer;
import com.mxgraph.util.mxResources;
import com.sun.jna.Pointer;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import org.encog.neural.networks.BasicNetwork;
import org.ururau.ANNDecideFann;
import org.ururau.ANNDecideEncog;
import org.ururau.Template;
import org.ururau.commands.Decide.DecideType;
import org.ururau.commands.DecideByChanceNWay;
import org.ururau.commands.DecideByChanceTwoWay;
import org.ururau.commands.DecideByConditionNWay;
import org.ururau.commands.DecideByConditionTwoWay;
import org.ururau.commands.DecideByANNEncog;
import org.ururau.commands.DecideByANNFann;
import org.ururau.commands.Nop;

/**
 *
 * @author tulio
 */
public class UXor extends Template implements Serializable {

    private static int counter = 0;
    private Template templateFalse;
    HashMap<Template, String> expressionMap;
    private DecideType  xorTypeE = DecideType.TWO_WAY_BY_CHANCE;
    private int xorType = DecideType.TWO_WAY_BY_CHANCE.ordinal();
    private String show ="";

    
    private BasicNetwork encogNet = null;
    private Fann fannNet;
    private String RNA1;
    private String RNA2;
    private String RNA3;
    private String RNA4;
    private String RNA5;
    private String arquivo;
    private int Camada1;
    private int Camada2;
    private int Camada3;
    private ANNDecideEncog annEncog;
    private ANNDecideFann annFANN;
    private String numeroCamadaOculta;

    
    public UXor (boolean enableCounter)
    {
        setIdsim("XOR"+counter);
        setType(TemplateType.Xor);
        expressionMap = new HashMap<Template, String>();
        if(enableCounter)
        {
            counter++;
        }
    }

    public UXor()
    {
        setType(TemplateType.Xor);
        expressionMap = new HashMap<Template, String>();

    }
    
    public void setNumeroCamadaOculta( String numero ){
    
        this.numeroCamadaOculta = numero;
            
    }
    
    public String getNumeroCamadaOculta(){
    
        
        return this.numeroCamadaOculta;
        
    }
    
    public void setRNA1(String rna) {
        
         this.RNA1 = rna;
         
    }
    public void setRNA2(String rna) {
        
        this.RNA2 = rna;
        
    }
     public void setRNA3(String rna) {
        
         this.RNA3 = rna;
         
    }
    public void setRNA4(String rna) {
        
        this.RNA4 = rna;
        
    }
    
    public void setRNA5(String rna) {
        
        this.RNA5 = rna;
        
    }
    
    
    
    public void setCamada1(int numero) {
        
         this.Camada1 = numero;
         
    }
    
     public void setCamada2(int numero) {
        
         this.Camada2 = numero;
         
    }
     
    public void setCamada3(int numero) {
        
         this.Camada3 = numero;
         
    }
    
    //Esse metodo passa o diretorio do local aonde esta o documento com o TREINAMENTO DA REDE
    public void setArquivo(String arquivo1) {
        arquivo = arquivo1;
    }
    
    
    public String getRNA1() {
        
        return this.RNA1;
    }
    
    public String getRNA2() {
        
        return this.RNA2;
    }
    public String getRNA3() {
        
        return this.RNA3;
    }
    
    public String getRNA4() {
        
        return this.RNA4;
    }
    
    public String getRNA5() {
        
        return this.RNA5;
    }
    
    public String getArquivo(){
    
        return this.arquivo;   
     }
    
    public int getCamada1(){
    
        return this.Camada1;   
     }
    
    public int getCamada2(){
    
        return this.Camada2;   
     }
    
    public int getCamada3(){
    
        return this.Camada3;   
     }


    public Template getTemplateFalse() {
        return templateFalse;
    }

    public void setTemplateFalse(Template templateFalse) {
        this.templateFalse = templateFalse;
    }

    
    public void putExpression(Template templateTrue, String expression)
    {
        expressionMap.put(templateTrue, expression);
    }

    public String getExpression(Template templateTrue)
    {
        return expressionMap.get(templateTrue);
    }

    public void clearExpressions()
    {
        expressionMap.clear();
    }
    
    public int getXorType() {
        return xorTypeE.ordinal();
    }

    public void setXorType(int xorType) {
        DecideType x[] = DecideType.values();
        this.xorTypeE = x[xorType];
        this.xorType = xorType;
    }
    
    @Override
    public String toString()
    {
        return show;
    }


    public ProcessCommand getXor(ModelElement parent)
    {
        if (xorTypeE == DecideType.TWO_WAY_BY_CHANCE)
        {
            Iterator<Template> it = expressionMap.keySet().iterator();
            if (it.hasNext())
            {
                Template templateTrue = it.next();
                show+=getLineCodeNumber()+"   Decide prob:"+expressionMap.get(templateTrue)+" True->"+templateTrue.getLineCodeNumber()+" False->"+templateFalse.getLineCodeNumber()+" ";
                double prob = Double.parseDouble(expressionMap.get(templateTrue));
                return new DecideByChanceTwoWay(parent, prob, templateTrue.getLineCodeNumber(), templateFalse.getLineCodeNumber(), getIdsim());
            }
        }
        else if(xorTypeE == DecideType.N_WAY_BY_CHANCE)
        {
            Iterator<Template> it = expressionMap.keySet().iterator();
            double probability [] = new double[expressionMap.size()];
            int templateTrueLineCodeNumber [] = new int[expressionMap.size()];
            int i = 0;
            while(it.hasNext())
            {
                Template template = it.next();

                probability[i] = Double.parseDouble(expressionMap.get(template));
                templateTrueLineCodeNumber[i] = template.getLineCodeNumber();
                show+=getLineCodeNumber()+"   Decide prob:"+probability[i]+" True->"+templateTrueLineCodeNumber[i]+" False->"+templateFalse.getLineCodeNumber()+" ";
                i++;
            }
            return new DecideByChanceNWay(parent, probability, templateTrueLineCodeNumber, templateFalse.getLineCodeNumber(), getIdsim());
        }
        else if (xorTypeE == DecideType.TWO_WAY_BY_CONDITION)
        {
            Iterator<Template> it = expressionMap.keySet().iterator();
            if (it.hasNext())
            {
                Template templateTrue = it.next();
                show+=getLineCodeNumber()+"   Decide expr:"+expressionMap.get(templateTrue)+" True->"+templateTrue.getLineCodeNumber()+" False->"+templateFalse.getLineCodeNumber()+" ";
                return new DecideByConditionTwoWay(parent, expressionMap.get(templateTrue), templateTrue.getLineCodeNumber(), templateFalse.getLineCodeNumber(), getIdsim());
            } 
        } else if (xorTypeE == DecideType.N_WAY_BY_CONDITION)
        {
            Iterator<Template> it = expressionMap.keySet().iterator();
            String expression [] = new String[expressionMap.size()];
            int templateTrueLineCodeNumber [] = new int[expressionMap.size()];
            int i = 0;
            while(it.hasNext())
            {
                Template template = it.next();
                expression[i] = expressionMap.get(template);
                templateTrueLineCodeNumber[i] = template.getLineCodeNumber();
                show+=getLineCodeNumber()+"   Decide expr:"+expression[i]+" True->"+templateTrueLineCodeNumber[i]+" False->"+templateFalse.getLineCodeNumber()+" ";
                i++;
            }
            return new DecideByConditionNWay(parent, expression, templateTrueLineCodeNumber, templateFalse.getLineCodeNumber(), getIdsim());            
        
        //editado em 11/02/2014 by Jhonathan Camacho
        }else{
        
            if (xorTypeE == DecideType.TWO_WAY_BY_RNA){
                
                Map<String, Integer> templatesMaps = new HashMap<String, Integer>();    

                Iterator<Template> it = expressionMap.keySet().iterator();

                if (USimulation.getInstance().getAnnLibraryType() == USimulation.ANNType.ENCOG.ordinal())
                {
                    if( encogNet == null ){

                        //Chama o treinamento do decide antes da criação do mesmo

                        this.annEncog = new ANNDecideEncog();
                        this.encogNet= annEncog.Treinamento( this.arquivo, this.Camada1,this.Camada2,this.Camada3); 
                        //this.net.Treinamento( this.arquivo );
                    }
                
                } else if (USimulation.getInstance().getAnnLibraryType() == USimulation.ANNType.FANN.ordinal())
                {
                    if (fannNet == null)
                    {
                        this.annFANN = new ANNDecideFann();
                        try {
                            this.fannNet = annFANN.Treinamento( this.arquivo, this.Camada1,this.Camada2,this.Camada3);
                        } catch (IOException ex) {
                            javax.swing.JDialog f=new javax.swing.JDialog();  
                            f.setSize(250,150);
                            javax.swing.JOptionPane.showMessageDialog(f,mxResources.get("trainingFileNotFound"),mxResources.get("warning"),javax.swing.JOptionPane.WARNING_MESSAGE);  
                            Logger.getLogger(UXor.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            javax.swing.JDialog f=new javax.swing.JDialog();  
                            f.setSize(250,150);
                           javax.swing.JOptionPane.showMessageDialog(f,mxResources.get("inputLayerNotMatchesWithTrainingFile")+" "+this.Camada1,
                                   mxResources.get("warning"),javax.swing.JOptionPane.WARNING_MESSAGE);  
                            Logger.getLogger(UXor.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    
                }
                
                while(it.hasNext()){


                    Template templateTrue = it.next();

                    /*
                     * peguei o valor que identifica cada template na INTERFACE, ( Esse valor é dado pelo usuario na tela )
                     * e associei a o valor do 
                     * template que foi dado na construção do graph ( O valor é dado no GraphEditor ).
                    */
                    templatesMaps.put(this.expressionMap.get(templateTrue), templateTrue.getLineCodeNumber());

                }


               List<String> aux = new ArrayList<String>();
               aux.addAll(templatesMaps.keySet());  
               Collections.sort(aux);

               for( int i = 0; i < aux.size(); i++ ){

                   if( !aux.get(i).equals(String.valueOf(i + 1)) ){

                       throw  new IllegalArgumentException(mxResources.get("illegalArgument")+aux.get(i)+" "+String.valueOf(i + 1));
                   }else{

                       //esse else tem que ser removido
                    System.out.println("FOI:" + aux.get(i));

                   }
               }

               /*
                * a divisão de 1(um) pela quantidade de caminhos é somado com a divisão de
                * 0,1 dividido também pela quntidade de caminhos, porque no resultado da RNA pode sair 0.0.
                * Para ficar dividido igualmente, é necessario dar um valor a "zero" -> 0,1 e dividi-lo também.
                */ 
             //double umDivididoPorListSize = ( 1.0 / aux.size() ) + ( 0.1 / aux.size());
                
              double umDivididoPorListSize = ( 1.0 / aux.size() );


               List<Double> acumuladorDeValores = new ArrayList<Double>();

               //pega o primeiro valor e insere no array
               acumuladorDeValores.add(umDivididoPorListSize);

               //tem que começar de 1 por que o 1º já foi inserido na posicao 0 (zero)
               for( int i = 1; i < aux.size(); i++ ){

                   double acumulador = umDivididoPorListSize + acumuladorDeValores.get(i - 1);

                   acumuladorDeValores.add(acumulador);

               }

                //Tem que passar o acumulador para ele.
               if (USimulation.getInstance().getAnnLibraryType() == USimulation.ANNType.ENCOG.ordinal())
               {
                  return new DecideByANNEncog(parent, this.RNA1, this.RNA2,this.RNA3, this.RNA4,this.RNA5, templatesMaps, getIdsim(), this.encogNet, acumuladorDeValores, this.annEncog, this.numeroCamadaOculta, this.Camada1);
               }  else {
                  return new DecideByANNFann(parent, this.RNA1, this.RNA2,this.RNA3, this.RNA4,this.RNA5, templatesMaps, getIdsim(), this.fannNet, acumuladorDeValores, this.annFANN, this.numeroCamadaOculta, this.Camada1); 
               }
            }
        
        
        
        }
        return new Nop(parent);
        
    }

    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        setCommandInserted(true);
        return getXor(parent);
    }


}