/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.modules;

import com.mxgraph.util.mxResources;
import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import org.ururau.Template;
import org.ururau.commands.Separate;

/**
 *
 * @author tulio
 */
public class USeparate extends Template implements Serializable  {
    private static final long serialVersionUID = 7526471110122711147L;
    private static int count = 0;
    
    public USeparate(boolean enableCounter)
    {
        setIdsim("S"+count);
        setName(mxResources.get("separate")+count);
        setType(TemplateType.Separate);
        if (enableCounter)
        {
            count++;
        }
    }

    public USeparate()
    {
        setType(TemplateType.Separate);
    }
    
    @Override
    public String toString() {
        return getLineCodeNumber()+" "+getType().toString()+" "+getIdsim();
    }

    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        setCommandInserted(true);
        return new Separate(parent, getIdsim()+"."+getName());
    }
    
}
