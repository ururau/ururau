/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import org.ururau.ErrorMsg;
import org.ururau.Template;
import org.ururau.commands.JumpTo;
import org.ururau.commands.Nop;

/**
 *
 * @author tulio
 */
public class UTerminate extends Template implements Serializable {

    private static final long serialVersionUID = 7516471155122776147L;
    private static int count = 0;
    private int lastLine = -1;
    
    public UTerminate(boolean enableCounter)
    {
        super("T"+count, "");
        setType(TemplateType.Terminate);
        if (enableCounter)
        {
            count++;
        }
    }

    public UTerminate()
    {
        setType(TemplateType.Terminate);
    }

    public void setLastLine(int lastLine)
    {
        this.lastLine = lastLine;
    }

    public int getLastLine()
    {
        return lastLine;
    }

    public String toString() {
        return getLineCodeNumber()+" "+getIdsim()+" jump to: "+(lastLine+1);
    }
    
    public ProcessCommand getTerminate(ModelElement m) throws Exception {
        return new JumpTo(m,lastLine+1);
    }

    public ProcessCommand getProcessCommand(ModelElement m)
    {
        setCommandInserted(true);
        try {
            return getTerminate(m);
        } catch (Exception ex) {
            Logger.getLogger(UTerminate.class.getName()).log(Level.SEVERE, null, ex);
            ErrorMsg.displayMsg("process_command_error", getIdsim(), "terminate", ex);
            return new Nop(m,getName());
        }
    }
}
