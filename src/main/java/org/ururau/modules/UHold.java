/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.modules;

import com.mxgraph.util.mxResources;
import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.queue.Queue;
import org.ururau.Template;
import org.ururau.commands.Hold;

/**
 *
 * @author tulio
 */
public class UHold extends Template implements Serializable {
    
    private static int count = 0;
    private static final long serialVersionUID = 7526471111122711147L;

    private String expression;
    public UHold(boolean enableCounter)
    {
        setIdsim("L"+count);
        setName(mxResources.get("hold")+count);
        setType(TemplateType.Hold);
        if (enableCounter)
        {
            count++;
        }
    }

    public UHold()
    {
        setType(TemplateType.Hold);
    }
    
    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }    
    
    public String getLocalQueue() {
        return getIdsim()+"."+getName()+".queue";
    }
        
    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        Queue q = new Queue(parent, getLocalQueue());
        return new Hold(parent, q, expression);
    }
    
}
