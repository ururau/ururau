/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.modules;


import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import com.mxgraph.util.mxResources;
import org.ururau.Template;
import org.ururau.commands.AssignAttribute;
import org.ururau.commands.AssignVariable;
import org.ururau.commands.CommunicationTagWriterBoolean;
import org.ururau.commands.CommunicationTagWriterInterger;
import org.ururau.commands.CommunicationTagWriterReal;
import org.ururau.commands.MyEmissions;
import org.ururau.commands.Nop;
/**
 *
 * @author Eder & Fabio
 */
public class UEmissions extends Template implements Serializable {

    private static final long serialVersionUID = 7526471110122716147L;
    private static int count = 0;
    private String attributeName=  "atrib1";
    private String variableName = "var1";
    private String tagName = "tag1";
    private String value = "";
    private double ec = 1;
    private double power =1;
    private String time ="0";
    private double load=0;
    private double pbt = 0;
    private double percentage = 100;

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }
    
     public String getVariableName() {   
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }
    public double getEc (){
        return ec;
    }
    public void setEc(double ec){
        this.ec = ec;
    }
    public double getPower(){
        return power;
    }
    public void setPower(double power){
        this.power = power;
    }
    public String getTime(){
        return time;
    }
    public void setTime(String time){
        this.time = time;
    }
    public double getLoad(){
        return load;
    }
    public void setLoad(double load){
        this.load = load;
    }
    public double getPbt(){
        return pbt;
}
    public void setPbt(double pbt){
        this.pbt = pbt;
    }
    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

public UEmissions(boolean enableCounter)
    {
        setIdsim("C"+count);
        setName(mxResources.get("assignment")+count);
        setType(Template.TemplateType.MyEmissions);
        if (enableCounter)
        {
            count++;
        }
    }

    public UEmissions()
    {
        setType(Template.TemplateType.MyEmissions);
    }
   
    @Override
    public String toString() {
        return getLineCodeNumber()+" "+getType().toString()+" "+getIdsim();
    }
    
    @Override
    public ProcessCommand getProcessCommand(ModelElement parent) {
        setCommandInserted(true);
        return new MyEmissions(parent, variableName, ec, power, time,load,pbt,percentage, getIdsim()+"."+getName());
    }
    
}