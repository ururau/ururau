/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau.modules;

import com.mxgraph.util.mxResources;
import java.io.Serializable;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessCommand;
import jsl.modeling.elements.resource.Resource;
import org.ururau.Template;
import org.ururau.commands.Nop;

/**
 *
 * @author tulio
 */
public class UResource extends Template implements Serializable {

    private static final long serialVersionUID = 7526471112122476141L;
    private static int count = 0;
    private int capacity = 1;

    public long getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        if (capacity > 0)
            this.capacity = capacity;
    }

    public UResource()
    {
        setType(TemplateType.Resource);
    }
    
    public UResource(boolean enableCounter)
    {
        super("R"+count, mxResources.get("resource")+count);
        setType(TemplateType.Resource);
        if (enableCounter)
        {
            count++;
        }
    }

    public Resource getResource(ModelElement m)
    {
        ModelElement r = m.getModel().getModelElement(getIdsim()+"."+getName());
        if (r != null)
        {
            if (r instanceof Resource)
            {
                return (Resource) r;
            }
        } 
        return new Resource(m, capacity, getIdsim()+"."+getName());
    }

    public ProcessCommand getProcessCommand(ModelElement m)
    {
        return new Nop(m,getName());
    }
}
