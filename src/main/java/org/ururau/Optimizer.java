/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau;

import java.io.Serializable;
import java.util.ArrayList;
import jsl.modeling.elements.variable.Variable;
import org.ururau.modules.UModel;
import org.ururau.variable.CustomVariable;

/**
 *
 * @author tulio
 */
public class Optimizer implements Serializable {
    ArrayList<GAVariable> gaVariables = new ArrayList<GAVariable>();
    private static Optimizer instance = null;
    private String objective;
    private boolean minimizing;
    
    public Optimizer() {
    }

    public ArrayList<GAVariable> getGaVariables() {
        return gaVariables;
    }

    public void setGaVariables(ArrayList<GAVariable> gaVariables) {
        this.gaVariables = gaVariables;
    }
    
    public static Optimizer getInstance()
    {
        if (Optimizer.instance == null)
            Optimizer.instance = new Optimizer();
        return Optimizer.instance;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public boolean isMinimizing() {
        return minimizing;
    }

    public void setMinimizing(boolean minimizing) {
        this.minimizing = minimizing;
    }
    
    
    public void addVariablesToModel(UModel model) {
        if (model != null)
        {
            for (int i = 0 ; i < gaVariables.size(); i++)
            {
                if (model.getModel().getModelElement(gaVariables.get(i).getName()) == null)
                {
                    new Variable(model.getModel(),gaVariables.get(i).getName());
                    new CustomVariable(model.getModel(), gaVariables.get(i).getName()+".RespVar").setDefaultReportingOption(true);
                } else {
                    String name = gaVariables.get(i).getName();
                    if (name.matches("R[0-9].*"))
                    {
                        new CustomVariable(model.getModel(), gaVariables.get(i).getName()+".RespVar").setDefaultReportingOption(true);
                    }
                }
            }
        }
    }
}
