/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ururau.variable;

import jsl.modeling.ModelElement;
import jsl.modeling.elements.variable.AcrossReplicationStatisticIfc;
import jsl.modeling.elements.variable.DefaultReportingOptionIfc;
import jsl.modeling.elements.variable.Variable;
import jsl.utilities.statistic.StatisticAccessorIfc;
import org.ururau.observers.CustomVariableObserver;

/**
 *
 * @author tulio
 */
public class CustomVariable extends Variable implements AcrossReplicationStatisticIfc,  DefaultReportingOptionIfc {
    protected double myTotalDuringTimedUpdate;
    protected CustomVariableObserver myCustomVariableObserver;
    protected boolean myDefaultReportingOption = true;
    
    public CustomVariable(ModelElement parent)
    {
        super(parent, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, null);
        myCustomVariableObserver = new CustomVariableObserver(getName());
	addObserver(myCustomVariableObserver);  
    }
    
    public CustomVariable(ModelElement parent, double initialValue) {
        super(parent, initialValue, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, null);
        myCustomVariableObserver = new CustomVariableObserver(getName());
	addObserver(myCustomVariableObserver);  

    }
    
    public CustomVariable(ModelElement parent, String name) {
        super(parent, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, name);
        myCustomVariableObserver = new CustomVariableObserver(getName());
	addObserver(myCustomVariableObserver);  

    }
    
    public CustomVariable(ModelElement parent, double initialValue, String name) {
        super(parent, initialValue, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, name);
        myCustomVariableObserver = new CustomVariableObserver(getName());
	addObserver(myCustomVariableObserver);  
    }
    
    @Override
    protected void timedUpdate() {
        myTotalDuringTimedUpdate = this.getValue() ;// - this.getPreviousValue();
    }
    
    public final double getTotalDuringTimedUpdate() {
        return myTotalDuringTimedUpdate;
    }

    public final CustomVariableObserver getCounterObserver(){
        return(myCustomVariableObserver);
    }

    public StatisticAccessorIfc getAcrossReplicationStatistic() {
        return (myCustomVariableObserver.getAcrossReplicationStatistic());
    }

    public double getAcrossReplicationAverage() {
        return(myCustomVariableObserver.getAcrossReplicationStatistic().getAverage());
    }

    public void setAcrossReplicationStatisticName(String name) {
        myCustomVariableObserver.setAcrossReplicationStatisticName(name);
    }
    
    @Override
    protected void removedFromModel() {
        super.removeFromModel();
        myCustomVariableObserver = null ;
    }

    public void setDefaultReportingOption(boolean flag) {
        myDefaultReportingOption = flag;
    }

    public boolean getDefaultReportingOption() {
        return myDefaultReportingOption;
    }

}
