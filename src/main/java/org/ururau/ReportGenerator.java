/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ururau;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.mxgraph.util.mxResources;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

//import org.ururau.commands.Process;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import jsl.modeling.Experiment;
import jsl.modeling.Model;
import jsl.modeling.ModelElement;
import jsl.modeling.elements.processview.description.ProcessDescription;
import jsl.modeling.elements.queue.UQueue;
import jsl.modeling.elements.resource.Resource;
import jsl.modeling.elements.variable.Counter;
import jsl.modeling.elements.variable.RandomVariable;
import jsl.modeling.elements.variable.Variable;
import jsl.utilities.Responses;
import jsl.utilities.random.distributions.Constant;
import jsl.utilities.random.distributions.DistributionIfc;
import jsl.utilities.random.distributions.Normal;
import jsl.utilities.statistic.AbstractStatistic;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.ururau.commands.AssignVariable;
import org.ururau.commands.Delay;
import org.ururau.commands.DelayVariable;
import org.ururau.commands.JumpTo;
import org.ururau.commands.Process;
import org.ururau.commands.Record;
import org.ururau.commands.Record.RecordType;
import org.ururau.commands.RecordCounter;
import org.ururau.commands.Terminate;
import org.ururau.description.EntityProcessGenerator;
import org.ururau.variable.CustomVariable;
import com.itextpdf.awt.DefaultFontMapper;

/**
 *
 * @author tulio
 */
public class ReportGenerator {
    /** Path to the resulting PDF file. */
    public static final String RESULT
        = "hello.pdf";
    public static boolean enableShowPdfReport = true;
    
    public static void turnOnShowPdfReport()
    {
        ReportGenerator.enableShowPdfReport = true;
    }
    
    public static void turnOffShowPdfReport()
    {
        ReportGenerator.enableShowPdfReport = false;
    }
    
    public ReportGenerator() {
    }

    /**
     * Creates a PDF file: hello.pdf
     * @param    args    no arguments needed
     */
    public static void main(String[] args)
    	throws DocumentException, IOException {
        try
		{
            Locale localeBR = new Locale("pt","BR");
            mxResources.add("i18n.editor", localeBR);
		}
		catch (Exception e)
		{
			// ignore
		}

        Experiment sim = simulation();

    	ReportGenerator r = new ReportGenerator();
        r.createPdf(RESULT.replace(" ", "_"), sim);
        r.showReport(RESULT.replace(" ", "_"));
    }

    public void showReport(String file)
            throws IOException
    {
        if (enableShowPdfReport)
        try {
            if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0)
            {
                String command = "cmd /c "+file;
                java.lang.Process child = Runtime.getRuntime().exec(command);
            } if (System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0) {
                String command = "open -a Preview.app "+file;
                java.lang.Process child = Runtime.getRuntime().exec(command);
            }
            else
            {
                String command = "/usr/bin/evince "+file;
                java.lang.Process child = Runtime.getRuntime().exec(command);
            }
        } catch (IOException e) {
        }
    }


    public static Experiment simulation()
          {
		
		Model m = buildModel();

		// create the experiment to run the model
		Experiment e = new Experiment(m);

		// set the parameters of the experiment
		e.setNumberOfReplications(100);
		e.setLengthOfReplication(3600.0);
		e.setLengthOfWarmUp(0);

		// turn on the desired reporting
		e.turnOnExperimentReport();
                e.setSaveExperimentStatisticsOption(true);
                
		// tell the experiment to run
		e.runAll();
		System.out.println("Feito!");

        return e;


	}

    public static Model buildModel(){
    // create the containing model
    Model m = Model.createModel();
    //RandomVariable processAtendimento = new RandomVariable(m,new Normal(5, 1),"Process Atendimento");
    DistributionIfc tecCliente = new Constant(10);
    DistributionIfc primChegada = new Constant(0);

    RandomVariable processCarregamento = new RandomVariable(m, new Normal(40,16), "carregamento");
    RandomVariable processTransporteIda = new RandomVariable(m, new Constant(60), "ida");
    RandomVariable processDescarregamento = new RandomVariable(m, new Normal(15,4), "descarregamento");
    RandomVariable processTransporteVolta = new RandomVariable(m, new Constant(60), "volta");
    Resource r1 = new Resource(m, 1, "maq frente");
    Resource r2 = new Resource(m, 1, "maq usina");
    UQueue q1 = new UQueue(m, "Fila carregamento");
    UQueue q2 = new UQueue(m, "Fila descarregamento");

    ProcessDescription cana = new ProcessDescription(m, "Cana");
    Variable um = new Variable(m, 1);
    Variable v1 = new Variable(m, 1, "maq requesitadas");
    new EntityProcessGenerator(m, cana, primChegada, tecCliente, 200, "cana");
    cana.addProcessCommand(new Process(m, v1, r1, q1, processCarregamento));
    cana.addProcessCommand(new DelayVariable(m,processTransporteIda));
    cana.addProcessCommand(new Process(m, v1, r2, q2, processDescarregamento));
    cana.addProcessCommand(new DelayVariable(m,processTransporteVolta));
    //cana.addProcessCommand(new Record(m, um, RecordType.COUNTER,"conta viagens"));
    cana.addProcessCommand(new JumpTo(m, 0, "Volta"));
    cana.addProcessCommand(new Terminate(m,true,"caminhão"));
    return m;

    }

    /**
     * Creates a PDF document.
     * @param filename the path to the new PDF document
     * @throws    DocumentException
     * @throws    IOException
     */
    
    public void createPdf(String filename, Experiment e)
	throws DocumentException, IOException {
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
        document.open();


        String report = "";
        report = mxResources.get("experiment")+": "+e.getName()+"\n";
        report += mxResources.get("numberOfReplications")+": "+e.getNumberOfReplications()+"\n";
        report += mxResources.get("lengthOfReplication")+": "+e.getLengthOfReplication()+"\n";
        report += mxResources.get("lengthOfWarmUp")+": "+e.getLengthOfWarmUp()+"\n";
        report += "\n\n\n";

        Paragraph title = new Paragraph(mxResources.get("experimentParameters"),
                FontFactory.getFont(FontFactory.HELVETICA, "iso-8859-1", true, 18, Font.BOLD,new BaseColor(0,0,255)));
        document.add(title);
        document.add(new Paragraph(report));

        List<AbstractStatistic> acrossReplicationStatistics = e.getAcrossReplicationStatistics();
        
        // Variáveis do usuário
        title = new Paragraph(mxResources.get("userVariables"),
                FontFactory.getFont(FontFactory.HELVETICA, "iso-8859-1", true, 18, Font.BOLD,new BaseColor(0,0,255)));
        document.add(title);
        
        String recordVarHeader[] = {mxResources.get("responseVariables"), mxResources.get("average"),mxResources.get("standardDeviation"),mxResources.get("semiInterval"),mxResources.get("minimum"),mxResources.get("maximum")};      
                    
        
        Iterator<AbstractStatistic> iterator = acrossReplicationStatistics.iterator();
        int j = 0;
        while(iterator.hasNext())
        {
            AbstractStatistic st = iterator.next();
            if (st.getName().endsWith("RecordRespVar"))
            {
                if (j == 0)
                {
                    document.add(makeSubItemHeader(recordVarHeader));
                }
                String str = st.getName().replaceAll("Across Rep Stat ", "");
                str = str.replaceAll(".RecordRespVar", "");
                String line[] = {str,
                String.format("%.3f", st.getAverage()),
                String.format("%.3f", st.getStandardDeviation()),
                String.format("%.3f", st.getHalfWidth()),
                String.format("%.3f", st.getMin()),
                String.format("%.3f", st.getMax())};
                document.add(makeSubItem(line));
            }
            j++;
        }
        

        String recordNoRespVarHeader[] = {mxResources.get("otherVariables"), mxResources.get("average"),mxResources.get("standardDeviation"),mxResources.get("semiInterval"),mxResources.get("minimum"),mxResources.get("maximum")};
        List<Variable> v = e.getModel().getVariables();
        if (v.size() > 0)
            document.add(makeSubItemHeader(recordNoRespVarHeader));
        
        Iterator<ModelElement> meIterator = e.getModel().getModelElementIterator();
        while(meIterator.hasNext())
        {
            ModelElement me = meIterator.next(); 
            if (me.getName().endsWith("RespVar") && me instanceof CustomVariable)
            {
                CustomVariable customVar = (CustomVariable) me;
                AbstractStatistic st = (AbstractStatistic) customVar.getAcrossReplicationStatistic();
                String str = st.getName().replaceAll("Across Rep Stat ", "");
                str = str.replaceAll(".RespVar", "");
                String line[] = {str,
                String.format("%.3f", st.getAverage()),
                String.format("%.3f", st.getStandardDeviation()),
                String.format("%.3f", st.getHalfWidth()),
                String.format("%.3f", st.getMin()),
                String.format("%.3f", st.getMax())};
                document.add(makeSubItem(line));
            }
        }

        
        String recordCountVarHeader[] = {mxResources.get("counters"), mxResources.get("average"),mxResources.get("standardDeviation"),mxResources.get("semiInterval"),mxResources.get("minimum"),mxResources.get("maximum")};
        
        List<Counter> c = e.getModel().getCounters();
        if (c.size() > 0)
            document.add(makeSubItemHeader(recordCountVarHeader));
        
        j = 0;
        while(j < c.size())
        {
            AbstractStatistic st = (AbstractStatistic) c.get(j).getAcrossReplicationStatistic();
            if (st.getName().endsWith("RecordCounterVar"))
            {
                
                String str = st.getName().replaceAll("Across Rep Stat ", "");
                str = str.replaceAll(".RecordCounterVar", "");
                String line[] = {str,
                String.format("%.3f", st.getAverage()),
                String.format("%.3f", st.getStandardDeviation()),
                String.format("%.3f", st.getHalfWidth()),
                String.format("%.3f", st.getMin()),
                String.format("%.3f", st.getMax())};
                document.add(makeSubItem(line));
            }
            j++;
        }

        // Entity
        title = new Paragraph(mxResources.get("entity"),
                FontFactory.getFont(FontFactory.HELVETICA, "UTF-8", true, 18, Font.BOLD,new BaseColor(0,0,255)));
        document.add(title);

        // Flow Time

        String flowTimeHeader[] = {mxResources.get("flowTime"),  mxResources.get("average"),mxResources.get("standardDeviation"),mxResources.get("semiInterval"),mxResources.get("minimum"),mxResources.get("maximum")};
  
        document.add(makeSubItemHeader(flowTimeHeader));
       
        iterator = acrossReplicationStatistics.iterator();
        while(iterator.hasNext())
        {
            AbstractStatistic st = iterator.next();
            if (st.getName().endsWith("Entity Flow Time"))
            {
                String str = st.getName().replaceAll("Across Rep Stat ", "");
                str = str.replaceAll(" Entity Flow Time", "");
                if (st.getSum()>0)
                {
                    String line[] = {str, 
                    String.format("%.3f", st.getAverage()),
                    String.format("%.3f", st.getStandardDeviation()),
                    String.format("%.3f", st.getHalfWidth()),
                    String.format("%.3f", st.getMin()),
                    String.format("%.3f", st.getMax())};
                   document.add(makeSubItem(line));
                }
                else
                {
                   String line[] = {str, "0","","",""};
                   document.add(makeSubItem(line));
                }
            }
        }

        // Queue

        title = new Paragraph(mxResources.get("queue"),
                FontFactory.getFont(FontFactory.HELVETICA, "UTF-8", true, 18, Font.BOLD,new BaseColor(0,0,255)));
        document.add(title);

        // Time in Queue
        String TimeInQueueHeader[] = {mxResources.get("timeInQueue"), mxResources.get("average"),mxResources.get("standardDeviation"),mxResources.get("semiInterval"),mxResources.get("minimum"),mxResources.get("maximum")};
        document.add(makeSubItemHeader(TimeInQueueHeader));

        iterator = acrossReplicationStatistics.iterator();
        while(iterator.hasNext())
        {
            AbstractStatistic st = iterator.next();
            if (st.getName().endsWith("TimeInQ"))
            {
                String str = st.getName().replaceAll("Across Rep Stat ", "");
                str = str.replaceAll(" TimeInQ", "");

                String line[] = {str, 
                String.format("%.3f", st.getAverage()),
                String.format("%.3f", st.getStandardDeviation()),
                String.format("%.3f", st.getHalfWidth()),
                String.format("%.3f", st.getMin()),
                String.format("%.3f", st.getMax())};
                document.add(makeSubItem(line));
                /*
                document.add(new Paragraph(str+" "+
                        st.getAverage()+" "+
                        st.getHalfWidth()+" "+
                        st.getMin()+" "+
                        st.getMax()+" "+
                        "\n"));
                 *
                 */
            }
        }

        // Number in Queue

        String NumberInQueueHeader[] = {mxResources.get("numberInQueue"), mxResources.get("average"),mxResources.get("standardDeviation"),mxResources.get("semiInterval"),mxResources.get("minimum"),mxResources.get("maximum")};
        document.add(makeSubItemHeader(NumberInQueueHeader));

        iterator = acrossReplicationStatistics.iterator();
        while(iterator.hasNext())
        {
            AbstractStatistic st = iterator.next();
            if (st.getName().endsWith("NumberInQ"))
            {
                String str = st.getName().replaceAll("Across Rep Stat ", "");
                str = str.replaceAll(" NumberInQ", "");
                String line[] = {str, 
                String.format("%.3f", st.getAverage()),
                String.format("%.3f", st.getStandardDeviation()),
                String.format("%.3f",st.getHalfWidth()),
                String.format("%.3f",st.getMin()),
                String.format("%.3f",st.getMax())};
                document.add(makeSubItem(line));
            }
        }

        // Resource
        title = new Paragraph(mxResources.get("resource"),
                FontFactory.getFont(FontFactory.HELVETICA, "UTF-8", true, 18, Font.BOLD,new BaseColor(0,0,255)));
        document.add(title);

        // Ocupados
        String numberBusy[] = {mxResources.get("busy"), mxResources.get("average"),mxResources.get("standardDeviation"),mxResources.get("semiInterval"),mxResources.get("minimum"),mxResources.get("maximum")};
        document.add(makeSubItemHeader(numberBusy));

        ArrayList<AbstractStatistic> numberBusyStat = new ArrayList();
        iterator = acrossReplicationStatistics.iterator();
        while(iterator.hasNext())
        {
            AbstractStatistic st = iterator.next();
            if (st.getName().endsWith("Number Busy Units"))
            {
                numberBusyStat.add(st);
                String str = st.getName().replaceAll("Across Rep Stat ", "");
                str = str.replaceAll(" Number Busy Units", "");
                String line[] = {str, 
                String.format("%.3f",st.getAverage()),
                String.format("%.3f", st.getStandardDeviation()),
                String.format("%.3f",st.getHalfWidth()),
                String.format("%.3f",st.getMin()),
                String.format("%.3f",st.getMax())};
                document.add(makeSubItem(line));
            } 
        }

        // Number Idle

        String numberIdle[] = {mxResources.get("numberIdle"), mxResources.get("average"),mxResources.get("standardDeviation"),mxResources.get("semiInterval"),mxResources.get("minimum"),mxResources.get("maximum")};
        document.add(makeSubItemHeader(numberIdle));

        ArrayList<AbstractStatistic> numberIdleStat = new ArrayList();
        iterator = acrossReplicationStatistics.iterator();
        while(iterator.hasNext())
        {
            AbstractStatistic st = iterator.next();
            if (st.getName().endsWith("Number Idle Units"))
            {
                numberIdleStat.add(st);
                String str = st.getName().replaceAll("Across Rep Stat ", "");
                str = str.replaceAll(" Number Idle Units", "");
                String line[] = {str, 
                String.format("%.3f",st.getAverage()),
                String.format("%.3f", st.getStandardDeviation()),
                String.format("%.3f",st.getHalfWidth()),
                String.format("%.3f",st.getMin()),
                String.format("%.3f",st.getMax())};
                document.add(makeSubItem(line));
            }
        }

        // Number Scheduled
        String NumberScheduled[] = {mxResources.get("numberScheduled"), mxResources.get("average"),mxResources.get("standardDeviation"),mxResources.get("semiInterval"),mxResources.get("minimum"),mxResources.get("maximum")};
        document.add(makeSubItemHeader(NumberScheduled));

        int i = 0;
        while(i < numberBusyStat.size())
        {
            AbstractStatistic busyStat = numberBusyStat.get(i);
            AbstractStatistic idleStat = numberIdleStat.get(i);

            String str = busyStat.getName().replaceAll("Across Rep Stat ", "");
            str = str.replaceAll(" Number Busy Units", "");
            double avr = busyStat.getAverage() + idleStat.getAverage();
            double sd = busyStat.getAverage() + idleStat.getStandardDeviation();
            double hw = busyStat.getHalfWidth() + idleStat.getHalfWidth();
            double min = busyStat.getAverage() + idleStat.getAverage();
            double max = busyStat.getAverage() + idleStat.getAverage() ;
            String line[] = {str, 
            String.format("%.0f", avr),
            String.format("%.0f", sd),
            String.format("%.0f",hw),
            String.format("%.0f",min),
            String.format("%.0f",max)};
            document.add(makeSubItem(line));
            i++;
        }

        // Instantaneous Utilization
       // document.newPage();

        String instantaneousUtilization[] = {mxResources.get("utilization"), mxResources.get("average"),mxResources.get("standardDeviation"),mxResources.get("semiInterval"),mxResources.get("minimum"),mxResources.get("maximum")};
        document.add(makeSubItemHeader(instantaneousUtilization));

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        i = 0;
        while(i < numberBusyStat.size())
        {
            AbstractStatistic busyStat = numberBusyStat.get(i);
            AbstractStatistic idleStat = numberIdleStat.get(i);

            String str = busyStat.getName().replaceAll("Across Rep Stat ", "");
            str = str.replaceAll(" Number Busy Units", "");
            double avr = busyStat.getAverage() / (busyStat.getAverage() + idleStat.getAverage());
            double sd = busyStat.getStandardDeviation();
            double hw = busyStat.getHalfWidth();
            double min = busyStat.getMin() / (busyStat.getMin() + idleStat.getMin());
            double max = busyStat.getMax() / (busyStat.getMax() + idleStat.getMax());
            String line[] = {str, 
                String.format("%.3f",avr),
                String.format("%.3f",sd),
                String.format("%.3f",hw),
                String.format("%.3f",min),
                String.format("%.3f",max)};
            document.add(makeSubItem(line));
            dataset.addValue(avr, str,"");
            i++;
        }

        JFreeChart chart = ChartFactory.createBarChart(
            mxResources.get("utilization"), // chart title
            mxResources.get("resource"), // domain axis label
            mxResources.get("value"), // range axis label
            dataset, // data
            PlotOrientation.VERTICAL, // orientation
            true, // include legend
            true, // tooltips?
            false // URLs?
        );


        int width = 500;
        int height = 300;

        if (numberBusyStat.size() > 1)
        {
            PdfContentByte cb = writer.getDirectContent();
            PdfTemplate tp = cb.createTemplate(width, height);
            Graphics2D g2 = tp.createGraphics(width, height, new DefaultFontMapper());
            Rectangle2D r2D = new Rectangle2D.Double(0, 0, width, height);
            chart.draw(g2, r2D);
            g2.dispose();

            Image image = Image.getInstance(tp);

            document.add(image);
        }
        document.close();
    }

    private PdfPTable makeSubItemHeader(String content[])
    {
        float columms[] = {90,45,45,50,40,50};
        PdfPTable table =new PdfPTable(columms);
        table.setTotalWidth(1200);
        table.setHorizontalAlignment(table.ALIGN_LEFT);
        PdfPCell cell ;
        for(int i = 0; i < content.length; i++)
        {
            if (i == 0)
            {
                cell = new PdfPCell(new Phrase(content[i],
                        FontFactory.getFont(FontFactory.HELVETICA, "ISO-8859-1", true, 14, Font.BOLD, new BaseColor(Color.BLUE.getRGB()))));
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell);
            } else
            {
                cell = new PdfPCell(new Phrase(content[i],
                        FontFactory.getFont(FontFactory.HELVETICA, "ISO-8859-1", true, 12, Font.NORMAL, new BaseColor(Color.BLUE.getRGB()))));
                cell.setBorder(Rectangle.NO_BORDER);
                table.addCell(cell);
            }
        }
        return table;
    }

    private PdfPTable makeSubItem(String content[])
    {
        float columms[] = {90,45,45,50,40,50};
        PdfPTable table =new PdfPTable(columms);
        table.setTotalWidth(1200);
        table.setHorizontalAlignment(table.ALIGN_LEFT);
        PdfPCell cell ;
        for(int i = 0; i < content.length; i++)
        {
            cell = new PdfPCell(new Phrase(content[i]));
            cell.setBorder(Rectangle.NO_BORDER);
            table.addCell(cell);
        }
        return table;
    }

}
