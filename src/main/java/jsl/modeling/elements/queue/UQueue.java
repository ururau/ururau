/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsl.modeling.elements.queue;

import jsl.modeling.ModelElement;
import static jsl.modeling.ModelElement.getTime;
import jsl.modeling.elements.queue.QObject;
import jsl.modeling.elements.queue.Queue;
import jsl.modeling.elements.queue.QueueDiscipline;

/**
 *
 * @author tulio
 */
public class UQueue extends Queue {
    
    public UQueue(ModelElement parent, String name) {
        super(parent, name, FIFO, false);
    }
    
    public UQueue(ModelElement parent, String name, QueueDiscipline discipline) {
        super(parent, name, discipline, false);
    }
    
    public UQueue(ModelElement parent, String name, QueueDiscipline discipline, boolean mapTrackingFlag) {
        super(parent, name, discipline, mapTrackingFlag);
    }
    
    public QObject remove(int index, boolean waitStats, boolean sizeStatss){
        QObject qObj = (QObject)myList.remove(index);
        if (getEntityTrackingOption() == true){
            Object obj = qObj.getQueuedObject();
            myQObjectMap.remove(obj);
        }
        int priority = qObj.getPriority();
                
        qObj.exitQueue();
        if (waitStats){
        double timeInQ = getTime() - qObj.getTimeEnteredQueue();
        myTimeInQ.setValue(timeInQ);
        }
        if (sizeStatss) {
            myNumInQ.setValue(myList.size());
        }
        
        return(qObj);
    }
    
    public QObject removeNext(boolean waitStats, boolean sizeStatss){
        QObject qObj = myDiscipline.removeNext(myList);
        if (qObj != null){
            qObj.exitQueue();
            if (waitStats) {
                double timeInQ = getTime() - qObj.getTimeEnteredQueue();
                myTimeInQ.setValue(timeInQ);
            }
            if (sizeStatss) {
                myNumInQ.setValue(myList.size());
            }
            if (getEntityTrackingOption() == true){
                Object obj = qObj.getQueuedObject();
                myQObjectMap.remove(obj);
            }
        }
        myStatus = Status.DEQUEUED;
        notifyQueueListeners(qObj);
        return(qObj);
    }
    
    public final void enqueue(QObject queueingObject, int priority, boolean sizeStats){
        enqueue(queueingObject, priority, queueingObject.getQueuedObject(), sizeStats);
    }
    
    public void enqueue(QObject qObject, int priority, Object obj, boolean sizeStats){
        if (qObject == null)
                throw new IllegalArgumentException("The QObject must be non-null");

        qObject.enterQueue(this, priority, obj);

        myDiscipline.add(myList, qObject);
        if (getEntityTrackingOption() == true){
                if (obj == null)
                        throw new IllegalArgumentException("Attached object was null and map tracking was on.!");
                myQObjectMap.put(obj, qObject);
        }
        if (sizeStats) {
            myNumInQ.setValue(myList.size());
        }
        myStatus = Status.ENQUEUED;
        notifyQueueListeners(qObject);
    }
}
