@echo off
mvn install:install-file -DgroupId=org.ururau -DartifactId=jsc -Dversion=1.0 -Dfile=jars/jsc.jar -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true
mvn install:install-file -DgroupId=org.ururau -DartifactId=jsl-core -Dversion=1.0 -Dfile=jars/jsl-core.jar -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true
mvn install:install-file -DgroupId=org.ururau -DartifactId=JSLR1b1 -Dversion=1.0 -Dfile=jars/JSLR1b1.jar -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true
mvn install:install-file -DgroupId=org.ururau -DartifactId=ColtNoHep -Dversion=1.0 -Dfile=jars/ColtNoHep.jar -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true
