if WScript.Arguments.Count <> 2 then
    WScript.Echo "xlsm file and macro missing"
    WScript.Quit
end if
Set objExcel = CreateObject("Excel.Application")
objExcel.Workbooks.Open (objExcel.librarypath & "\SOLVER\SOLVER.XLAM")

wsfile = WScript.Arguments(0)
Set objworkbook = objExcel.Workbooks.Open(wsfile)
objExcel.Workbooks("solver.xlam").RunAutoMacros 1
wsfile_and_macro = WScript.Arguments(1)
objExcel.Application.Run wsfile_and_macro
objWorkbook.Save
objExcel.ActiveWorkbook.Close

objExcel.Application.Quit
WScript.Quit
