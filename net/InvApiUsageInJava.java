import net.sf.jni4net.Bridge;

import java.io.IOException;

import invapibasics40library.ApiComClass;
import invapibasics40library.IApiComClass;

public class InvApiUsageInJava {
    public static void main(String args[]) throws IOException {
        Bridge.init();
        Bridge.LoadAndRegisterAssemblyFrom(new java.io.File("InvApiBasics40Library.j4n.dll"));

        IApiComClass c = new ApiComClass();
	System.out.println("Load...");
	c.AppLoad();
	System.out.println("Run...");
	double time = c.RunSimulation(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
	
        System.out.printf(time+"(s) done.");
    }
}
