// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package invapibasics40library;

@net.sf.jni4net.attributes.ClrType
public class ApiComClass extends system.Object implements invapibasics40library.IApiComClass {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected ApiComClass(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public ApiComClass() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        invapibasics40library.ApiComClass.__ctorApiComClass0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorApiComClass0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    public native void AppLoad();
    
    @net.sf.jni4net.attributes.ClrMethod("(II)V")
    public native void ModifyActiveView(int w, int h);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void ModifyCaption(java.lang.String text);
    
    @net.sf.jni4net.attributes.ClrMethod("(II)D")
    public native double RunSimulation(int startStep, int endStep);
    
    @net.sf.jni4net.attributes.ClrMethod("(II)D")
    public native double GetSimulationLength(int startStep, int endStep);
    
    @net.sf.jni4net.attributes.ClrMethod("()I")
    public native int GetSimulationNumberOfSteps();
    
    public static system.Type typeof() {
        return invapibasics40library.ApiComClass.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        invapibasics40library.ApiComClass.staticType = staticType;
    }
    //</generated-proxy>
}
