@echo off
if not exist target mkdir target
if not exist target\classes mkdir target\classes


echo compile classes
javac -nowarn -d target\classes -sourcepath jvm -cp "..\..\lib\jni4net.j-0.8.8.0.jar"; "jvm\invapibasics40library\IApiComClass.java" "jvm\invapibasics40library\IApiComClass_.java"  "jvm\invapibasics40library\ApiComClass.java" 
IF %ERRORLEVEL% NEQ 0 goto end


echo InvApiBasics40Library.j4n.jar 
jar cvf InvApiBasics40Library.j4n.jar  -C target\classes "invapibasics40library\IApiComClass.class"  -C target\classes "invapibasics40library\IApiComClass_.class"  -C target\classes "invapibasics40library\__IApiComClass.class" -C target\classes "invapibasics40library\ApiComClass.class" > nul 
IF %ERRORLEVEL% NEQ 0 goto end


echo InvApiBasics40Library.j4n.dll 
del /s /q clr\inventor
csc /nologo /warn:0 /t:library /out:InvApiBasics40Library.j4n.dll /recurse:clr\*.cs  /reference:".\InvApiBasics40Library.dll" /reference:"..\lib\jni4net.n-0.8.8.0.dll"
IF %ERRORLEVEL% NEQ 0 goto end

:end
