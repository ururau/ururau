﻿using Inventor;
using System;

namespace InvApiBasics40Library
{
    public interface IApiComClass
    {
        void AppLoad();
        void ModifyActiveView(int w, int h);
        void ModifyCaption(String text);
        double RunSimulation(int startStep, int endStep);
        double GetSimulationLength(int startStep, int endStep);
        int GetSimulationNumberOfSteps();
    }

    public class ApiComClass : IApiComClass
    {
        private Inventor.Application m_inventorApp = null;
        private Inventor.View oView = null;
        private Inventor.AssemblyDocument ad = null;
        private SimulationManager dsman = null;
        public void AppLoad()
        {
            
            try //Try to get an active instance of Inventor
            {
                try
                {
                    m_inventorApp = System.Runtime.InteropServices.Marshal.GetActiveObject("Inventor.Application") as Inventor.Application;
                    
                    System.Console.WriteLine("Create Inventor App instance via Interop");
                    ad = m_inventorApp.ActiveDocument as AssemblyDocument;
                    dsman = ad.ComponentDefinition.SimulationManager;
                }
                catch
                {
                    Type inventorAppType = System.Type.GetTypeFromProgID("Inventor.Application");

                    m_inventorApp = System.Activator.CreateInstance(inventorAppType) as Inventor.Application;
                    //Must be set visible explicitly
                    ad = m_inventorApp.ActiveDocument as AssemblyDocument;
                    dsman = ad.ComponentDefinition.SimulationManager;
                    m_inventorApp.Visible = true;
                    System.Console.WriteLine("Open Inventor");
                }
            }
            catch
            {
                System.Console.WriteLine("Error: couldn't create Inventor instance");
            }
        }

        public double RunSimulation(int startStep, int endStep)
        {
            int steps =1;
            int speed =1;
            double l = 0;
            
            foreach (DynamicSimulation dssim in dsman.DynamicSimulations)
            {
                    if (dssim.IsInSimulationMode)
                    {
                        dssim.ComputeSimulation(endStep);
                        dssim.PlaySimulation(startStep, endStep);
                        l = dssim.SimulationLength;
                    }
                    steps = dssim.NumberOfTimeSteps;
                    speed = dssim.PlaybackSpeed;
                    
                    break;
            }
            
            
            l = ((endStep - startStep-1) * l) / steps; 
            return l;
        }

        public double GetSimulationLength(int startStep, int endStep)
        {
            Inventor.AssemblyDocument ad;
            ad = (Inventor.AssemblyDocument)m_inventorApp.ActiveDocument;
            int steps = 1;
            int speed = 1;
            double l = 0;
            SimulationManager dsman = ad.ComponentDefinition.SimulationManager;
            foreach (DynamicSimulation dssim in dsman.DynamicSimulations)
            {
                if (dssim.IsInSimulationMode)
                {
                    l = dssim.SimulationLength;
                }
                steps = dssim.NumberOfTimeSteps;
                speed = dssim.PlaybackSpeed;
                break;
            }

            l = ((endStep - startStep - 1) * l) / steps;
            return l;
        }

        public int GetSimulationNumberOfSteps()
        {
            Inventor.AssemblyDocument ad;
            ad = (Inventor.AssemblyDocument)m_inventorApp.ActiveDocument;
            int steps = 1;
            int speed = 1;
            double l = 0;
            SimulationManager dsman = ad.ComponentDefinition.SimulationManager;
            foreach (DynamicSimulation dssim in dsman.DynamicSimulations)
            {
                if (dssim.IsInSimulationMode)
                {
                    l = dssim.SimulationLength;
                }
                steps = dssim.NumberOfTimeSteps;
                speed = dssim.PlaybackSpeed;
                break;
            }

            return steps;
        }

        public void ModifyActiveView(int w, int h)
        {
            oView = m_inventorApp.ActiveView;
            ApplicationAddIns addins = m_inventorApp.ApplicationAddIns;
            Inventor.Document d;
            if (oView != null && w > 0 && h > 0)
            {
                oView.Width = w;
                oView.Height = h;
                
            }
        }

        public void ModifyCaption(String text)
        {
            if (text.Length > 0)
            {
                m_inventorApp.Caption = text;
            }
        }
    }
}
